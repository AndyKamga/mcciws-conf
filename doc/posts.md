## Création d'un forum

Ce mini tutoriel décrit de manière brève la méthodologie de création d'une partie de l'API du forum des anciens. **Elle n'est pas à répéter car déjà faite !** 

NB : Il ne s'agit pas d'un cours sur Laravel. En cas d'incompréhension du code, merçi de vous rediriger vers la docummentation du Framework.

### Création de la branche de travail

Créer une branche à partir de la branche "develop". Cette branche nous permettra de mettre à jour la base de données.

```bash
git checkout develop
git checkout -b nom-de-ma-branche
```

Rendez-vous dans le répertoire "core".

```bash
cd core
```

### Création des nouvelles tables (Création des modèles et leur migration)

La table "users" existe déjà. Dans le répertoire "core", générer 7 "objets/modèles/entités" correspondants aux 7 nouvelles tables de notre application, en exécutant les commandes suivantes. 

NB : L'ordre de création des objets a une grande importance. Il doit être respecté. Dans le cas contraire, on aura des erreurs lors de la migration des tables en raison de la non existence de certaines.

```
php artisan make:model Models/PostCategory --migration
php artisan make:model Models/Post --migration
php artisan make:model Models/PostPostCategory --migration 
php artisan make:model Models/PostLike --migration
php artisan make:model Models/PostFile --migration
php artisan make:model Models/PostComment --migration
php artisan make:model Models/PostReport --migration
```

Chaque commande génère un model dans le répertoire "core/app/Models" et une migration dans le répertoire "core/database/migrations".

Nous allons compléter nos fichiers

##### PostCategory (Migration et Model)

```php
# XXX_XX_XX_XXXXXXcreate_post_categories_table.php

public function up()
    {
       Schema::create('post_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 50)->unique();
            $table->string('description', 300);
            $table->timestamps();
        });
    }
```

```php
# postCategory.php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

/**
 * @property string name
 * @property string description
 */
class PostCategory extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description'];

    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }
}
```



##### Post (Migration et Model)

```php
# XXX_XX_XX_XXXXXXcreate_posts_table.php

public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('text', 500);
            $table->string('iframe_code', 3000)->nullable();
            $table->boolean('is_pinned')->default(false);
            $table->unsignedInteger('user_id');

            $table->foreign('user_id')
                ->references('id') ->on('users');

            $table->timestamps();
        });
    }
```

```php
# post.php

/**
 * @property string text
 * @property string iframe_code
 * @property boolean is_pinned
 * @property integer user_id
 */
class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'text', 'iframe_code', 'is_pinned'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function categories()
    {
        return $this->belongsToMany(PostCategory::class);
    }

    public function likes()
    {
        return $this->hasMany(PostLike::class);
    }

    public function files()
    {
        return $this->hasMany(PostFile::class);
    }

    public function comments()
    {
        return $this->hasMany(PostComment::class);
    }

    public function reports()
    {
        return $this->hasMany(PostReport::class);
    }
}
```



**PostPostCategory (Migration) :** Permet de faire la liaison ManyToMany entre les tables post_categories et posts

```php
# XXX_XX_XX_XXXXXXcreate_post_post_categories_table.php

public function up()
    {
       Schema::create('post_post_category', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('post_id');
            $table->unsignedBigInteger('post_category_id');

            $table->foreign('post_id')
                ->references('id') ->on('posts');

            $table->foreign('post_category_id')
                ->references('id') ->on('post_categories');

            $table->timestamps();
        });
    }
```



##### PostLike (Migration et Model)

```php
# XXX_XX_XX_XXXXXXcreate_post_likes_table.php

public function up()
    {
        Schema::create('post_likes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id');
            $table->unsignedBigInteger('post_id');

            $table->foreign('user_id')
                ->references('id') ->on('users');

            $table->foreign('post_id')
                ->references('id') ->on('posts')
                ->onDelete('cascade');
            
            $table->timestamps();
        });
    }
```

```php
# postLike.php

/**
 * @property number user_id
 * @property number post_id
 */
class PostLike extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'post_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
```



##### PostFile (Migration et Model)

```php
# XXX_XX_XX_XXXXXXcreate_post_files_table.php

public function up()
    {
         Schema::create('post_files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 1000);
            $table->text('path');
            $table->string('media_type', 30);
            $table->unsignedBigInteger('post_id');

            $table->foreign('post_id')
                ->references('id') ->on('posts')
                ->onDelete('cascade');
            
            $table->timestamps();
        });
    }
```

```php
# postFile.php

/**
 * @property string name
 * @property string path
 * @property string media_type
 * @property number post_id
 */
class PostFile extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'path', 'media_type', 'post_id'];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
```



##### PostComment (Migration et Model)

```php
# XXX_XX_XX_XXXXXXcreate_post_comments_table.php

public function up()
    {
        Schema::create('post_comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('text', 200);
            $table->unsignedBigInteger('post_id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')
                ->references('id') ->on('users');
            $table->foreign('post_id')
                ->references('id')->on('posts')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }
```

```php
# postComment.php

/**
 * @property string text
 * @property string post_id
 * @property integer user_id
 */
class PostComment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['text', 'post_id', 'user_id'];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }
    
    public function reports()
    {
        return $this->hasMany(PostCommentReport::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
```

##### PostReport (Migration et Model)

```php
# XXX_XX_XX_XXXXXXcreate_post_reports_table.php

public function up()
    {
        Schema::create('post_reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description', 2000);
            $table->unsignedInteger('user_id');
            $table->unsignedBigInteger('post_id');
            $table->unsignedBigInteger('comment_id')->nullable();

            $table->foreign('user_id')
                ->references('id') ->on('users');
            $table->foreign('post_id')
                ->references('id') ->on('posts')
                ->onDelete('cascade');
            $table->foreign('comment_id')
                ->references('id') ->on('post_comments')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }
```

```php
# postReport.php

/**
 * @property string description
 */
class PostReport extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['description'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }
    
    public function comment()
    {
        return $this->belongsTo(PostComment::class);
    }
}
```



#### **Mises à jour du Model User**

Ajouter les 4 méthodes suivantes au model User.

```php
# User.php

public function posts()
{
        return $this->hasMany(Post::class);
}
    
public function postLikes()
{
    return $this->hasMany(PostLike::class);
}

public function postComments()
{
    return $this->hasMany(PostComment::class);
}

public function postCommentReports()
{
    return $this->hasMany(PostCommentReport::class);
}
```



#### Création d'un Seeder pour les catégories

Lors de la migration des nouvelles tables dans la base de données, notamment de la table "post_categories", nous souhaitons que l'application génère des données de initiales. Pour cela nous allons créer un seeder grâce à la commande (rappel : dans le répertoire "core") :

```
php artisan make:seeder PostCategorySeeder
```

Compléter la méthode "run" du fichier PostCategorySeeder, situé dans le répertoire "core/database/seeds".

```php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ["Examen", "Conférences", "Événement", "Conseil", "Stage", "Offre d'emploi"];
        $descriptions = ["Sujet, date, mise à jour d'examen", "Destinée aux conférences", "Relative aux sorties", 
            "Conseil d'étudiants ou d'anciens", "Stage", "Proposition d'emploi"
        
        ];
        $nbr = sizeof($categories);

        for ($i = 0; $i < $nbr; $i++) {
            DB::table('post_categories')->insert(
                [
                    'id' => $i + 1,
                    'name' =>  $categories[$i],
                    'description' => $descriptions[$i]
                ]
            );
        }
    }
}

```
Enregistrez le nouveau seeder (PostCategorySeeder) dans le fichier DatabaseSeeder.php situé dans le même répertoire. A la prochaine installation de l'application, le seeder générant les catégories de publication sera exécuté, en plus des autres seeder déjà enregistrés.

```php
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            DroitsSeeder::class,
            DatesSeeder::class,
            UsersSeederStd::class,
            PostCategorySeeder::class
        ]);
    }
}
```

Exécutez les migrations (générez vos nouvelles tables) en exécutant (toujours dans le dossier "core") la commande suivante :

```
php artisan migrate
```

Exécutez le seeder récemment créé

```
php artisan db:seed --class=PostCategorySeeder
```

Si tout fonctionne correctement, enregistrer vos modifications

```
git add . 
git commit -m "feat : update database"
```



### Mise en place de la gestion des publications (commentaires exclus)

Notre application (Laravel) adopte une architecture Modèle, Vue, Contrôleur - MVC ( https://openclassrooms.com/fr/courses/4670706-adoptez-une-architecture-mvc-en-php/4678736-comment-fonctionne-une-architecture-mvc).

A été ajouté au model MVC de notre application la notion de services, afin d'alléger les contrôleurs. On passe donc d'un modèle MVC (Modèle, Vue, Contrôleur) à un modèle MVCS (Modèle, Vue, Contrôleur, Service). 

![mvcs](mvcs.png)

Les routes communiquent avec un contrôleur qui communique avec un service. Les requêtes vers la base de données sont généralement effectuées depuis un service, qui est l'élément métier. Elles peuvent parfois être effectuées depuis le contrôleur, si celui ci n'est pas trop surchargé. 

Dans les règles de l'art, un contrôleur ne doit jamais être saturé en ligne de code. Autrement dit, **un contrôleur doit toujours avoir au maximum 5 lignes de code**. En présence de plus de 5 lignes de code, il est impératif de songer à déplacer une partie de notre logique dans un service. Une dizaine de ligne de code (et pas plus) dans un contrôleur est tolérable dans certains cas !



#### Création d'un service

Créer la class PostService dans le dossier *app/Services*. Ajouter les méthodes nécessaires... 

Pour le site du Master, notre service contient une méthode pour créer une publication et une méthode pour supprimer une publication. Comme sur Twitter, nous ne prenons pas en compte la modification d'une publication.

**Afin de ne pas retourner aux clients de l'API un objet modèle de notre base de données, mais une structure cohérente et adaptée à leur application, les réponses sont reformatées grâce à un PostDto, crée préalablement dans le dossier *app/Dto*.**

```php

namespace App\Services;

use App\Dto\PostDto;
use App\Models\Post;
use Illuminate\Http\Request;

class PostService extends Service
{
    /**
     * @description Répertoire de stockage des pièces jointes
     * @var string
     */
    private $postFilesFolder = 'post_files';

    /**
     * @description Permet de créer une publication
     * @param Request $request
     * @return PostDto
     */
    public function create(Request $request) {
        $user = $request->user();
        $post = new Post([
            'text' => $request->input('text'),
            'iframe_code' => $request->input('iframe_code'),
            'is_pinned' => false
        ]);

        $post->user()->associate($user);
        $post->save();

        if($request->hasfile('attachments'))
        {
            foreach($request->allFiles() as $file)
            {
                $post->files()->create([
                    'name' => $file->getClientOriginalName(),
                    'path' => $file->store($this->postFilesFolder, 'public'),
                    'media_type' => $file->getClientMimeType(),
                    'post_id' => $post->id
                ]);
            }
        }

        return new PostDto($post, $user);
    }

    /**
     * @description Permet de supprimer une publication
     * @param Request $request
     * @param $id
     * @return true | false
     */
    public function delete(Request $request, $id) {
        $post = Post::find($id);
        $userRequest = $request->user();

        if($post == null) {
            return false;
        } else if($post->user()->first()->id != $userRequest->id && !in_array($userRequest->droit, [3, 4, 5])) {
            return false;
        }

        foreach ($post->files()->get() as $postFile) {
            unlink(storage_path('app/public/'.$postFile->path));
        }

        return $post->delete();
    }
}
```



#### Création d'un Contrôleur 

Créer un contrôleur en exécutant la commande suivante et  ajouter les méthodes nécessaires... 

```
php artisan make:controller Api/PostController
```

Pour le site du Master, notre contrôleur contient une méthode pour récupérer la liste des publications, une méthode pour récupérer une publication, une méthode pour créer une publication (Elle valide les données puis les envoies au service qui enregistre le tout en base de données), une méthode pour épingler ou désépingler une publication, et une méthode pour supprimer une publication (La requête est envoyée dans le service qui s'occupe du traitement).

On notera que chaque contrôleur (méthode) de notre classe PostController contient en dessus d'elle une documentation qui est visible par les clients de l'API, à l'adresse (https://adresse-du-site/api/documentation).

Après avoir documenté un contrôlleur, la commande suivante permet de rendre accéssible cette documentation via le lien ci-dessus.

```
php artisan l5-swagger:generate  
```

Il est important de toujours documenter les contrôleurs créés, afin d'indiquer aux clients de l'API comment soumettre leurs requêtes, et surtout à quoi ils ont accès. Pour en savoir plus (https://swagger.io, https://github.com/DarkaOnLine/L5-Swagger). 



```php
namespace App\Http\Controllers\Api;

use App\Dto\PostDto;
use App\Http\Responses\ResponseEntity;
use App\Models\Post;
use App\Services\PostService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    private $postService;

    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    /**
     * @OA\Get (path="/api/social/posts", tags={"Publications"}, security={{ "apiAuth": {} }},
     *     description="Voir toutes les publications", summary="Voir les publications",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *      @OA\Parameter(
     *         name="page",
     *         in="query",
     *         description="Pagination",
     *         style="form"
     *      ),
     *      @OA\Response(response="200", description="Liste des publications postées",
     *          @OA\JsonContent(
     *              @OA\Property(property="current_page", type="number", example=1),
     *              @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/Publication")),
     *              @OA\Property(property="first_page_url", type="string", example="https://cci.univ-tours.fr/api/social/posts?page=1"),
     *              @OA\Property(property="from", type="number", example=1),
     *              @OA\Property(property="last_page", type="number", example=1),
     *              @OA\Property(property="last_page_url", type="string", example="https://cci.univ-tours.fr/api/social/posts?page=1"),
     *              @OA\Property(property="next_page_url", type="string", example=null),
     *              @OA\Property(property="path", type="string", example="https://cci.univ-tours.fr/api/social/posts"),
     *              @OA\Property(property="per_page", type="number", example=5),
     *              @OA\Property(property="prev_page_url", type="string", example=null),
     *              @OA\Property(property="to", type="number", example=2),
     *              @OA\Property(property="total", type="number", example=2),
     *     ))
     * )
     */
    public function findAll()
    {
        $paginatedPosts = Post::orderBy('id', 'desc')->paginate(5);
        $paginatedPosts->getCollection()->transform(function ($post) {
            return new PostDto($post);
        });

        return ResponseEntity::ok($paginatedPosts);
    }

    /**
     * @OA\Get (path="/api/social/posts/{post}", tags={"Publications"}, security={{ "apiAuth": {} }},
     *     description="Afficher une publication", summary="Voir une publication",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *     @OA\Parameter(
     *         name="post",
     *         in="path",
     *         description="Identifiant de la publication",
     *         required=true,
     *         style="form"
     *     ),
     *      @OA\Response(response="200", description="Publication",
     *          @OA\JsonContent(ref="#/components/schemas/Publication")
     *     ))
     * )
     * @param $id
     * @return JsonResponse
     */
    public function findById($id)
    {
        $post = Post::find($id);

        if($post == null) {
            return ResponseEntity::notFound(null, "La publication n'existe pas !");
        }

        return ResponseEntity::ok(new PostDto($post));
    }

    /**
     * @OA\Post (path="/api/social/posts", tags={"Publications"}, security={{ "apiAuth": {} }},
     *     description="Créer une publication", summary="Créer une publication",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *
     *      @OA\RequestBody (
     *          @OA\MediaType(mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  @OA\Property(property="text", type="string", description="text"),
     *                  @OA\Property(property="iframe_code", type="string", description="iframe_code"),
     *                  @OA\Property(property="attachments", type="file", description="attachments")
     *              )
     *          ),
     *      ),
     *      @OA\Response(response="201", description="Publication",
     *          @OA\JsonContent(ref="#/components/schemas/Publication")
     *     ))
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'text' => ['required', 'string', 'min:10', 'max:500'],
            'iframe_code' => ['nullable', 'string',  'min:20', 'max:3000'],
            'is_pinned'  => ['nullable', 'boolean'],
            'attachments' => ['nullable','mimes:jpeg,jpg,png,doc,pdf,docx,jpg,gif,svg', 'max:4048'],
            'attachments.*' => ['nullable','mimes:jpeg,jpg,png,doc,pdf,docx,jpg,gif,svg', 'max:4048']
        ]);

        if ($validator->fails()) {
            return ResponseEntity::badRequest($validator->errors());
        }

        return ResponseEntity::created($this->postService->create($request));
    }

    /**
     * @OA\Patch  (path="/api/social/posts/{post}", tags={"Publications"}, security={{ "apiAuth": {} }},
     *     description="Épingler/Désépingler une publication", summary="Épingler/Désépingler une publication",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *     @OA\Parameter(
     *         name="post",
     *         in="path",
     *         description="Identifiant de la publication",
     *         required=true,
     *         style="form"
     *     ),
     *     @OA\RequestBody (required=true, description="Informations d'identification de l'utilisateur",
     *      @OA\JsonContent (required={"is_pinned"}, @OA\Property(property="is_pinned", type="boolean", example=true)),
     *      ),
     *      @OA\Response(response="200", description="Publication")
     *    )
     * )
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function pin(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'is_pinned'  => ['required', 'boolean']
        ]);

        if ($validator->fails()) {
            return ResponseEntity::badRequest($validator->errors());
        }

        $post = Post::find($id);

        if($post == null) {
            return ResponseEntity::notFound(null,"La publication a supprimer n'existe pas !");
        }

        $post->update($request->all());

        $message = $request->input('is_pinned') ?"La publication a été épinglée":"La publication a été désépinglée";

        return ResponseEntity::ok(null, $message);
    }

    /**
     * @OA\Delete  (path="/api/social/posts/{post}", tags={"Publications"}, security={{ "apiAuth": {} }},
     *     description="Supprimer une publication", summary="Supprimer une publication",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *     @OA\Parameter(
     *         name="post",
     *         in="path",
     *         description="Identifiant de la publication",
     *         required=true,
     *         style="form"
     *     ),
     *      @OA\Response(response="200", description="Publication supprimée"
     *     ))
     * )
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function delete(Request $request, $id)
    {
        if(!$this->postService->delete($request, $id)) {
            return ResponseEntity::notFound(null,"La publication n'a pas pu être supprimée !");
        }

        return ResponseEntity::ok(null, "La publication a été supprimée");
    }
}
```



#### Création des routes 

##### Liste des routes à créer

| URI                 | METHODE | DESCRIPTION URI                           |
| ------------------- | ------- | ----------------------------------------- |
| social/posts        | GET     | Permet de récupérer tous les publications |
| social/posts/{post} | GET     | Permet de récupérer une publication       |
| social/posts        | POST    | Permet de créer une publication           |
| social/posts/{post} | DELETE  | Permet de supprimer une publication       |
| social/posts/{post} | PATCH   | Permet d'épingler une publication         |

La définition des routes d'API se fait dans le dossier *core/routes/api.php*.  Comme on peut le voir  ci-dessous, Nos routes sont protégées par deux middlewares qui permettent de vérifier l'authentification d'un utilisateur et ses droits d'accès. Elles sont préfixées par le mots 'social'.

Les routes sont reliées à un contrôleur et **surtout sont nommées !** 

```php
#core/routes/api.php

Route::group(
    [
        'middleware' => ['auth:api', 'userAuthorizationApi'],
    ],
    function () {
        Route::prefix('social')->group(function () {
            Route::get('posts', 'Api\PostController@findAll')->name('voir-publications');
            Route::get('posts/{post}', 'Api\PostController@findById')->name('voir-publications');
            Route::post('posts', 'Api\PostController@create')->name('voir-publications');
            Route::patch('posts/{post}', 'Api\PostController@pin')->name('epingler-publication');
            Route::delete('posts/{post}', 'Api\PostController@delete')->name('voir-publications');
        });
    }
);

```

**Le nommage des routes est très important. Il permet à l'administrateur d'attribuer des permissions sur une fonctionnalité (sur une route). Le nom de la route doit toujours être composé de maximum trois mots séparés par un tiret. Les mots d'écrivent la fonctionnalité**. Enfin de simplifier l'attribution des permissions, plusieurs routes peuvent avoir le même nom.  Par exemple, un utilisateur ayant la permission de voir les publications doit automatiquement avoir la permission de créer une publication et supprimer sa publication. C'est pourquoi on attribut aux trois routes concernées le nom "voir-publication".



Mettez à jour le Swagger (la documentation) de l'API en exécutant la commande 

```php
php artisan l5-swagger:generate
```

Tester votre API à l'aide de Postman. Si tout fonctionne correctement, enregistrer vos modifications et pousser  (pusher) les en ligne.

```bash
git add .
git commit -m "feat : add post management"
git push --set-upstream origin nom-de-ma-branche
```



