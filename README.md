<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>
<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>



# Développement et maintenance du site du Master CCI.

**Si vous possédez la machine virtuelle de développement du site du master CCI, après avoir effectué l'étape 8 et 9 des prérequis, passez à l'étape "Démarrer le serveur en local" .**

## Quelques liens:

Site officiel du master : https://www.cci.univ-tours.fr

Documentation de l'API du site : https://www.cci.univ-tours.fr/api/documentation

## Prérequis

1. Des bases en programmation Orienté Objet en PHP et sur le Framework Laravel.
2. PhpStorm (<https://www.jetbrains.com/phpstorm/download/#section=windows>) équipé du plugin Laravel. Il est fortement recommandé de télécharger et installer l'IDE PhpStorm pour le développement de site en PHP. En plus d'être le standard en entreprise, il facilitera grandement votre développement. L'IDE est gratuit pendant 1 an pour les étudiants. L'activation passe par une inscription sur le site du fabriquant, muni de son adresse email étudiant.
2. Composer (<https://getcomposer.org/download/>).
3. Xampp (<https://www.apachefriends.org/fr/download.html>).
4. Postman (<https://www.getpostman.com/downloads/>) : Permet de tester l'API.
6. Git (https://git-scm.com/downloads).
7. SSHFS-Win : Permet de se connecter en SSH à une machine linux afin exécuter des commandes Git sur le serveur (Window seulement).
8. Un compte Pusher (https://pusher.com) pour tester le chat
9. Un compte Mailtrap (https://mailtrap.io) pour tester l'envoi d'email



## Création de la base de données

Créer votre base de données depuis PhpMyAdmin. Le nom de notre base de données doit obligatoirement être "**mastercci**", **sans mot de passe** et "**root**" comme identifiant avec un format de donnée "**utf8mb4_unicode_ci**".



## Récupération et installation du projet

***Vous devez être collaborateur du projet sur Gitlab pour pouvoir récupérer le projet !***

Clonez le projet depuis Gitlab.

```
git clone https://gitlab.com/mastercci/mcciws.git
```

Rendez-vous dans le dossier "core" situé dans le dossier "mcciws" en exécutant les trois commandes suivantes dans l'ordre.

```
cd mcciws/core
composer install
php artisan app:install:local
```

En cas d'echec de la commande, assurez vous que votre base de données ne contient pas de tables et saisissez à nouveau les commandes suivantes

```
composer install
php artisan app:install:local
```

Le schéma de la base de données se trouve le répertoire : core/database/diagram.png

![diagram](core/database/diagram.png)

Identifiant root 

```
login : root@mastercci.fr
mot de passe : "masterccidev". 
```



## Démarrer le serveur en local

Dans le dossier "core", exécutez la commande suivante :

```
php artisan serve
```

Si vous obtenez une erreur du type "No Application Encryption Key Has Been Specified", arrêter le serveur et exécutez les commandes.

```
php artisan key:generate
php artisan serve
```

Le serveur démarrera à l'adresse 127.0.0.1 sur le port 8000 par défaut (127.0.0.1:8000).**

Il est possible de se passer de la commande artisan pour démarrer le serveur. Pour cela, le fichier "httpd.conf" doit être modifié depuis Xampp, en ajoutant les lignes suivantes :

```
DocumentRoot "C:/Users/nom_ordinateur/Documents/MonSite/mastercci-app"

<Directory "C:/Users/nom_ordinateur/Documents/MonSite/mastercci-app">
	Options -Indexes
	Require all denied 
	Require all granted
	AllowOverride All
</Directory>
```

Le site sera ensuite accessible depuis l'adresse 127.0.0.1 sur le port 80 (127.0.0.1:80).

##  

## Mise à jour du site

#### Développement d'une nouvelle fonctionnalité

**Les développeurs doivent pusher leur branche de développement et demander un MERGE REQUEST sur GITLAB au responsable du développement. La création des branches et les merges doivent êtres effectués par le responsable du développement du site ! ** 

**Aucun Push ne doit être éffectué dans le serveur, seul les Pull sont autorisés !** 

Créer une branche portant le nom de votre fonctionnalité à partir de la branche "develop" et développé votre fonctionnalité dans celle ci. 

```
 git checkout develop
 git checkout -b ma-fonctionnalite
```

Quand le développement de votre fonctionnalité est terminé, mergé les modifications (votre branche) dans la branche "develop", après avoir effectué un commit puis un push de votre branche.

```
git add *
git commit -m "Nom du commit"
git push origin ma-fontionnalite
git checkout develop
git merge ma-fonctionnalite
git push origin develop
```

#### Mise en préproduction

Connectez-vous à Gitlab et demandez un "merge request" de la branche **develop vers la branche release** (voir tutoriel : https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)

Quand le merge request est validé, connectez-vous au serveur en sshfs depuis une machine sous linux. Si votre machine ne possède pas ce module, installer le en excécutant la commande suivante :

```
sudo apt install sshfs
```

Si vous rencontrez une erreur de verrouillage du "dpkg", la commande suivant devrait résoudre le problème.

```
sudo rm /var/lib/dpkg/lock
```

##### Connexion au serveur en SSHFS depuis une machine Linux

Crééz un dossier de montage sur votre machine. Dans cet exemple, notre dossier s'appelle "masterCCI" et connectez-vous en SSHFS avec vos identifiants universitaires.

```bash
mkdir masterCCI
sshfs numero-etu-suivi-de-t@webhosting2.sciences.univ-tours.fr:/ masterCCI/
```

Si une confirmation de connexion est demandée, saisissez "yes". Puis votre mot de passe étudiant. Si vos identifiants sont corrects, le serveur sera monté dans le répertoire masterCCI. 

Rendez-vous dans répertoire contenant les fichiers du site de préproduction.

```bash
cd masterCCI/mes_sites/master-cci-prov/
```

Récupérer modifications en executant la commande suivante :

```
git pull
```

En cas **d'installation de nouvelles dépendances** au projet, exécuter à la suite, les commandes suivantes :

```
cd core
php artisan app:update
```

Vérifier que le site fonctionne correctement en préproduction en vous rendant à l'adresse :  http://master-cci-prov.sciences.univ-tours.fr/ 

Si tout fonctionne correctement, passer à la mise en production.

#### Mise en production

Connectez-vous à Gitlab et demandez un "merge request" de la branche **release vers la branche master** (voir tutoriel : https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)

Quand le merge request est validé, connectez-vous au serveur en sshfs (voir plus haut).

Rendez-vous dans répertoire contenant les fichiers du site de production.

```bash
cd masterCCI/mes_sites/master-cci.sciences.univ-tours.fr/
```

Récupérer modifications en executant la commande suivante :

```
git pull
```

En cas **d'installation de nouvelles dépendances** au projet, exécuter à la suite, les commandes suivantes :

```
cd core
php artisan app:update
```

Vérifier que le site fonctionne correctement en production en vous rendant à l'adresse :   https://www.cci.univ-tours.fr/ 

PS : La commande *php artisan app:update* effectue dans l'ordre les commandes suivantes :

```bash
'git fetch origin','git pull','composer install','composer dumpautoload','php artisan migrate','php artisan storage:link'
```

## Principe de fonctionnement du site

Prérequis :  Des connaissances de base sur le Framework Laravel 5.7 (https://laravel.com/docs/5.7/installation).

L'accès au routes de l'API se fait après s'être authentifié via la route "api/login". Si l'authentification se passe correctement, vous recevrez un token JWT à passer dans les headers lors de chaque requêtes de votre application client (Exemple avec postman : https://dev.to/loopdelicious/using-jwt-to-authenticate-and-authorize-requests-in-postman-3a5h).

#### Création d'une API de gestion des publications (Exemple) :

[Exemple de création d'une API de publication](./doc/posts.md)  (doc/posts.md)

#### Bon à savoir :

Les websockets sur le chat passent par un service tiers appelé Pusher. L'événement émit lors de l'envoie des messages est intitulé "MessageCreatedEvent". Les applications clientes devont utiliser le package npm "**Laravel Echo**" (https://www.npmjs.com/package/laravel-echo) et le package npm "**Pusher**'" (https://www.npmjs.com/package/pusher-js) pour générer les sockets.

***Exemple sur Angular CLI***

```typescript
import Pusher from "pusher-js"
import Echo from 'laravel-echo';

public pusher = new Pusher(<PUSHER_API_KEY>, {
    cluster: <PUSHER_API_CLUSTER>,
})
  
public echo = new Echo({
    authEndpoint : 'http://localhost:8000/broadcasting/auth',
    broadcaster: 'pusher',
    key: <PUSHER_API_KEY>,
    cluster: <PUSHER_API_CLUSTER>,
    forceTLS: true,
    encrypted: true,
    auth:{
        headers:
        {
            'Authorization': 'Bearer ' + <JWT_ACCESS_TOKEN>
        }
    }
  })
  
  echo.private('chat.' + <IDENTIFIANT_DU_CHAT>)
      .listen('MessageCreatedEvent', (e) => {
        // Traitement de l'évément...
  })
```

pour des raisons de performances, les requêtes SQL doivent êtres fait avec l'ORM Eloquent. Pour en savoir plus : https://laravel.com/docs/5.7/eloquent