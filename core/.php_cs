<?php
$finder = PhpCsFixer\Finder::create()
    ->exclude('bootstrap/')
    ->exclude('app/Console/')
    ->exclude('app/Exceptions/')
    ->exclude('app/Providers/')
    ->exclude('app/Http/Middleware/')
    ->exclude('config/')
    ->exclude('public/')
    ->exclude('resources/')
    ->exclude('storage/')
    ->exclude('server.php')
    ->in(__DIR__)
;
return PhpCsFixer\Config::create()
    ->setRules([
        '@PSR2' => true,
        'array_syntax' => ['syntax' => 'short'],
        'array_indentation' => true,
        'method_separation' => true,
        'no_multiline_whitespace_before_semicolons' => true,
        'single_quote' => true,
        'binary_operator_spaces' => array(
            'align_double_arrow' => false,
            'align_equals' => false,
        ),
        'braces' => array(
            'allow_single_line_closure' => true,
        ),
        'concat_space' => array('spacing' => 'none'),
        'declare_equal_normalize' => true,
        'function_typehint_space' => true,
        'hash_to_slash_comment' => true,
        'include' => true,
        'lowercase_cast' => true,
        'no_extra_consecutive_blank_lines' => array(
            'curly_brace_block',
            'extra',
            'parenthesis_brace_block',
            'square_brace_block',
            'throw',
            'use',
        ),
        'no_multiline_whitespace_around_double_arrow' => true,
        'no_spaces_around_offset' => true,
        'no_unused_imports' => true,
        'ordered_imports' => array('sort_algorithm' => 'alpha'),
        'no_whitespace_before_comma_in_array' => true,
        'no_whitespace_in_blank_line' => true,
        'object_operator_without_whitespace' => true,
        'phpdoc_order' => false,
        'phpdoc_summary' => true,
        'return_type_declaration' => array('space_before' => 'one'),
        'single_blank_line_before_namespace' => true,
        'ternary_operator_spaces' => true,
        'trim_array_spaces' => true,
        'unary_operator_spaces' => true,
        'whitespace_after_comma_in_array' => true,
        'function_declaration' => true,
        'indentation_type' => true,
        'no_spaces_after_function_name' => true,
        'no_spaces_inside_parenthesis' => true,
        'not_operator_with_successor_space' => true,
    ])
    ->setFinder($finder)
;