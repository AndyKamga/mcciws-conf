<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use Illuminate\Support\Facades\Route;

Route::post('login', 'Api\AuthController@login')->name('login-api');
Route::get('logout', 'Api\AuthController@logout')->middleware('auth:api')->name('login-api');

Route::group(
    [
        'middleware' => ['auth:api', 'userAuthorizationApi'],
    ],
    function () {
        Route::get('internship-offers', 'Api\OffresController@index')->name('voir-offres-de-stage-api');
        Route::patch('internship-offers/{offre}', 'Api\OffresController@disable')->name('voir-offres-de-stage-api');
    }
);

// Routes relatives au réseau social

Route::group(
    [
        'middleware' => ['auth:api', 'userAuthorizationApi'],
    ],
    function () {

        // Routes liées à la recherche
        Route::prefix('search')->group(function () {
            // Rechercher un utilisateur

            Route::get('users/{name}', 'Api\SearchController@users')->name('rechercher-utilisateur');
            Route::get('categories/{name}', 'Api\SearchController@categories')->name('rechercher-utilisateur');
        });

        // Routes liées au réseaux social

        Route::prefix('social')->group(function () {

            // Statistiques

            Route::get('stats/posts', 'Api\StatisticController@posts')->name('voir-publications');

            // Profil utilisateur

            Route::get('profils/{user}', 'Api\SocialProfilController@user')->name('voir-publications');

            // Publications, commentaires et catégories

            Route::get('posts', 'Api\PostController@findAll')->name('voir-publications');
            Route::get('posts/{post}', 'Api\PostController@findById')->name('voir-publications');
            Route::post('posts', 'Api\PostController@create')->name('voir-publications');
            Route::patch('posts/{post}', 'Api\PostController@pin')->name('epingler-publication');
            Route::delete('posts/{post}', 'Api\PostController@delete')->name('voir-publications');

            Route::get('posts/{post}/likes', 'Api\PostController@findAllLike')->name('voir-publications');
            Route::post('posts/{post}/likes', 'Api\PostController@addLike')->name('voir-publications');
            Route::delete('posts/{post}/likes', 'Api\PostController@removeLike')->name('voir-publications');

            Route::get('posts/{post}/comments', 'Api\CommentController@findAll')->name('commenter-publication');
            Route::get('posts/{post}/comments/{comment}', 'Api\CommentController@findById')->name('commenter-publication');
            Route::post('posts/{post}/comments', 'Api\CommentController@create')->name('commenter-publication');
            Route::post('posts/{post}/comments/{comment}/report', 'Api\CommentController@report')->name('commenter-publication');
            Route::put('posts/{post}/comments/{comment}', 'Api\CommentController@update')->name('commenter-publication');
            Route::patch('posts/{post}/comments/{comment}', 'Api\CommentController@bestAnswer')->name('epingler-publication');
            Route::delete('posts/{post}/comments/{comment}', 'Api\CommentController@delete')->name('commenter-publication');

            Route::post('posts/{post}/reports/{comment?}', 'Api\ReportController@create')->name('voir-publications');
            Route::get('posts/{post}/reports', 'Api\ReportController@findAll')->name('liste-signalisation');
            Route::delete('posts/{post}/reports/{report}', 'Api\ReportController@delete')->name('liste-signalisation');

            Route::get('categories', 'Api\CategoryController@findAll')->name('voir-publications');
            Route::post('categories', 'Api\CategoryController@create')->name('epingler-publication');
            Route::put('categories/{category}', 'Api\CategoryController@update')->name('epingler-publication');
            Route::put('posts/{post}/category/{category}', 'Api\CategoryController@addToPost')->name('epingler-publication');
            Route::delete('posts/{post}/category/{category}', 'Api\CategoryController@removeToPost')->name('epingler-publication');

            // Conversations

            Route::get('chats', 'Api\ChatController@findAll')->name('chatter');
            Route::post('chats', 'Api\ChatController@create')->name('chatter');
            Route::delete('chats/{chat}', 'Api\ChatController@delete')->name('chatter');
            Route::patch('chats/{chat}/leave', 'Api\ChatController@leave')->name('chatter');
            Route::get('chats/{chat}/messages', 'Api\ChatController@messages')->name('chatter');
            Route::post('chats/{chat}/messages', 'Api\ChatController@send')->name('chatter');
        });
    }
);
