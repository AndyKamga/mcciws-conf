<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Les routes suivantes gèrent les pages du front-office

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

// Accueil
Route::get('/', 'FrontOfficeController@accueil');
Route::get('accueil', 'FrontOfficeController@accueil');

// Mots du responsable
Route::get('mots-du-responsable', 'FrontOfficeController@motsDuResponsable');

// Témoignages
Route::get('temoignages', 'FrontOfficeController@temoignages');

// Programme de la formation
Route::get('programme', 'FrontOfficeController@programme');

// Candidater
Route::get('candidater', 'FrontOfficeController@candidater');

// Entreprise
Route::get('entreprises', 'FrontOfficeController@entreprises');
Route::post('envoiOffreFichier', 'FrontOfficeController@envoiOffreFichier');
Route::post('envoiOffreTexte', 'FrontOfficeController@envoiOffreTexte');

// Projet des étudiants
Route::get(
    'projets-des-etudiants',
    'FrontOfficeController@projetsDesEtudiants'
);

// Nous trouver
Route::get('nous-trouver', 'FrontOfficeController@nousTrouver');
Route::post('envoi-formulaire', 'FrontOfficeController@envoiDemande');

// Mentions légales
Route::get('mentions-legales', 'FrontOfficeController@mentionsLegales');

// S'enregistrer
Route::get('enregistrement', 'FrontOfficeController@enregistrement');
Route::post('enregistrement', 'FrontOfficeController@verificationEnreg');

/***************************************************************************
 *
 * Toutes les routes après Auth::routes() nécessitent l'authentification
 * pour y accèder. Elles correspondent aux routes d'accès à l'espace privé.
 * TOUTES LES ROUTES DOIVENT AVOIR UN NOM (->name(nom))
 ***************************************************************************/

Auth::routes();

// Accueil

Route::get('home', 'HomeController@index');

// Gestion des utilisateurs

Route::group(
    [
        'middleware' => ['userAuthorization'],
    ],
    function () {
        Route::get(
            'gestion-des-utilisateurs',
            'GestionUtilisateursController@index'
        )->name('gestion-des-utilisateurs');
        Route::post(
            'ajouter-utilisateur',
            'GestionUtilisateursController@add'
        )->name('gestion-des-utilisateurs');
        Route::post(
            'modifier-utilisateur',
            'GestionUtilisateursController@update'
        )->name('gestion-des-utilisateurs');
    }
);

// Gestion des droits

Route::group(
    [
        'middleware' => ['userAuthorization'],
    ],
    function () {
        Route::get('gestion-des-droits', 'GestionDroitsController@index')
            ->name('gestion-des-droits');

        Route::post('ajouter-un-droit', 'GestionDroitsController@add')
            ->name('gestion-des-droits');

        Route::post('modifier-un-droit', 'GestionDroitsController@update')
            ->name('gestion-des-droits');
    }
);

// Gestion des inscriptions

Route::group(
    [
        'middleware' => ['userAuthorization'],
    ],
    function () {
        Route::get(
            'gestion-des-inscriptions',
            'GestionInscriptionsController@index'
        )->name('gestion-des-inscriptions');
        Route::post(
            'modifier-inscription',
            'GestionInscriptionsController@update'
        )->name('gestion-des-inscriptions');
    }
);

// Mon profil

Route::group(
    [
        'middleware' => ['userAuthorization'],
    ],
    function () {
        Route::get('mon-profil', 'MonProfilController@index')
            ->name('mon-profil');
        ;
        Route::post('mettre-a-jour-profil', 'MonProfilController@update')
            ->name('mon-profil');
        ;
        Route::post('mettre-a-jour-mdp', 'MonProfilController@updatePassword')
            ->name('mon-profil');
    }
);

// Dates importantes

Route::group(
    [
        'middleware' => ['userAuthorization'],
    ],
    function () {
        Route::get(
            'gestion-des-dates-importantes',
            'DatesImportantesController@index'
        )->name('gestion-des-dates-importantes');
        Route::post(
            'mettre-a-jour-dates',
            'DatesImportantesController@update'
        )->name('gestion-des-dates-importantes');
    }
);

// Gestion des offres de stage

Route::group(
    [
        'middleware' => ['userAuthorization'],
    ],
    function () {
        Route::get(
            'gestion-offres-de-stage',
            'GestionOffresStageController@index'
        )->name('gestion-offres-de-stage');
        Route::post(
            'mettre-a-jour-offre',
            'GestionOffresStageController@update'
        )->name('gestion-offres-de-stage');
        Route::post(
            'desactiver-offre',
            'GestionOffresStageController@disable'
        )->name('gestion-offres-de-stage');
    }
);

// Offre de stage

Route::group(
    [
        'middleware' => ['userAuthorization'],
    ],
    function () {
        Route::get('voir-offres-de-stage', 'OffresController@index')
            ->name('voir-offres-de-stage');
        Route::post('confirmer-offre', 'OffresController@update')
            ->name('voir-offres-de-stage');
    }
);

// Déposer un rapport de stage

Route::group(
    [
        'middleware' => ['userAuthorization'],
    ],
    function () {
        Route::get('mon-rapport', 'MonRapportController@index')
            ->name('mon-rapport');
        Route::post('poster-rapport', 'MonRapportController@update')
            ->name('mon-rapport');
    }
);

// Poster un témoignage

Route::group(
    [
        'middleware' => ['userAuthorization'],
    ],
    function () {
        Route::get('mon-temoignage', 'MonTemoignageController@index')
            ->name('mon-temoignage');
        Route::post(
            'poster-temoignage',
            'MonTemoignageController@update'
        )->name('mon-temoignage');
    }
);

// Gestion des témoignages

Route::group(
    [
        'middleware' => ['userAuthorization'],
    ],
    function () {
        Route::get(
            'gestion-des-temoignages',
            'GestionTemoignagesController@index'
        )->name('gestion-des-temoignages');
        Route::post(
            'valider-temoignage',
            'GestionTemoignagesController@update'
        )->name('gestion-des-temoignages');
        Route::post(
            'desactiver-temoignage',
            'GestionTemoignagesController@disable'
        )->name('gestion-des-temoignages');
    }
);

// Gestion des rapports de stage

Route::group(
    [
        'middleware' => ['userAuthorization'],
    ],
    function () {
        Route::get(
            'gestion-rapports-stages',
            'GestionRapportsController@index'
        )->name('gestion-rapports-stages');
        Route::post(
            'rendre-visible-stage',
            'GestionRapportsController@update'
        )->name('gestion-rapports-stages');
        Route::post(
            'rendre-invisible-stage',
            'GestionRapportsController@disable'
        )->name('gestion-rapports-stages');
    }
);

// Rapport de stage

Route::group(
    [
        'middleware' => ['userAuthorization'],
    ],
    function () {
        Route::get('rapports-stage-anciens', 'RapportStagesController@index')
            ->name('rapports-stage-anciens');
    }
);

// Gestion situation des anciens

Route::group(
    [
        'middleware' => ['userAuthorization'],
    ],
    function () {
        Route::get(
            'gestion-situation-anciens',
            'GestionSituationController@index'
        )->name('gestion-situation-anciens');
        Route::post(
            'valider-situation',
            'GestionSituationController@update'
        )->name('gestion-situation-anciens');
        Route::post(
            'desactiver-situation',
            'GestionSituationController@disable'
        )->name('gestion-situation-anciens');
    }
);

// Gestion des projets des étudiants

Route::group(
    [
        'middleware' => ['userAuthorization'],
    ],
    function () {
        Route::get('gestion-des-projets', 'ProjetsController@index')
            ->name('gestion-des-projets');
        Route::post('ajout-projet', 'ProjetsController@add')
            ->name('gestion-des-projets');
        Route::post('modifier-projet', 'ProjetsController@update')
            ->name('gestion-des-projets');
        Route::post('supprimer-projet', 'ProjetsController@delete')
            ->name('gestion-des-projets');
    }
);

// Messagerie

Route::group(
    [
        'middleware' => ['userAuthorization'],
    ],
    function () {
        Route::get('messagerie', 'MessagerieController@index')
            ->name('messagerie');
        Route::get(
            'historique-des-mails',
            'MessagerieController@historique'
        )->name('messagerie');
        Route::post('envoyer-mail', 'MessagerieController@add')
            ->name('messagerie');
        Route::post('supprimer-message', 'MessagerieController@delete')
            ->name('messagerie');
    }
);

//

Route::group(
    [
        'middleware' => ['userAuthorization'],
    ],
    function () {
        Route::get('mise-a-jour-site', 'MiseAJourController@index')
            ->name('mise-a-jour-site');
        Route::post('commande-auto', 'MiseAJourController@update')
            ->name('mise-a-jour-site');
        Route::post('commande-site', 'MiseAJourController@commande')
            ->name('mise-a-jour-site');
        Route::post('commande-laravel', 'MiseAJourController@laravel')
            ->name('mise-a-jour-site');
        Route::post('commande-artisan', 'MiseAJourController@artisan')
            ->name('mise-a-jour-site');
    }
);
