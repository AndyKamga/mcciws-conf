<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;

class AppInstallProd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:install:prod';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to install app dependencies in production';

    /**
     * Process timeout.
     *
     * @var int
     */
    private $commands = [];

    /**
     * Process timeout.
     *
     * @var int
     */
    private $timeout = 1200;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->commands = [
            'composer install',
            'composer dumpautoload',
            'php artisan migrate',
            'php artisan passport:install --force',
            'php artisan storage:link',
            'php artisan cache:clear',
            'php artisan config:clear',
            'php artisan key:generate',
            'php artisan l5-swagger:generate'
        ];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        printf('Installation des dépendances du projet sur le serveur...');

        $this->runCommands();

        printf('Installation du projet terminé !');

        return null;
    }

    public function runCommands()
    {
        foreach ($this->commands as $command) {
            $process = Process::fromShellCommandline($command);
            $process->setTimeout($this->timeout);
            $process->start();
            $process->wait(function ($type, $buffer) {
                printf($buffer);
            });
        }
    }
}
