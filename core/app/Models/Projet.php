<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Projet extends Model
{
    public static function dateProjetsEtudiants()
    {
        return DB::select('SELECT DISTINCT date FROM projets ORDER BY date DESC');
    }

    public static function projetsEtudiants($annee)
    {
        if ($annee == 0) {
            return DB::select('SELECT titre, description, langage_competence, date FROM projets ORDER BY RAND() LIMIT 0, 40');
        }
        return DB::select("SELECT titre, description, langage_competence FROM projets WHERE date ='$annee' LIMIT 0, 40");
    }

    public static function add($titre, $description, $langage_competence, $date)
    {
        $projets = new Projet;

        $projets->titre = ucfirst($titre);
        $projets->description = ucfirst($description);
        $projets->langage_competence = ucfirst($langage_competence);
        $projets->date = $date;
        $projets->save();
    }
}
