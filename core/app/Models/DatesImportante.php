<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DatesImportante extends Model
{
    public static function findAll()
    {
        return DB::select('SELECT cle, day(date) as jour,
            month(date) as mois, year(date) as annee FROM dates_importantes');
    }

    public static function findAllListed()
    {
        $liste_dates = [];
        $request = DatesImportante::all();
        foreach ($request as $result) {
            array_push($liste_dates, $result->date);
        }
        return $liste_dates;
    }

    public static function updateDatesImportantes(
        $datePreRentree,
        $dateRentree,
        $dateOuverture,
        $dateLimite,
        $dateRapport,
        $dateChangementCompte
    ) {
        DatesImportante::where('cle', 'debut_candidature')->update(
            ['date' => $dateOuverture]
        );

        DatesImportante::where('cle', 'prerentree')->update(
            ['date' => $datePreRentree]
        );

        DatesImportante::where('cle', 'debut_cours')->update(
            ['date' => $dateRentree]
        );

        DatesImportante::where('cle', 'fin_candidature')->update(
            ['date' => $dateLimite]
        );

        DatesImportante::where('cle', 'depot_rapports')->update(
            ['date' => $dateRapport]
        );

        DatesImportante::where('cle', 'changement_compte')->update(
            ['date' => $dateChangementCompte]
        );
    }
}
