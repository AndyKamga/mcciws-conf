<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * @property mixed|string contact
 * @property mixed|string titre
 * @property mixed|string entreprise
 * @property mixed|string localisation
 * @property mixed fichier
 * @property mixed|string description
 * @property int|mixed statut
 * @property mixed id_offre
 * @property mixed debut
 * @property mixed telephone
 * @property mixed mail
 * @property mixed fin
 */
class Offre extends Model
{
    public static function ajouterOffreFichier(
        $titre,
        $entreprise,
        $localisation,
        $fichier
    ) {
        $titre = ucfirst($titre);
        $entreprise = ucfirst($entreprise);
        $localisation = ucfirst($localisation);

        $offre = new Offre;
        $offre->titre = $titre;
        $offre->entreprise = $entreprise;
        $offre->localisation = $localisation;
        $offre->fichier = $fichier;
        $offre->contact = 'Voir pièce jointe';
        $offre->description = 'Voir pièce jointe';
        $offre->statut = 0;

        $offre->save();
    }

    public static function ajouterOffreTexte(
        $titre,
        $entreprise,
        $localisation,
        $debut,
        $fin,
        $contact,
        $mail,
        $telephone,
        $description
    ) {
        $titre = ucfirst($titre);
        $entreprise = ucfirst($entreprise);
        $localisation = ucfirst($localisation);

        $offre = new Offre;
        $offre->titre = $titre;
        $offre->entreprise = $entreprise;
        $offre->localisation = $localisation;
        $offre->debut = $debut;
        $offre->fin = $fin;
        $offre->contact = $contact;
        $offre->mail = $mail;
        $offre->telephone = $telephone;
        $offre->description = $description;
        $offre->statut = 0;

        $offre->save();
    }

    /*------ Gestion Offres ----*/

    public static function validerOffre($id_offre)
    {
        DB::update("UPDATE offres SET statut=1 WHERE id_offre=$id_offre");
    }

    public static function desactiverOffre($id_offre)
    {
        DB::update("UPDATE offres SET statut=0 WHERE id_offre=$id_offre");
    }

    public static function confirmerOffre($id_offre, $id_user)
    {
        DB::update("UPDATE offres SET statut=2, id_user=$id_user WHERE id_offre=$id_offre");
    }

    public static function supprimerOffre($id_offre)
    {
        DB::delete("DELETE FROM offres WHERE id_offre=$id_offre");
    }

    public static function getOffres()
    {
        $request = DB::select('SELECT * FROM offres LEFT JOIN users ON users.id =offres.id_user');
        return $request;
    }

    public static function getvalidOffres()
    {
        $request = DB::select('SELECT * FROM offres WHERE statut=1');
        return $request;
    }

    public static function getOffresValidNumber()
    {
        return Offre::where('statut', 1)->count();
    }

    public static function getOffresNONValidNumber()
    {
        return Offre::where('statut', 0)->count();
    }

    public static function getOffresPourvueNumber()
    {
        return Offre::where('statut', 2)->count();
    }
}
