<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Demande extends Model
{
    public static function ajouterDemande($nom, $email, $sujet, $description)
    {
        $sujet = ucfirst($sujet);

        $demandes = new Demande;
        $demandes->nom = $nom;
        $demandes->email = $email;
        $demandes->sujet = $sujet;
        $demandes->description = $description;

        $demandes->save();
    }
}
