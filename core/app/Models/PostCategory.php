<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

/**
 * @property string name
 * @property string description
 * @method static where(string $string, string $string1, string $string2)
 * @method static orderBy(string $string, string $string1)
 * @method static create(array $array)
 * @method static find($categoryId)
 */
class PostCategory extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description'];

    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }
}
