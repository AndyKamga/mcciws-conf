<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;

/**
 * @property mixed prenom
 * @property mixed nom
 * @property mixed photo
 * @property mixed email
 * @property mixed droit
 * @property mixed id
 * @method static where(string $string, $user)
 * @method static whereIn(string $string, array|string|null $input)
 * @method static find($user_id)
 */
class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function droits()
    {
        return $this->belongsTo(Droit::class, 'droits', 'id_droit');
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function postLikes()
    {
        return $this->hasMany(PostLike::class);
    }

    public function postComments()
    {
        return $this->hasMany(PostComment::class);
    }

    public function chats()
    {
        return $this->hasMany(Chat::class);
    }

    /*-------------------------------------*/
    //        Requêtes personnalisées
    /*------------------------------------*/

    public static function getAllUser()
    {
        $request = DB::select('SELECT id, nom, prenom, email, annee, description FROM users INNER JOIN droits ON
         users.droits = droits.id_droit  WHERE users.droits != 5');
        return $request;
    }

    public static function getAccount()
    {
        $request = DB::select('SELECT * FROM droits WHERE id_droit != 5');
        return $request;
    }

    public static function getUserNumber()
    {
        $request = DB::select('SELECT COUNT(*) as userNbr FROM users WHERE droits != 5');
        return $request[0]->userNbr;
    }

    public static function getStudentNumber()
    {
        $request = DB::select('SELECT COUNT(*) as userNbr FROM users WHERE droits NOT IN (0, 1, 4, 5)');
        return $request[0]->userNbr;
    }

    public static function getOldStudentNumber()
    {
        $request = DB::select('SELECT COUNT(*) as userNbr FROM users WHERE droits=1');
        return $request[0]->userNbr;
    }

    public static function getAdminNumber()
    {
        $request = DB::select('SELECT COUNT(*) as userNbr FROM users WHERE droits NOT IN (0, 1, 2, 5)');
        return $request[0]->userNbr;
    }

    public static function getInactifNumber()
    {
        $request = DB::select('SELECT COUNT(*) as userNbr FROM users WHERE droits=0');
        return $request[0]->userNbr;
    }

    /*--------- Situation -------*/

    public static function getSituation()
    {
        $request = DB::select('SELECT id, nom, prenom, email, annee, poste_occupe, nom_entr, statut FROM ((users
        INNER JOIN postes ON users.id = postes.id_user) INNER JOIN entreprises ON postes.id_entr=entreprises.id_entr)
        WHERE postes.poste_occupe IS NOT NULL AND postes.id_entr IS NOT NULL');
        return $request;
    }

    public static function getSituationValidNumber()
    {
        $request = DB::select('SELECT COUNT(*) as nbr  FROM postes WHERE statut=1');
        return $request[0]->nbr;
    }

    public static function getSituationNonValidNumber()
    {
        $request = DB::select('SELECT COUNT(*) as nbr FROM postes WHERE
        poste_occupe IS NOT NULL AND id_entr IS NOT NULL AND statut != 1');

        return $request[0]->nbr;
    }

    public static function disableUserSituation($id_user)
    {
        DB::update("UPDATE postes SET statut=0 WHERE id_user=$id_user");
    }

    public static function hasRapport($id_user)
    {
        // Attention avant de modifier ceci !!!
        return Rapport::where('id_user', $id_user)->count();
    }

    public static function hasTemoignage($id_user)
    {
        return Temoignage::where('id_user', $id_user)->count() > 0;
    }
}
