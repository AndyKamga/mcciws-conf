<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Rapport extends Model
{
    public static function findAllDepo()
    {
        return DB::select('SELECT * FROM (((users 
        INNER JOIN rapports ON users.id = rapports.id_user) INNER JOIN stages ON 
        rapports.id_stage = stages.id_stage) INNER JOIN entreprises ON stages.id_entr=entreprises.id_entr)
        ');
    }

    public static function findDepo()
    {
        $result = DB::select('SELECT * FROM (((users 
        INNER JOIN rapports ON users.id = rapports.id_user) INNER JOIN stages ON 
        rapports.id_stage = stages.id_stage) INNER JOIN entreprises ON stages.id_entr=entreprises.id_entr)
        WHERE rapports.confidentialite=0');
        return $result;
    }

    public static function getRapportDepoUser($id_user)
    {
        $result = DB::select("SELECT fichier, date FROM rapports WHERE id_user = $id_user");
        return $result;
    }

    public static function getRapportDepoNUMBER()
    {
        $result = DB::select('SELECT COUNT(*) as nbr FROM users 
        INNER JOIN rapports ON users.id = rapports.id_user');
        return $result[0]->nbr;
    }

    public static function findAllNONDepo()
    {
        $result = DB::select('SELECT nom, prenom, email, annee FROM users WHERE droits NOT IN (0, 1, 4, 5) AND 
        id NOT IN (SELECT id_user FROM rapports)');
        return $result;
    }

    public static function getRapportNONDepoNUMBER()
    {
        $result = DB::select('SELECT COUNT(*) as nbr FROM users WHERE droits NOT IN (0, 1, 4, 5) AND 
        id NOT IN (SELECT id_user FROM rapports)');
        return $result[0]->nbr;
    }

    public static function deactiveInfoStage($id_stage)
    {
        DB::update("UPDATE stages SET statut = 0 WHERE id_stage=$id_stage");
    }
}
