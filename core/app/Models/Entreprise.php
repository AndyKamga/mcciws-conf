<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Entreprise extends Model
{
    public static function entreprises()
    {
        return DB::select('SELECT sujet_stage, nom_entr FROM stages, entreprises 
        WHERE stages.id_entr=entreprises.id_entr AND stages.statut = 1 ORDER BY RAND() LIMIT 40');
    }

    public static function situations()
    {
        return DB::select('SELECT postes.poste_occupe, entreprises.nom_entr FROM postes, entreprises 
        WHERE postes.id_entr = entreprises.id_entr AND postes.statut = 1 ORDER BY RAND() LIMIT 40');
    }
}
