<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Temoignage extends Model
{
    public static function findSummaryTestimony()
    {
        /*
         * $request = DB::select('SELECT prenom, nom, poste_occupe as metier, resume FROM temoignages inner join users
        on temoignages.id_user=users.id INNER JOIN postes on users.id = postes.id_user WHERE temoignages.statut=1 ORDER BY RAND() LIMIT 4');
         */
        return DB::table('temoignages')
            ->join('users', 'temoignages.id_user', '=', 'users.id')
            ->join('postes', 'users.id', '=', 'postes.id_user')
            ->where('temoignages.statut', '=', 1)
            ->inRandomOrder()
            ->limit(4)
            ->get(
                [
                    'users.prenom',
                    'users.nom',
                    'postes.poste_occupe as metier',
                    'temoignages.resume'
                ]
            );
    }

    public static function findTestimony()
    {
        /*
         * $request = DB::select('SELECT nom, prenom, poste_occupe as metier, description FROM temoignages INNER JOIN users
        ON temoignages.id_user = users.id INNER JOIN postes on users.id = postes.id_user WHERE temoignages.statut=1');
         */
        return DB::table('temoignages')
            ->join('users', 'temoignages.id_user', '=', 'users.id')
            ->join('postes', 'users.id', '=', 'postes.id_user')
            ->where('temoignages.statut', '=', 1)
            ->inRandomOrder()
            ->limit(4)
            ->get(
                [
                    'users.prenom',
                    'users.nom',
                    'postes.poste_occupe as metier',
                    'temoignages.resume',
                    'temoignages.description'
                ]
            );
    }

    public static function getTemoignages()
    {
        $request = DB::select('SELECT nom, prenom, email, annee, temoignages.statut, resume, poste_occupe as metier,
        description, id_temoignage, postes.id_user as id_user FROM users INNER JOIN temoignages ON users.id = temoignages.id_user 
        INNER JOIN postes ON temoignages.id_user = postes.id_user');

        return $request;
    }

    public static function getTemoignagesValidNumber()
    {
        return Temoignage::where('statut', 1)->count();
    }

    public static function getTemoignagesNonValidNumber()
    {
        return Temoignage::where('statut', 0)->count();
    }

    public static function validerTemoignage($id)
    {
        DB::update("UPDATE temoignages SET statut=1 WHERE id_temoignage=$id");
    }

    public static function desactiverTemoignage($id)
    {
        $request = DB::update("UPDATE temoignages SET statut=0 WHERE id_temoignage=$id");
    }

    /*------------------- USER temoignage ---------------*/

    public static function getStatutTemoign($id_user)
    {
        $request
            = DB::select("SELECT statut FROM temoignages WHERE id_user=$id_user");
        return $request[0]->statut;
    }

    public static function getTemoignageUser($id_user)
    {
        $request = DB::select("SELECT poste_occupe as metier, resume, description FROM temoignages 
        INNER JOIN postes ON temoignages.id_user = postes.id_user WHERE temoignages.id_user=$id_user");

        /*
        Un traitement particulier est appliqué au résultat de la requête dans le cas actuel.
        Pour éviter un bug si jamais la requête n'a pas de résultat
        */
        if (count(array_keys($request)) > 0) {
            return [
                'metier' => $request[0]->metier,
                'resume' => $request[0]->resume,
                'description' => $request[0]->description
            ];
        }

        return ['metier' => '', 'resume' => '', 'description' => ''];
    }

    public static function getStatutTemoignageUser($id_user, $existUser)
    {
        if ($existUser > 0) {
            $request
                = DB::select("SELECT statut FROM temoignages WHERE id_user=$id_user");
            return $request[0]->statut;
        } else {
            // On retourne un chiffre différent de 0 ou de 1.
            // 1992 a été choisi aléatoirement
            return 1992;
        }
    }
}
