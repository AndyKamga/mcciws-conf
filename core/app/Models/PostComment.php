<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

/**
 * @property mixed id
 * @property string html_text
 * @property string post_id
 * @property integer user_id
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed is_best_answer
 * @method static where(string $string, $postId)
 */
class PostComment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['html_text'];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
