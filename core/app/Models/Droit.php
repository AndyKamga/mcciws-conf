<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Route;

/**
 * @method static latest()
 * @method static where(string $string, $droits)
 */
class Droit extends Model
{
    public static function getRoutes()
    {
        $routeToIgnore = [
            0 => 'passport.authorizations.authorize',
            1 => 'passport.authorizations.approve',
            2 => 'passport.authorizations.deny',
            3 => 'passport.token',
            4 => 'passport.tokens.index',
            5 => 'passport.tokens.destroy',
            6 => 'passport.token.refresh',
            7 => 'passport.clients.index',
            8 => 'passport.clients.store',
            9 => 'passport.clients.update',
            10 => 'passport.clients.destroy',
            11 => 'passport.scopes.index',
            12 => 'passport.personal.tokens.index',
            13 => 'passport.personal.tokens.store',
            14 => 'passport.personal.tokens.destroy',
            15 => 'password.request',
            16 => 'password.email',
            17 => 'password.reset',
            18 => 'password.update',
            20 => 'l5-swagger.api',
            21 => 'l5-swagger.docs',
            22 => 'l5-swagger.asset',
            23 => 'l5-swagger.oauth2_callback',
            24 => 'login',
            25 => 'register'
        ];

        $routesByName = Route::getRoutes()->getRoutesByName();

        return array_diff(array_keys($routesByName), $routeToIgnore);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
