<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Inscription extends Model
{
    public static function addToQueue(
        $nom,
        $prenom,
        $email,
        $description,
        $annee,
        $password
    ) {
        $nom = ucfirst($nom);
        $prenom = ucfirst($prenom);

        $inscriptions = new Inscription;
        $inscriptions->nom = $nom;
        $inscriptions->prenom = $prenom;
        $inscriptions->email = $email;
        $inscriptions->annee = $annee;
        $inscriptions->password = $password;
        $inscriptions->description = $description;

        $inscriptions->save();
    }

    public static function isInQueue($email)
    {
        return Inscription::where('email', $email)->count() > 0;
    }

    public static function getAccountMin()
    {
        $request = DB::select('SELECT * FROM droits WHERE id_droit NOT IN(0, 3, 4, 5)');

        return $request;
    }
}
