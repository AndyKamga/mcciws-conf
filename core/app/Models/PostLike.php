<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

/**
 * @property number user_id
 * @property number post_id
 * @method static where(string $string, $id)
 * @method static create(array $array)
 */
class PostLike extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'post_id'];

    public function getValidator()
    {
        $data = [
            'user_id' => $this->user_id,
            'post_id' => $this->post_id
        ];

        $rules = [
            'user_id' => ['required', 'numeric'],
            'post_id' => ['required', 'numeric'],
        ];

        $rulesMessage = [
            'user_id' => 'Votre like doit être associé à un utilisateur',
            'post_id' => 'Votre like doit être associé à un post',
        ];

        return Validator::make($data, $rules, $rulesMessage);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
