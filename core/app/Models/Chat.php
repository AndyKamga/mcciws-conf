<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static create(array $array)
 * @method static where(string $string, $id)
 * @property mixed id
 * @property mixed name
 * @property mixed created_at
 */
class Chat extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function members()
    {
        return $this->belongsTo(ChatMember::class, 'chat_members');
    }

    public function messages()
    {
        return $this->belongsTo(ChatMessage::class);
    }
}
