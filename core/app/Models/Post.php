<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed id
 * @property mixed title
 * @property mixed html_text
 * @property string iframe_code
 * @property boolean is_pinned
 * @property integer user_id
 * @property mixed created_at
 * @property mixed updated_at$
 * @property mixed is_solved
 * @method static find($id)
 * @method static paginate($i)
 * @method static orderBy(string $string, string $string1)
 * @method static where(string $string, false $false)
 */
class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'html_text', 'iframe_code', 'is_pinned'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function categories()
    {
        return $this->belongsToMany(PostCategory::class);
    }

    public function likes()
    {
        return $this->hasMany(PostLike::class);
    }

    public function files()
    {
        return $this->hasMany(PostFile::class);
    }

    public function comments()
    {
        return $this->hasMany(PostComment::class);
    }

    public function reports()
    {
        return $this->hasMany(PostReport::class);
    }
}
