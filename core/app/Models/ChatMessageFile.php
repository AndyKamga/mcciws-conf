<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static where(string $string, $id)
 */
class ChatMessageFile extends SocialFile
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'path', 'media_type', 'chat_message_id'];

    public function message()
    {
        return $this->hasOne(ChatMessage::class);
    }
}
