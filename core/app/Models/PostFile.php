<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class PostFile extends SocialFile
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'path', 'media_type', 'post_id'];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
