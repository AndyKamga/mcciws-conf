<?php


namespace App\Dto;

use App\Models\PostCategory;

/**
 * @OA\Schema(
 *   schema="Categorie",
 *   @OA\Property(property="id", type="number", example=2),
 *   @OA\Property(property="name", type="string", example="Nom de la catégorie"),
 *   @OA\Property(property="description", type="string", example="Description de la catégorie"),
 * )
 */
class PostCategoryDto
{
    public $id;
    public $name;
    public $description;

    public function __construct(PostCategory $postCategory)
    {
        if ($postCategory != null) {
            $this->id = $postCategory->id;
            $this->name = $postCategory->name;
            $this->description = $postCategory->description;
        }
    }
}
