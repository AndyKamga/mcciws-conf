<?php


namespace App\Dto;

use App\Models\ChatMessage;
use App\Models\ChatMessageFile;
use Illuminate\Support\Facades\Crypt;

/**
 * @OA\Schema(
 *   schema="Message",
 *   @OA\Property(property="text", type="string", example="Bonjour à tous"),
 *   @OA\Property(property="created_at", type="string", example="2020-09-26 12:49:53"),
 *   @OA\Property(property="is_read", type="boolean", example=true),
 *   @OA\Property(property="is_own_message", type="boolean", example=false),
 *   @OA\Property(property="message_sender", ref="#/components/schemas/Utilisateur"),
 *   @OA\Property(property="attachments", type="array", @OA\Items(ref="#/components/schemas/Fichier"))
 * )
 */
class ChatMessageDto
{
    public $text;
    public $created_at;
    public $is_read;
    public $is_own_message;
    public $message_sender;
    public $attachments = [];

    public function __construct(ChatMessage $chatMessage)
    {
        if ($chatMessage) {
            $user = $chatMessage->user()->first();
            $this->text = $chatMessage->text;
            $this->created_at = date($chatMessage->created_at);
            $this->is_read = $chatMessage->is_read == false;
            $this->message_sender = new UserDto($user);
            $this->is_own_message = $user->id == request()->user()->id;

            foreach (ChatMessageFile::where('chat_message_id', $chatMessage->id)->get() as $messageFile) {
                array_push($this->attachments, new SocialFileDto($messageFile));
            }
        }
    }
}
