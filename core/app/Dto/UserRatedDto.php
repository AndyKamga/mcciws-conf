<?php


namespace App\Dto;

use App\Models\User;

/**
 * @OA\Schema(
 *   schema="TopUser",
 *   @OA\Property(property="problems_solved", type="number", example=160),
 *   @OA\Property(property="user", ref="#/components/schemas/Utilisateur")
 * )
 */
class UserRatedDto
{
    public $problems_solved;
    public $user;

    public function __construct($result)
    {
        $this->problems_solved = $result->total;
        $this->user = new UserDto(User::find($result->user_id));
    }
}
