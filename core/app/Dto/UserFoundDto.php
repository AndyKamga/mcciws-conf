<?php


namespace App\Dto;

/**
 * @OA\Schema(
 *   schema="UserFound",
 *   @OA\Property(property="id", type="number", example=1),
 *   @OA\Property(property="first_name", type="string", example="John"),
 *   @OA\Property(property="last_name", type="string", example="Doe"),
 * )
 */
class UserFoundDto
{
    public $id;
    public $first_name;
    public $last_name;

    public function __construct($user)
    {
        if ($user != null) {
            $this->id = $user->id;
            $this->first_name = $user->prenom;
            $this->last_name = $user->nom;
        }
    }
}
