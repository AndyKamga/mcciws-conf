<?php


namespace App\Dto;

use App\Models\Offre;

/**
 * @OA\Schema(
 *   schema="Offre",
 *   @OA\Property(property="id", type="number", example=54),
 *   @OA\Property(property="title", type="string", example="Titre du stage"),
 *   @OA\Property(property="description", type="string", example="Description du stage"),
 *   @OA\Property(property="start", type="string", example="2021-04-05"),
 *   @OA\Property(property="end", type="string", example="2021-08-31"),
 *   @OA\Property(property="compagny", type="string", example="Nom de l'entreprise"),
 *   @OA\Property(property="location", type="string", example="Localisation"),
 *   @OA\Property(property="attachement", type="string", example="offres_de_stage/jvoFHRITCEyGjNeE1Ohwij3LDuzsKvdujFQ9koqn.pdf"),
 *   @OA\Property(property="contact", type="string", example="Nom du contact"),
 *   @OA\Property(property="email", type="string", example="contact@entreprise.fr"),
 *   @OA\Property(property="phone", type="number", example=123456789)
 * )
 */
class InternShipDto
{
    public $id;
    public $title;
    public $description;
    public $start;
    public $end;
    public $company;
    public $location;
    public $attachement;
    public $contact;
    public $email;
    public $phone;

    public function __construct($offre)
    {
        if ($offre != null) {
            $this->id = $offre->id_offre;
            $this->title = $offre->titre;
            $this->description = $offre->description;
            $this->start = $offre->debut;
            $this->end = $offre->fin;
            $this->company = $offre->entreprise;
            $this->location = $offre->localisation;
            $this->contact = $offre->contact;
            $this->email = $offre->mail;
            $this->phone = $offre->telephone;

            if ($offre->fichier) {
                $this->attachement = request()->getSchemeAndHttpHost().'/storage/'.$offre->fichier;
            }
        }
    }
}
