<?php


namespace App\Dto;

use App\Models\Chat;
use App\Models\ChatMember;

/**
 * @OA\Schema(
 *   schema="Conversation",
 *   @OA\Property(property="id", type="number", example=1),
 *   @OA\Property(property="name", type="string", example="John Doe"),
 *   @OA\Property(property="new_messages", type="number", example=0),
 *   @OA\Property(property="chat_owner", ref="#/components/schemas/Utilisateur"),
 *   @OA\Property(property="created_at", type="string", example="2020-09-26 12:49:53"),
 *   @OA\Property(property="chat_members", type="array", @OA\Items(ref="#/components/schemas/Utilisateur")),
 *   @OA\Property(property="last_message", ref="#/components/schemas/Message")
 * )
 */
class ChatDto
{
    public $id;
    public $name;
    public $new_messages;
    public $chat_owner;
    public $created_at;
    public $chat_members = [];
    public $last_message;

    public function __construct(Chat $chat, $members = null, $new_messages = null)
    {

        if ($chat != null) {
            $this->id = $chat->id;
            $this->name = $chat->name;
            $this->new_messages = $new_messages != null ? $new_messages : 0;
            $this->chat_owner = new UserDto($chat->user()->first());
            $this->created_at = date($chat->created_at);

            $last_message = $chat->messages()->orderBy('id', 'desc')->get();
            if ($last_message != null && count($last_message) > 0) {
                $this->last_message = new ChatMessageDto($last_message[0]);
            }

            if ($members != null) {
                foreach ($members as $member) {
                    array_push($this->chat_members, new UserDto($member));
                }
            } else {
                $members = ChatMember::where('chat_id', $chat->id)->get();

                foreach ($members as $member) {
                    array_push($this->chat_members, new UserDto($member->user()->first()));
                }
            }
        }
    }
}
