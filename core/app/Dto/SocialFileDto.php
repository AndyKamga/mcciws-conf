<?php


namespace App\Dto;

use App\Models\PostFile;
use App\Models\SocialFile;

/**
 * @OA\Schema(
 *   schema="Fichier",
 *   @OA\Property(property="name", type="string", example="file-sample_150kB.pdf"),
 *   @OA\Property(property="href", type="string", example="https://cci.univ-tours.fr/storage/post_files/wEeggDagJEIlCvCpdvkx1n5SX3ANJFWTqPrj8a29.pdf"),
 *   @OA\Property(property="media_type", type="string", example="application/pdf"),
 * )
 */
class SocialFileDto
{
    public $name;
    public $href;
    public $media_type;

    public function __construct(SocialFile $postFile)
    {

        if ($postFile != null) {
            $this->name = $postFile->name;
            $this->href = request()->getSchemeAndHttpHost() . '/storage/' .$postFile->path;
            $this->media_type = $postFile->media_type;
        }
    }
}
