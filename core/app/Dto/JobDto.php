<?php


namespace App\Dto;

/**
 * @OA\Schema(
 *   schema="Situation",
 *   @OA\Property(property="job", type="string", example="Ingénieur logiciel"),
 *  @OA\Property(property="compagny", type="string", example="Nom de l'entreprise")
 * )
 */
class JobDto
{
    public $job;
    public $compagny;

    public function __construct($situation)
    {
        $this->compagny = $situation['nom_entr'];
        $this->job = $situation['poste_occupe'];
    }
}
