<?php


namespace App\Dto;

use App\Models\Droit;

/**
 * @OA\Schema(
 *   schema="Utilisateur",
 *   @OA\Property(property="id", type="number", example=235),
 *   @OA\Property(property="first_name", type="string", example="John"),
 *   @OA\Property(property="last_name", type="string", example="Doe"),
 *   @OA\Property(property="email", type="string", example="john.doe@gmail.fr"),
 *   @OA\Property(property="avatar", type="string", example="https://cci.univ-tours.fr/storage/avatars/photo.png"),
 *   @OA\Property(property="role", type="string", example="Ancien")
 * )
 */
class UserDto
{
    public $id;
    public $first_name;
    public $last_name;
    public $email;
    public $avatar;
    public $role;

    public function __construct($user)
    {
        if ($user != null) {
            $this->id = $user->id;
            $this->first_name = $user->prenom;
            $this->last_name = $user->nom;
            $this->email = $user->email;
            $this->role = $user->droits()->first()->description;
            $this->avatar = request()->getSchemeAndHttpHost() . '/storage/' .$user->photo;
        }
    }
}
