<?php


namespace App\Dto;

use App\Models\User;

/**
 * @OA\Schema(
 *   schema="Profil",
 *   @OA\Property(property="user", ref="#/components/schemas/Utilisateur"),
 *   @OA\Property(property="professional_situation", ref="#/components/schemas/Situation"),
 *   @OA\Property(property="posts", type="array", @OA\Items(ref="#/components/schemas/Publication"))
 * )
 */
class SocialProfilDto
{
    public $user;
    public $professional_situation;
    public $posts;

    public function __construct(User $user, $posts, $situation)
    {
        $this->user =  new UserDto($user);
        $this->posts = $posts;
        $this->professional_situation = new JobDto($situation);
    }
}
