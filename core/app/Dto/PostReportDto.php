<?php


namespace App\Dto;

use App\Models\PostReport;

/**
 * @OA\Schema(
 *   schema="Signalisation",
 *   @OA\Property(property="id", type="number", example=1),
 *   @OA\Property(property="description", type="string", example="Description de la signalisation"),
 *   @OA\Property(property="created_at", type="string", example="2020-09-26 12:49:53"),
 *   @OA\Property(property="report_sender", ref="#/components/schemas/Utilisateur"),
 *   @OA\Property(property="post_id", type="number", example=15),
 *   @OA\Property(property="post_comment_id", type="number", example=30),
 * )
 */
class PostReportDto
{
    public $id;
    public $description;
    public $created_at;
    public $report_sender;
    public $post_id;
    public $post_comment_id;

    public function __construct(PostReport $postReport)
    {
        $user = $postReport->user()->first();

        if ($postReport != null) {
            $this->id = $postReport->id;
            $this->description = $postReport->description;
            $this->created_at = date($postReport->created_at);
            $this->report_sender = new UserDto($user);
            $this->post_id = $postReport->post_id;
            $this->post_comment_id = $postReport->post_comment_id;
        }
    }
}
