<?php


namespace App\Dto;

use App\Models\PostComment;

/**
 * @OA\Schema(
 *   schema="Commentaire",
 *   @OA\Property(property="id", type="number", example=1),
 *   @OA\Property(property="html_text", type="string", example="<p>Mon<br>commentaire</p>"),
 *   @OA\Property(property="is_best_answer", type="boolean", example=false),
 *   @OA\Property(property="created_at", type="string", example="2020-09-26 12:49:53"),
 *   @OA\Property(property="updated_at", type="string", example="2020-09-26 17:20:40"),
 *   @OA\Property(property="is_own_comment", type="boolean", example=true),
 *   @OA\Property(property="comment_sender", ref="#/components/schemas/Utilisateur"),
 *   @OA\Property(property="post_id", type="number", example=15),
 * )
 */
class PostCommentDto
{
    public $id;
    public $html_text;
    public $is_best_answer;
    public $created_at;
    public $updated_at;
    public $is_own_comment;
    public $comment_sender;
    public $post_id;

    public function __construct(PostComment $postComment)
    {
        $user = $postComment->user()->first();

        if ($postComment != null) {
            $this->id = $postComment->id;
            $this->html_text = $postComment->html_text;
            $this->is_best_answer = $postComment->is_best_answer == 1;
            $this->created_at = date($postComment->created_at);
            $this->updated_at = date($postComment->updated_at);
            $this->comment_sender = new UserDto($user);
            $this->is_own_comment = $user->id == request()->user()->id;
            $this->post_id = $postComment->post_id;
        }
    }
}
