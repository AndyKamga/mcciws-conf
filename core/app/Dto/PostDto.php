<?php


namespace App\Dto;

use App\Models\Post;

/**
 * @OA\Schema(
 *   schema="Publication",
 *   @OA\Property(property="id", type="number", example=1),
 *   @OA\Property(property="title", type="string", example="Title de la publication"),
 *   @OA\Property(property="html_text", type="string", example="<p>Description<br>de ma publication</p>"),
 *   @OA\Property(property="iframe_code", type="string", example="code d'intégration html"),
 *   @OA\Property(property="created_at", type="string", example="2020-09-26 12:49:53"),
 *   @OA\Property(property="is_solved", type="boolean", example=false),
 *   @OA\Property(property="is_pinned", type="boolean", example=false),
 *   @OA\Property(property="like_nbr", type="number", example=3),
 *   @OA\Property(property="comment_nbr", type="number", example=42),
 *   @OA\Property(property="categories", type="array", @OA\Items(ref="#/components/schemas/Categorie")),
 *   @OA\Property(property="is_own_post", type="boolean", example=true),
 *   @OA\Property(property="post_sender", ref="#/components/schemas/Utilisateur"),
 *   @OA\Property(property="attachments", type="array", @OA\Items(ref="#/components/schemas/Fichier"))
 * )
 */
class PostDto
{
    public $id;
    public $title;
    public $html_text;
    public $iframe_code;
    public $created_at;
    public $is_solved;
    public $is_pinned;
    public $like_nbr;
    public $comment_nbr;
    public $categories = [];
    public $is_own_post;
    public $post_sender;
    public $attachments = [];

    public function __construct(Post $post, $user = null)
    {
        if ($user == null) {
            $user  = $post->user()->first();
        }
        if ($post != null) {
            $this->id = $post->id;
            $this->title = $post->title;
            $this->html_text = $post->html_text;
            $this->iframe_code = $post->iframe_code;
            $this->created_at = date($post->created_at);
            $this->is_pinned = $post->is_pinned == 1;
            $this->is_solved = $post->is_solved == 1;
            $this->post_sender = new UserDto($user);
            $this->like_nbr = $post->likes()->count();
            $this->comment_nbr = $post->comments()->count();
            $this->is_own_post = $user->id == request()->user()->id;

            foreach ($post->files()->get() as $postFile) {
                array_push($this->attachments, new SocialFileDto($postFile));
            }

            foreach ($post->categories()->get() as $PostCategory) {
                array_push($this->categories, new PostCategoryDto($PostCategory));
            }
        }
    }
}
