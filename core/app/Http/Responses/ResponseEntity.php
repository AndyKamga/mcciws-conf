<?php


namespace App\Http\Responses;

class ResponseEntity
{
    public static function ok($body = null, $message = null)
    {
        if (!isset($body)) {
            $message = isset($message) ? $message : 'Ok';
            $body = ['message' => $message, 'uri' => request()->getUri(), 'code' => 200];
        }

        return response()->json($body, 200);
    }

    public static function created($body = null, $message = null)
    {
        if (!isset($body)) {
            $message = isset($message) ? $message : 'Created';
            $body = ['message' => $message, 'uri' => request()->getUri(), 'code' => 201];

            return response()->json($body, 201);
        }

        return response()->json([
            'uri' => request()->getUri(), 'method' => request()->getMethod(), 'code' => 201, 'data' => $body ], 201);
    }

    public static function noContent($body = null, $message = null)
    {
        if (!isset($body)) {
            $message = isset($message) ? $message : 'No content';
            $body = ['message' => $message, 'uri' => request()->getUri(), 'code' => 204];
        }

        return response()->json($body, 204);
    }

    public static function badRequest($body = null, $message = null)
    {
        if (!isset($body)) {
            $message = isset($message) ? $message : 'Bad request';
            $body = ['message' => $message, 'uri' => request()->getUri(), 'method' => request()->getMethod(), 'code' => 400];
        }
        return response()->json($body, 400);
    }

    public static function unauthorized($body = null, $message = null)
    {
        if (!isset($body)) {
            $message = isset($message) ? $message : 'Unauthorized';
            $body = ['message' => $message, 'uri' => request()->getUri(), 'method' => request()->getMethod(), 'code' => 401];
        }

        return response()->json($body, 401);
    }

    public static function notFound($body = null, $message = null)
    {
        $message = isset($message) ? $message : 'Not found';
        if (!isset($body)) {
            $body = ['message' => $message, 'uri' => request()->getUri(), 'method' => request()->getMethod(), 'code' => 404];
        }

        return response()->json($body, 404);
    }
}
