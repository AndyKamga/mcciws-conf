<?php

namespace App\Http\Controllers;

use App\Services\ReportService;

class MonRapportController extends Controller
{
    // Mon rapport de stage

    /**
     * @var ReportService
     */
    private $reportService;

    public function __construct(ReportService $reportService)
    {
        $this->middleware('auth');
        $this->reportService = $reportService;
    }

    public function index()
    {
        $id_user = auth()->user()->id;
        return $this->reportService->findAll($id_user);
    }

    public function update()
    {
        return $this->reportService->update();
    }
}
