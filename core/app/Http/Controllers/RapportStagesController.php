<?php

namespace App\Http\Controllers;

use App\Models\Rapport;

class RapportStagesController extends Controller
{
    // Rapport de stage déposés

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $results1 = Rapport::findDepo();

        return view('backoffice.rapports-stage-anciens', compact('results1'));
    }
}
