<?php

namespace App\Http\Controllers;

use App\Models\Temoignage;
use App\Models\User;
use App\Services\TestimonyService;

class MonTemoignageController extends Controller
{
    // Mon un témoignage

    /**
     * @var TestimonyService
     */
    private $testimonyService;

    public function __construct(TestimonyService $testimonyService)
    {
        $this->middleware('auth');
        $this->testimonyService = $testimonyService;
    }

    public function index()
    {
        $statut = 0;
        $userId = auth()->user()->id;
        $existUser = User::hasTemoignage(auth()->user()->id);
        if ($existUser) {
            $statut = Temoignage::getStatutTemoign($userId);
        }
        $results1 = Temoignage::getStatutTemoignageUser(
            $userId,
            $existUser
        );
        $results2 = Temoignage::getTemoignageUser($userId);

        return view(
            'backoffice.mon-temoignage',
            compact('results1', 'results2', 'existUser', 'statut')
        );
    }

    public function update()
    {
        $id_user = auth()->user()->id;
        $this->testimonyService->setTemoignage($id_user);

        return back()->withMessage('Merci pour votre témoignage !');
    }
}
