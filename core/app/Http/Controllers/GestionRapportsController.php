<?php

namespace App\Http\Controllers;

use App\Models\Entreprise;
use App\Models\Rapport;
use App\Models\Stage;
use App\Models\User;

class GestionRapportsController extends Controller
{
    // Gestion des rapports de stages

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $results1 = Rapport::findAllDepo();
        $results2 = Rapport::findAllNONDepo();

        $notif1 = Rapport::getRapportNONDepoNUMBER();
        $notif2 = User::getStudentNumber();
        $notif3 = Rapport::getRapportDepoNUMBER();

        return view(
            'backoffice/gestion-rapports-stages',
            compact(
                'results1',
                'results2',
                'notif1',
                'notif2',
                'notif3'
            )
        );
    }

    public function update()
    {
        Stage::where('id_stage', request('id_stage'))
            ->update(['statut' => 1, 'sujet_stage' => ucfirst(request('sujet_stage'))]);

        Entreprise::where('id_entr', request('id_entreprise'))
            ->update(
                ['nom_entr' => ucfirst(request('nom_entreprise'))]
            );

        return back()->withMessage('Les informations ont été mis à jour !');
    }

    public function disable()
    {
        $id_stage = request('id_stage_desac');
        Rapport::deactiveInfoStage($id_stage);

        return back()->withMessage('Les informations ont été mis à jour !');
    }
}
