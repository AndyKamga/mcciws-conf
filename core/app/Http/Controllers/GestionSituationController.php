<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\ProfilService;

class GestionSituationController extends Controller
{
    // Gestion des situations des anciens

    /**
     * @var ProfilService
     */
    private $profilService;

    public function __construct(ProfilService $profilService)
    {
        $this->middleware('auth');
        $this->profilService = $profilService;
    }

    public function index()
    {
        $results1 = User::getSituation();
        $notif1 = User::getSituationValidNumber();
        $notif2 = User::getSituationNonValidNumber();

        return view(
            'backoffice.gestion-situation-anciens',
            compact('results1', 'notif1', 'notif2')
        );
    }

    public function update()
    {
        $id_user = request('id_user');

        $this->profilService->setUserPoste($id_user, request('poste_occupe'), 1);
        $this->profilService->setUserEntreprise($id_user, request('entreprise'), 1);

        return back()->withMessage('La situation a été validée !');
    }

    public function disable()
    {
        $id_user = request('id_user_desact');
        User::disableUserSituation($id_user);

        return back()->withMessage('La situation a été désactivée !');
    }
}
