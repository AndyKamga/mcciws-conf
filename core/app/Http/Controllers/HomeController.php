<?php

namespace App\Http\Controllers;

use App\Services\AccountManagerService;
use Illuminate\Contracts\Support\Renderable;

class HomeController extends Controller
{
    /**
     * @var AccountManagerService
     */
    private $accountManagerService;

    /**
     * Create a new controller instance.
     *
     * @param  AccountManagerService  $accountManagerService
     */
    public function __construct(AccountManagerService $accountManagerService)
    {
        $this->middleware('auth');
        $this->accountManagerService = $accountManagerService;
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */

    /*------------------------------------------------------------
    Les utilisateurs ont accès aux pages en fonction de leur droit
    Aucun droit = 0     |  Ancien = 1
    Etudiant =2         |  Semi administrateur = 3
    Administrateur = 4  |  Développeur = 5
    -----------------------------------------------------------*/

    // Accueil

    public function index()
    {
        /* On met à jour les comptes si la date actuelle
        est la date de mise à jour des comptes*/

        $this->accountManagerService->updateAccount();

        /* Puis on retourne la vue*/

        return view('backoffice.mon-espace');
    }
}
