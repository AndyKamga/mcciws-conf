<?php

namespace App\Http\Controllers;

use App\Models\Temoignage;
use App\Services\TestimonyService;

class GestionTemoignagesController extends Controller
{
    // Gestion des témoignages

    /**
     * @var TestimonyService
     */
    private $testimonyService;

    public function __construct(TestimonyService $testimonyService)
    {
        $this->middleware('auth');
        $this->testimonyService = $testimonyService;
    }

    public function index()
    {
        $results1 = Temoignage::getTemoignages();
        $notif1 = Temoignage::getTemoignagesValidNumber();
        $notif2 = Temoignage::getTemoignagesNonValidNumber();

        return view(
            'backoffice.gestion-des-temoignages',
            compact('results1', 'notif1', 'notif2')
        );
    }

    public function update()
    {
        $id = request('id');
        $id_user = request('id_user');

        return $this->testimonyService->update($id, $id_user);
    }

    public function disable()
    {
        $id = request('id');
        Temoignage::desactiverTemoignage($id);

        return back()->withMessage('Le témoignage a été désactivé !');
    }
}
