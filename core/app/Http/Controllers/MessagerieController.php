<?php

namespace App\Http\Controllers;

use App\Models\Mail;
use App\Models\User;
use App\Services\MessagesService;

class MessagerieController extends Controller
{
    // MessagerieRepository

    /**
     * @var MessagesService
     */
    private $messagesService;

    public function __construct(MessagesService $messagesService)
    {
        $this->middleware('auth');
        $this->messagesService = $messagesService;
    }

    public function index()
    {
        $comptes = User::getAccount();
        $nbrHistorique = Mail::count();
        $historique = Mail::all();

        return view(
            'backoffice/messagerie',
            compact('comptes', 'nbrHistorique', 'historique')
        );
    }

    public function historique()
    {
        $results1 = Mail::orderBy('id_mail', 'desc')->get();
        $nbrHistorique = Mail::count();

        return view(
            'backoffice/messagerie/historique-des-mails',
            compact('results1', 'nbrHistorique')
        );
    }

    public function add()
    {
        return $this->messagesService->add();
    }

    public function delete()
    {
        $id_mail = request('id_mail');
        Mail::where('id_mail', $id_mail)->delete();

        return back()->withMessage('Le message a été supprimé !');
    }
}
