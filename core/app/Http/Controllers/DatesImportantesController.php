<?php

namespace App\Http\Controllers;

use App\Models\DatesImportante;

class DatesImportantesController extends Controller
{
    // Dates importantes
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $results = DatesImportante::findAllListed();

        return view('backoffice.gestion-des-dates-importantes', compact('results'));
    }

    /**
     * @return mixed
     */
    public function update()
    {
        $datePreRentree = date(request('datePreRentree'));
        $dateRentree = date(request('dateRentree'));
        $dateOuverture = date(request('dateOuverture'));
        $dateLimite = date(request('dateLimite'));
        $dateRapport = date(request('dateRapport'));
        $dateChangementCompte = date(request('dateComptes'));

        DatesImportante::updateDatesImportantes(
            $datePreRentree,
            $dateRentree,
            $dateOuverture,
            $dateLimite,
            $dateRapport,
            $dateChangementCompte
        );

        return back()->withMessage('Les dates ont été mis à jour !');
    }
}
