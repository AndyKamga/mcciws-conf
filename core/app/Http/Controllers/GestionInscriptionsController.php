<?php

namespace App\Http\Controllers;

use App\Models\Inscription;
use App\Services\RegistrationService;

class GestionInscriptionsController extends Controller
{
    // Gestion des inscriptions

    /**
     * @var RegistrationService
     */
    private $registrationService;

    public function __construct(RegistrationService $registrationService)
    {
        $this->middleware('auth');
        $this->registrationService = $registrationService;
    }

    public function index()
    {
        $results1 = Inscription::all();
        $results2 = Inscription::getAccountMin();

        return view(
            'backoffice/gestion-des-inscriptions',
            compact('results1', 'results2')
        );
    }

    public function update()
    {
        return $this->registrationService->update();
    }
}
