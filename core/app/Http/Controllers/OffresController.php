<?php

namespace App\Http\Controllers;

use App\Models\Offre;

class OffresController extends Controller
{
    // Offre de stage

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $results1 = Offre::getvalidOffres();

        return view('backoffice.voir-offres-de-stage', compact('results1'));
    }

    public function update()
    {
        $id_offre = request('id_offre');
        $id_user = request('id_user');

        Offre::confirmerOffre($id_offre, $id_user);

        return back()->withMessage('Félicitation pour le stage !');
    }
}
