<?php

namespace App\Http\Controllers\Api;

use App\Dto\PostCommentDto;
use App\Http\Responses\ResponseEntity;
use App\Models\Post;
use App\Models\PostComment;
use App\Services\PostService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    private $postService;

    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    /**
     * @OA\Get (path="/api/social/posts/{post}/comments", tags={"Forum : Commentaires"}, security={{ "apiAuth": {} }},
     *     description="Afficher les commentaires d'une publication", summary="Voir les commentaires d'une publication",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *     @OA\Parameter(
     *         name="post",
     *         in="path",
     *         description="Identifiant de la publication",
     *         required=true,
     *         style="form"
     *     ),
     *      @OA\Response(response="200", description="Liste des commentaires",
     *          @OA\JsonContent(
     *              @OA\Property(property="current_page", type="number", example=1),
     *              @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/Commentaire")),
     *              @OA\Property(property="first_page_url", type="string", example="https://cci.univ-tours.fr/api/social/posts2/comments?page=1"),
     *              @OA\Property(property="from", type="number", example=1),
     *              @OA\Property(property="last_page", type="number", example=1),
     *              @OA\Property(property="last_page_url", type="string", example="https://cci.univ-tours.fr/api/social/posts2/comments?page=1"),
     *              @OA\Property(property="next_page_url", type="string", example=null),
     *              @OA\Property(property="path", type="string", example="https://cci.univ-tours.fr/api/social/posts/2/comments"),
     *              @OA\Property(property="per_page", type="number", example=5),
     *              @OA\Property(property="prev_page_url", type="string", example=null),
     *              @OA\Property(property="to", type="number", example=2),
     *              @OA\Property(property="total", type="number", example=2),
     *     ))
     * )
     * @param $postId
     * @return JsonResponse
     */
    public function findAll($postId)
    {
        $paginatedComments = PostComment::where('post_id', $postId)->orderBy('id', 'desc')->paginate(15);
        $paginatedComments->getCollection()->transform(function ($postComment) {
            return new PostCommentDto($postComment);
        });

        return ResponseEntity::ok($paginatedComments);
    }

    /**
     * @OA\Get (path="/api/social/posts/{post}/comments/{comment}", tags={"Forum : Commentaires"}, security={{ "apiAuth": {} }},
     *     description="Afficher un commentaire d'une publication", summary="Voir un commentaire d'une publication",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *     @OA\Parameter(
     *         name="post",
     *         in="path",
     *         description="Identifiant de la publication",
     *         required=true,
     *         style="form"
     *     ),
     *     @OA\Parameter(
     *         name="comment",
     *         in="path",
     *         description="Identifiant du commentaire",
     *         required=true,
     *         style="form"
     *     ),
     *      @OA\Response(response="200", description="Commentaire",
     *          @OA\JsonContent(ref="#/components/schemas/Commentaire")
     *     ))
     * )
     * @param $postId
     * @param $commentId
     * @return JsonResponse
     */
    public function findById($postId, $commentId)
    {
        $postComment =  PostComment::where('post_id', $postId)->where('id', $commentId)->first();
        if ($postComment == null) {
            return ResponseEntity::notFound(null, "Le commentaire n'existe pas !");
        }

        return ResponseEntity::ok(new PostCommentDto($postComment));
    }

    /**
     * @OA\Post (path="/api/social/posts/{post}/comments", tags={"Forum : Commentaires"}, security={{ "apiAuth": {} }},
     *     description="Ajouter un commentaire à une publication", summary="Ajouter un commentaire à une publication",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *      @OA\Parameter(
     *         name="post",
     *         in="path",
     *         description="Identifiant de la publication",
     *         required=true,
     *         style="form"
     *      ),
     *      @OA\RequestBody (@OA\JsonContent(@OA\Property(property="html_text", type="string", description="text"))),
     *      @OA\Response(response="201", description="Commentaire",
     *          @OA\JsonContent(ref="#/components/schemas/Commentaire")
     *     ))
     * )
     * @param Request $request
     * @param $postId
     * @return JsonResponse
     */
    public function create(Request $request, $postId)
    {
        $validator = Validator::make($request->all(), [
            'html_text' => ['required', 'string', 'min:10', 'max:5000']
        ]);

        if ($validator->fails()) {
            return ResponseEntity::badRequest($validator->errors());
        }

        $post = Post::where('id', $postId)->first();

        if ($post == null) {
            return ResponseEntity::notFound(null, "La publication n'existe pas !");
        }

        $postComment = new PostComment($request->all());
        $postComment->user()->associate($request->user());
        $postComment->post()->associate($post);

        $postComment->save();


        return ResponseEntity::created(new PostCommentDto($postComment));
    }

    /**
     * @OA\Patch  (path="/api/social/posts/{post}/comments/{comment}", tags={"Forum : Commentaires"}, security={{ "apiAuth": {} }},
     *     description="Déclarer un commentaire comme étant la meilleur réponse", summary="Déclarer un commentaire comme étant la meilleur réponse",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *     @OA\Parameter(
     *         name="post",
     *         in="path",
     *         description="Identifiant de la publication",
     *         required=true,
     *         style="form"
     *     ),
     *     @OA\Parameter(
     *         name="comment",
     *         in="path",
     *         description="Identifiant du commentaire",
     *         required=true,
     *         style="form"
     *     ),
     *     @OA\RequestBody (required=true, description="Payload",
     *      @OA\JsonContent (required={"is_best_answer"}, @OA\Property(property="is_best_answer", type="boolean", example=true)),
     *      ),
     *      @OA\Response(response="200", description="Commentaire déclarée comme meilleur réponse")
     *    )
     * )
     * @param Request $request
     * @param $postId
     * @param $commentId
     * @return JsonResponse
     */
    public function bestAnswer(Request $request, $postId, $commentId)
    {
        $validator = Validator::make($request->all(), [
            'is_best_answer'  => ['required', 'boolean']
        ]);

        if ($validator->fails()) {
            return ResponseEntity::badRequest($validator->errors());
        }

        $postComment =  PostComment::where('post_id', $postId)->where('id', $commentId)->first();
        if ($postComment == null) {
            return ResponseEntity::notFound(null, "Le commentaire n'existe pas !");
        }

        return ResponseEntity::ok(null, $this->postService->bestAnswer($request, $commentId, $postComment));
    }

    /**
     * @OA\Put (path="/api/social/posts/{post}/comments/{comment}", tags={"Forum : Commentaires"}, security={{ "apiAuth": {} }},
     *     description="Modifier un commentaire d'une publication", summary="Modifier un commentaire d'une publication",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *      @OA\Parameter(
     *         name="post",
     *         in="path",
     *         description="Identifiant de la publication",
     *         required=true,
     *         style="form"
     *     ),
     *     @OA\Parameter(
     *         name="comment",
     *         in="path",
     *         description="Identifiant du commentaire",
     *         required=true,
     *         style="form"
     *     ),
     *      @OA\RequestBody (@OA\JsonContent(@OA\Property(property="text", type="string", description="Mon commentaire"))),
     *      @OA\Response(response="200", description="Commentaire mis à jour",
     *          @OA\JsonContent(ref="#/components/schemas/Commentaire")
     *     ))
     * )
     * @param Request $request
     * @param $postId
     * @param $commentId
     * @return JsonResponse
     */
    public function update(Request $request, $postId, $commentId)
    {
        $validator = Validator::make($request->all(), [
            'html_text' => ['required', 'string', 'min:10', 'max:5000']
        ]);

        if ($validator->fails()) {
            return ResponseEntity::badRequest($validator->errors());
        }

        $postComment =  PostComment::where('post_id', $postId)->where('id', $commentId)->first();
        if ($postComment == null) {
            return ResponseEntity::notFound(null, "Le commentaire n'existe pas !");
        }

        $postComment->text = $request->input('html_text');
        $postComment->save();

        return ResponseEntity::ok(new PostCommentDto($postComment));
    }

    /**
     * @OA\Delete  (path="/api/social/posts/{post}/comments/{comment}", tags={"Forum : Commentaires"}, security={{ "apiAuth": {} }},
     *     description="Supprimer un commentaire d'une publication", summary="Supprimer un commentaire d'une publication",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *     @OA\Parameter(
     *         name="post",
     *         in="path",
     *         description="Identifiant de la publication",
     *         required=true,
     *         style="form"
     *     ),
     *     @OA\Parameter(
     *         name="comment",
     *         in="path",
     *         description="Identifiant du commentaire",
     *         required=true,
     *         style="form"
     *     ),
     *      @OA\Response(response="200", description="Commentaire supprimé"))
     * )
     * @param Request $request
     * @param $postId
     * @param $commentId
     * @return JsonResponse
     */
    public function delete(Request $request, $postId, $commentId)
    {
        if (!$this->postService->deleteComment($request, $postId, $commentId)) {
            return ResponseEntity::notFound(null, "Le commentaire n'a pas pu être supprimé ou n'existe pas !");
        }

        return ResponseEntity::ok(null, "Le commentaire a été supprimée");
    }
}
