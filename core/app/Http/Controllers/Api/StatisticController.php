<?php

namespace App\Http\Controllers\Api;

use App\Dto\UserRatedDto;
use App\Http\Responses\ResponseEntity;
use App\Http\Controllers\Controller;
use App\Models\PostComment;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class StatisticController extends Controller
{
    /**
     * @OA\Get (path="/api/social/stats/posts", tags={"Forum : Statistiques"}, security={{ "apiAuth": {} }},
     *     summary="Meilleurs contributeurs", description="Utilisateurs ayant résolus le plus de problèmes",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *      @OA\Response(response="200", description="Liste de utilisateurs",
     *          @OA\JsonContent(
     *              @OA\Items(ref="#/components/schemas/TopUser")
     *          ))
     *      )
     * )
     * @description
     * Permet de récupérer les utilisateurs qui ont résolut les plus de problèmes évoqués dans les publications
     *
     * @return JsonResponse
     */
    public function posts()
    {
        $topUsers = [];
        $results = DB::table('post_comments')
            ->select(DB::raw('count(*) as total'), 'user_id')
            ->where('is_best_answer', true)
            ->groupBy('user_id')
            ->orderBy('total', 'desc')
            ->get();

        foreach ($results as $result) {
            array_push($topUsers, new UserRatedDto($result));
        }

        return ResponseEntity::ok($topUsers);
    }
}
