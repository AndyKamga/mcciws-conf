<?php

namespace App\Http\Controllers\Api;

use App\Dto\PostDto;
use App\Dto\UserDto;
use App\Http\Responses\ResponseEntity;
use App\Models\Post;
use App\Models\PostCategory;
use App\Services\PostService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    private $postService;
    private $filesLimitNbr = 3;

    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    /**
     * @OA\Get (path="/api/social/posts", tags={"Forum : Publications"}, security={{ "apiAuth": {} }},
     *     summary="Voir les publications", description="Voir toutes les publications et les publications associées à une catégorie",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *      @OA\Parameter(
     *         name="page",
     *         in="query",
     *         description="Pagination",
     *         style="form"
     *      ),
     *     @OA\Parameter(
     *         name="category",
     *         in="query",
     *         description="Identifiant de la catégorie",
     *         required=false,
     *         style="form"
     *     ),
     *      @OA\Response(response="200", description="Liste des publications postées",
     *          @OA\JsonContent(
     *              @OA\Property(property="current_page", type="number", example=1),
     *              @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/Publication")),
     *              @OA\Property(property="first_page_url", type="string", example="https://cci.univ-tours.fr/api/social/posts?page=1"),
     *              @OA\Property(property="from", type="number", example=1),
     *              @OA\Property(property="last_page", type="number", example=1),
     *              @OA\Property(property="last_page_url", type="string", example="https://cci.univ-tours.fr/api/social/posts?page=1"),
     *              @OA\Property(property="next_page_url", type="string", example=null),
     *              @OA\Property(property="path", type="string", example="https://cci.univ-tours.fr/api/social/posts"),
     *              @OA\Property(property="per_page", type="number", example=5),
     *              @OA\Property(property="prev_page_url", type="string", example=null),
     *              @OA\Property(property="to", type="number", example=2),
     *              @OA\Property(property="total", type="number", example=2),
     *     ))
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function findAll(Request $request)
    {
        $category = $request->input('category');
        $paginatedPosts = $category != null ? PostCategory::where('id', $category)->first()
            ->posts()->orderBy('id', 'desc')->paginate(5) : Post::orderBy('id', 'desc')->paginate(5);

        $paginatedPosts->getCollection()->transform(function ($post) {
            return new PostDto($post);
        });

        return ResponseEntity::ok($paginatedPosts);
    }

    /**
     * @OA\Get (path="/api/social/posts/{post}", tags={"Forum : Publications"}, security={{ "apiAuth": {} }},
     *     description="Afficher une publication", summary="Voir une publication",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *     @OA\Parameter(
     *         name="post",
     *         in="path",
     *         description="Identifiant de la publication",
     *         required=true,
     *         style="form"
     *     ),
     *      @OA\Response(response="200", description="Profil",
     *          @OA\JsonContent(ref="#/components/schemas/Publication")
     *     ))
     * )
     * @param $id
     * @return JsonResponse
     */
    public function findById($id)
    {
        $post = Post::where('id', $id)->first();

        if ($post == null) {
            return ResponseEntity::notFound(null, "La publication n'existe pas !");
        }

        return ResponseEntity::ok(new PostDto($post));
    }

    /**
     * @OA\Post (path="/api/social/posts", tags={"Forum : Publications"}, security={{ "apiAuth": {} }},
     *     description="Créer une publication", summary="Créer une publication",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *
     *      @OA\RequestBody (
     *          @OA\MediaType(mediaType="multipart/form-data",
     *              @OA\Schema(required={"title", "html_text"},
     *                  @OA\Property(property="title", type="string", description="titre"),
     *                  @OA\Property(property="html_text", type="string", description="texte au format HTML"),
     *                  @OA\Property(property="iframe_code", type="string", description="iframe_code"),
     *                  @OA\Property(property="attachments", type="file", description="Pièces jointes à la publication")
     *              )
     *          ),
     *      ),
     *      @OA\Response(response="201", description="Publication",
     *          @OA\JsonContent(ref="#/components/schemas/Publication")
     *     ))
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => ['required', 'string', 'min:10', 'max:60'],
            'html_text' => ['required', 'string', 'min:10', 'max:5000'],
            'iframe_code' => ['nullable', 'string',  'min:20', 'max:3000'],
            'is_pinned'  => ['nullable', 'boolean'],
            'attachments' => ['nullable','mimes:jpeg,jpg,png,doc,pdf,docx,jpg,gif,svg', 'max:4048'],
            'attachments.*' => ['nullable','mimes:jpeg,jpg,png,doc,pdf,docx,jpg,gif,svg', 'max:4048']
        ]);

        if ($validator->fails()) {
            return ResponseEntity::badRequest($validator->errors());
        } else if ($request->hasfile('attachments') && count($request->allFiles()) > $this->filesLimitNbr) {
            return ResponseEntity::badRequest(
                null,
                "Vous ne pouvez qu'envoyer " .$this->filesLimitNbr. " fichiers"
            );
        }

        return ResponseEntity::created($this->postService->create($request));
    }

    /**
     * @OA\Patch  (path="/api/social/posts/{post}", tags={"Forum : Publications"}, security={{ "apiAuth": {} }},
     *     description="Épingler/Désépingler une publication", summary="Épingler/Désépingler une publication",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *     @OA\Parameter(
     *         name="post",
     *         in="path",
     *         description="Identifiant de la publication",
     *         required=true,
     *         style="form"
     *     ),
     *     @OA\RequestBody (required=true, description="Payload",
     *      @OA\JsonContent (required={"is_pinned"}, @OA\Property(property="is_pinned", type="boolean", example=true)),
     *      ),
     *      @OA\Response(response="200", description="Publication")
     *    )
     * )
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function pin(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'is_pinned'  => ['required', 'boolean']
        ]);

        if ($validator->fails()) {
            return ResponseEntity::badRequest($validator->errors());
        }

        $post = Post::where('id', $id)->first();

        if ($post == null) {
            return ResponseEntity::notFound(null, "La publication a supprimer n'existe pas !");
        }

        $post->update($request->all());

        $message = $request->input('is_pinned') ?"La publication a été épinglée":"La publication a été désépinglée";

        return ResponseEntity::ok(null, $message);
    }

    /**
     * @OA\Delete  (path="/api/social/posts/{post}", tags={"Forum : Publications"}, security={{ "apiAuth": {} }},
     *     description="Supprimer une publication", summary="Supprimer une publication",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *     @OA\Parameter(
     *         name="post",
     *         in="path",
     *         description="Identifiant de la publication",
     *         required=true,
     *         style="form"
     *     ),
     *      @OA\Response(response="200", description="Publication supprimée"
     *     ))
     * )
     * @param $id
     * @return JsonResponse
     */
    public function delete($id)
    {
        if (!$this->postService->delete($id)) {
            return ResponseEntity::notFound(null, "La publication n'a pas pu être supprimée ou n'existe pas !");
        }

        return ResponseEntity::ok(null, "La publication a été supprimée");
    }

    /**
     * @OA\Get (path="/api/social/posts/{post}/likes", tags={"Forum : Publications"}, security={{ "apiAuth": {} }},
     *     description="Voir les personnes qui ont aimé la publication", summary="Voir les likes",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *     @OA\Parameter(
     *         name="post",
     *         in="path",
     *         description="Identifiant de la publication",
     *         required=true,
     *         style="form"
     *     ),
     *      @OA\Response(response="200", description="Liste des likes",
     *       @OA\JsonContent(@OA\Items(ref="#/components/schemas/Utilisateur"))
     *      ))
     * )
     * @param $id
     * @return JsonResponse
     */
    public function findAllLike($id)
    {
        $post = Post::where('id', $id)->first();

        if ($post == null) {
            return ResponseEntity::notFound(null, "La publication n'existe pas !");
        }

        $likers = [];
        foreach ($post->likes()->get() as $like) {
            array_push($likers, new UserDto($like->user()->first()));
        }

        return ResponseEntity::ok($likers);
    }

    /**
     * @OA\Post (path="/api/social/posts/{post}/likes", tags={"Forum : Publications"}, security={{ "apiAuth": {} }},
     *     description="Ajouter un like à une publication", summary="Ajouter un like à une publication",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *     @OA\Parameter(
     *         name="post",
     *         in="path",
     *         description="Identifiant de la publication",
     *         required=true,
     *         style="form"
     *     ),
     *      @OA\Response(response="200", description="Publication aimée"
     *     ))
     * )
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function addLike(Request $request, $id)
    {
        if (Post::where('id', $id)->first() == null) {
            return ResponseEntity::notFound(null, "La publication n'existe pas !");
        }

        if (!$this->postService->addLike($request, $id)) {
            return ResponseEntity::badRequest(null, "Vous avez déjà aimé la publication !");
        }

        return ResponseEntity::ok(null, "La publication a été aimée");
    }

    /**
     * @OA\Delete  (path="/api/social/posts/{post}/likes", tags={"Forum : Publications"}, security={{ "apiAuth": {} }},
     *     description="Supprimer son like sur une publication", summary="Supprimer son like sur une publication",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *     @OA\Parameter(
     *         name="post",
     *         in="path",
     *         description="Identifiant de la publication",
     *         required=true,
     *         style="form"
     *     ),
     *      @OA\Response(response="200", description="Like supprimée"
     *     ))
     * )
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function removeLike(Request $request, $id)
    {
        if (Post::where('id', $id)->first() == null) {
            return ResponseEntity::notFound(null, "La publication n'existe pas !");
        }

        if (!$this->postService->removeLike($request, $id)) {
            return ResponseEntity::badRequest(null, "La publication n'est déjà plus aimée !");
        }

        return ResponseEntity::ok(null, "Vous n'aimez plus la publication");
    }
}
