<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Responses\ResponseEntity;
use App\Models\Droit;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Lcobucci\JWT\Parser;

class AuthController extends Controller
{
    /**
     * @OA\Post (path="/api/login", tags={"Authentification"}, description="Permet de s'authentifier à l'API",
     * @OA\RequestBody (required=true, description="Informations d'identification de l'utilisateur",
     *  @OA\JsonContent (required={"email","password"},
     *      @OA\Property(property="email", type="string", format="email", example="john.doe@gmail.com"),
     *      @OA\Property(property="password", type="string", format="password", example="password12345"),
     *    ),
     * ),
     *  @OA\Response(response="200", description="Informations sur l'utilisateur connecté, accompagnées du token d'authentification, à faire passer dans le header lors de chaques requêtes vers l'API.",
     *     @OA\JsonContent(
     *       @OA\Property(property="id", type="number", example=5),
     *       @OA\Property(property="first_name", type="string", example="John"),
     *       @OA\Property(property="last_name", type="string", example="Doe"),
     *       @OA\Property(property="email", type="string", example="john.doe@gmail.com"),
     *       @OA\Property(property="avatar", type="string", example="https://cci.univ-tours.fr/storage/avatar/name.png"),
     *       @OA\Property(property="role", type="string", example="Etudiant"),
     *       @OA\Property(property="access_token", type="string", example="eyJhbGciOi.eyJzdOiIxMjM0.SflKxwc")
     *     )
     *   ))
     * )
     */
    public function login()
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            $token = $user->createToken('mastercci')->accessToken;

            return ResponseEntity::ok([
                'id'  => $user->id,
                'first_name' => $user->prenom,
                'last_name' => $user->nom,
                'email' => $user->email,
                'avatar' => request()->getSchemeAndHttpHost() . '/storage/' .$user->photo,
                'role' => $user->droits()->first()->description,
                'access_token' => $token
            ]);
        }

        return ResponseEntity::unauthorized(null, 'Bad credentials !');
    }

    /**
     * @OA\Get (path="/api/logout", tags={"Authentification"}, security={{ "apiAuth": {} }},
     *     description="Permet de se déconnecter de l'API",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *      @OA\Response(response="200", description="Déconnexion réussie",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Vous êtes déconnecté !")
     *     ))
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request)
    {
        $id = (new Parser())->parse($request->bearerToken())->getHeader('jti');
        if ($id) {
            $token = $request->user()->token()->find($id);
            $token->revoke();

            return ResponseEntity::ok(null, 'Vous êtes déconnecté !');
        }

        return ResponseEntity::badRequest(null, "Vous n'êtes pas connecté !");
    }
}
