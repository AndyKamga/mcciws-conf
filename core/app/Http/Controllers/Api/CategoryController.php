<?php

namespace App\Http\Controllers\Api;

use App\Dto\PostCategoryDto;
use App\Http\Responses\ResponseEntity;
use App\Models\Post;
use App\Models\PostCategory;
use App\Services\PostCategoryService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    private $postCategoryService;

    public function __construct(PostCategoryService $postCategoryService)
    {
        $this->postCategoryService = $postCategoryService;
    }

    /**
     * @OA\Get (path="/api/social/categories", tags={"Forum : Catégories"}, security={{ "apiAuth": {} }},
     *     summary="Voir les catégories", description="Voir toutes les catégories",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *      @OA\Parameter(
     *         name="page",
     *         in="query",
     *         description="Pagination",
     *         style="form"
     *      ),
     *      @OA\Response(response="200", description="Liste des catégories",
     *          @OA\JsonContent(
     *              @OA\Property(property="current_page", type="number", example=1),
     *              @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/Categorie")),
     *              @OA\Property(property="first_page_url", type="string", example="https://cci.univ-tours.fr/api/social/categories?page=1"),
     *              @OA\Property(property="from", type="number", example=1),
     *              @OA\Property(property="last_page", type="number", example=1),
     *              @OA\Property(property="last_page_url", type="string", example="https://cci.univ-tours.fr/api/social/categories?page=1"),
     *              @OA\Property(property="next_page_url", type="string", example=null),
     *              @OA\Property(property="path", type="string", example="https://cci.univ-tours.fr/api/social/categories"),
     *              @OA\Property(property="per_page", type="number", example=5),
     *              @OA\Property(property="prev_page_url", type="string", example=null),
     *              @OA\Property(property="to", type="number", example=2),
     *              @OA\Property(property="total", type="number", example=2),
     *     ))
     * )
     * @return JsonResponse
     */
    public function findAll()
    {
        $paginatedCategories = PostCategory::orderBy('id', 'desc')->paginate(5);
        $paginatedCategories->getCollection()->transform(function ($category) {
            return new PostCategoryDto($category);
        });

        return ResponseEntity::ok($paginatedCategories);
    }

    /**
     * @OA\Post (path="/api/social/categories", tags={"Forum : Catégories"}, security={{ "apiAuth": {} }},
     *     description="Créer une catégorie", summary="Créer une catégorie",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *
     *      @OA\RequestBody (
     *          @OA\JsonContent(ref="#/components/schemas/Categorie"),
     *      ),
     *      @OA\Response(response="201", description="Catégorie créée",
     *          @OA\JsonContent(ref="#/components/schemas/Categorie")
     *     ))
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'min:5', 'max:50'],
            'description' => ['required', 'string', 'min:10', 'max:300'],
        ]);

        if ($validator->fails()) {
            return ResponseEntity::badRequest($validator->errors());
        }

        return $this->postCategoryService->create($request);
    }

    /**
     * @OA\Put (path="/api/social/categories/{category}", tags={"Forum : Catégories"}, security={{ "apiAuth": {} }},
     *     description="Modifier une catégorie", summary="Modifier une catégorie",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *     @OA\Parameter(
     *         name="category",
     *         in="path",
     *         description="Identifiant de la catégorie",
     *         required=true,
     *         style="form"
     *     ),
     *      @OA\Response(response="200", description="Catégorie modifiée"
     *     ))
     * )
     * @param Request $request
     * @param $categoryId
     * @return JsonResponse
     */
    public function update(Request $request, $categoryId)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'min:5', 'max:50'],
            'description' => ['required', 'string', 'min:10', 'max:300'],
        ]);

        if ($validator->fails()) {
            return ResponseEntity::badRequest($validator->errors());
        }

        return $this->postCategoryService->update($request, $categoryId);
    }

    /**
     * @OA\Put (path="/api/social/posts/{post}/category/{category}", tags={"Forum : Publications"}, security={{ "apiAuth": {} }},
     *     description="Associer un catégorie à une publication", summary="Associer un catégorie à une publication",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *     @OA\Parameter(
     *         name="post",
     *         in="path",
     *         description="Identifiant de la publication",
     *         required=true,
     *         style="form"
     *     ),
     *     @OA\Parameter(
     *         name="category",
     *         in="path",
     *         description="Identifiant de la catégorie",
     *         required=true,
     *         style="form"
     *     ),
     *      @OA\Response(response="200", description="Catégorie ajoutée à la publication"
     *     ))
     * )
     * @param $postId
     * @param $categoryId
     * @return JsonResponse
     */
    public function addToPost($postId, $categoryId)
    {
        $post = Post::where('id', $postId)->first();

        if ($post == null) {
            return ResponseEntity::notFound(null, "La publication n'existe pas !");
        }

        $category = PostCategory::where('id', $categoryId)->first();

        if ($category == null) {
            return ResponseEntity::notFound(null, "La catégorie n'existe pas !");
        } else if ($post->categories()->get()->where('id', $categoryId)->count() > 0) {
            return ResponseEntity::badRequest(
                null,
                "La publication est déjà associée à la catégorie '". $category->name."'"
            );
        }

        $post->categories()->attach($category);

        return ResponseEntity::ok(
            null,
            "La publication '".$post->title. "' appartient à la catégorie '". $category->name."'"
        );
    }

    /**
     * @OA\Delete  (path="/api/social/posts/{post}/category/{category}", tags={"Forum : Publications"}, security={{ "apiAuth": {} }},
     *     description="Ne plus associer un catégorie à une publication", summary="Ne plus associer un catégorie à une publication",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *     @OA\Parameter(
     *         name="post",
     *         in="path",
     *         description="Identifiant de la publication",
     *         required=true,
     *         style="form"
     *     ),
     *     @OA\Parameter(
     *         name="category",
     *         in="path",
     *         description="Identifiant de la catégorie",
     *         required=true,
     *         style="form"
     *     ),
     *      @OA\Response(response="200", description="Catégorie retirée de à la publication"
     *     ))
     * )
     * @param $postId
     * @param $categoryId
     * @return JsonResponse
     */
    public function removeToPost($postId, $categoryId)
    {
        $post = Post::where('id', $postId)->first();

        if ($post == null) {
            return ResponseEntity::notFound(null, "La publication n'existe pas !");
        }

        $category = PostCategory::where('id', $categoryId)->first();

        if ($category == null) {
            return ResponseEntity::notFound(null, "La catégorie n'existe pas !");
        }

        $post->categories()->detach($category);

        return ResponseEntity::ok(
            null,
            "La publication '".$post->title. "' n'appartient plus à la catégorie '". $category->name."'"
        );
    }
}
