<?php

namespace App\Http\Controllers\Api;

use App\Dto\PostDto;
use App\Dto\SocialProfilDto;
use App\Http\Responses\ResponseEntity;
use App\Models\Post;
use App\Models\User;
use App\Services\ProfilService;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class SocialProfilController extends Controller
{
    /**
     * @var ProfilService
     */
    private $profilService;

    public function __construct(ProfilService $profilService)
    {
        $this->profilService = $profilService;
    }

    /**
     * @OA\Get (path="/api/social/profils/{user}", tags={"Forum : Profils"}, security={{ "apiAuth": {} }},
     *     description="Voir un profil", summary="Voir un profil (Publications et situation professionnelle)",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *      @OA\Parameter(
     *         name="user",
     *         in="path",
     *         description="Identifiant de l'utilisateur",
     *         required=true,
     *         style="form"
     *      ),
     *      @OA\Response(response="200", description="Profil de l'utilisateur",
     *          @OA\JsonContent(ref="#/components/schemas/Profil"))
     * )
     * @param $id
     * @return JsonResponse
     */
    public function user($id)
    {
        $user = User::where('id', $id)->first();
        if ($user == null) {
            return ResponseEntity::notFound(null, "L'utilisateur n'existe pas !");
        }

        $situation = $this->profilService->getUserSituation($user->id);

        $posts = Post::orderBy('id', 'desc')->where('user_id', $id)->paginate(10);
        $posts->getCollection()->transform(function ($post) {
            return new PostDto($post);
        });

        return ResponseEntity::ok(new SocialProfilDto($user, $posts, $situation));
    }
}
