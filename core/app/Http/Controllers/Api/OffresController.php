<?php

namespace App\Http\Controllers\Api;

use App\Dto\InternShipDto;
use App\Http\Controllers\Controller;
use App\Http\Responses\ResponseEntity;
use App\Models\Offre;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class OffresController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * @OA\Get (path="/api/internship-offers", tags={"Offres de stage"}, security={{ "apiAuth": {} }},
     *     description="Voir les offres de stages",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *      @OA\Response(response="200", description="Liste des offres de stage",
     *          @OA\JsonContent(@OA\Items(ref="#/components/schemas/Offre")
     *     ))
     * )
     *
     * @return JsonResponse
     */
    public function index()
    {
        $offres = [];
        foreach (Offre::getvalidOffres() as $offer) {
            array_push($offres, new InternShipDto($offer));
        }

        return ResponseEntity::ok($offres);
    }

    /**
     * @OA\Patch (path="/api/internship-offers/{offre}", tags={"Offres de stage"}, security={{ "apiAuth": {} }},
     *     description="Permet de déclarer une offre de stage comme étant pourvue",
     *   @OA\Parameter(
     *       name="offre",
     *       in="path",
     *       description="Identifiant de l'offre",
     *       required=true,
     *       style="form"
     *   ),
     *  @OA\Response(response="200", description="",
     *     @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Félicitation pour le stage !")
     *     )
     *   ))
     * )
     *
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function disable(Request $request, $id)
    {
        $id_user = $request->user()->id;

        Offre::confirmerOffre($id, $id_user);

        return ResponseEntity::ok(null, 'Félicitation pour le stage !');
    }
}
