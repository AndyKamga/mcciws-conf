<?php

namespace App\Http\Controllers\Api;

use App\Dto\PostReportDto;
use App\Http\Responses\ResponseEntity;
use App\Models\PostReport;
use App\Services\PostService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ReportController extends Controller
{
    private $postService;

    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    /**
     * @OA\Get (path="/api/social/posts/{post}/reports", tags={"Forum : Signalisation"}, security={{ "apiAuth": {} }},
     *     description="Voir les signalisations", summary="Voir les signalisations",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *     @OA\Parameter(
     *         name="post",
     *         in="path",
     *         description="Identifiant de la publication",
     *         required=true,
     *         style="form"
     *     ),
     *      @OA\Response(response="200", description="Liste des signalisations",
     *          @OA\JsonContent(
     *              @OA\Property(property="current_page", type="number", example=1),
     *              @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/Signalisation")),
     *              @OA\Property(property="first_page_url", type="string", example="https://cci.univ-tours.fr/api/social/posts2/reports?page=1"),
     *              @OA\Property(property="from", type="number", example=1),
     *              @OA\Property(property="last_page", type="number", example=1),
     *              @OA\Property(property="last_page_url", type="string", example="https://cci.univ-tours.fr/api/social/posts2/reports?page=1"),
     *              @OA\Property(property="next_page_url", type="string", example=null),
     *              @OA\Property(property="path", type="string", example="https://cci.univ-tours.fr/api/social/posts/2/reports"),
     *              @OA\Property(property="per_page", type="number", example=5),
     *              @OA\Property(property="prev_page_url", type="string", example=null),
     *              @OA\Property(property="to", type="number", example=2),
     *              @OA\Property(property="total", type="number", example=2),
     *     ))
     * )
     * @param $postId
     * @return JsonResponse
     */
    public function findAll($postId)
    {
        $paginatedReports = PostReport::where('post_id', $postId)->orderBy('id', 'desc')->paginate(15);
        $paginatedReports->getCollection()->transform(function ($postReport) {
            return new PostReportDto($postReport);
        });

        return ResponseEntity::ok($paginatedReports);
    }

    /**
     * @OA\Post (path="/api/social/posts/{post}/reports/{comment}", tags={"Forum : Signalisation"}, security={{ "apiAuth": {} }},
     *     description="Signaler une publication ou un commentaire", summary="Signaler une publication ou un commentaire",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *      @OA\Parameter(
     *         name="post",
     *         in="path",
     *         description="Identifiant de la publication",
     *         required=true,
     *         style="form"
     *      ),
     *     @OA\Parameter(
     *         name="comment",
     *         in="path",
     *         description="Identifiant du commentaire",
     *         required=false,
     *         style="form"
     *     ),
     *      @OA\RequestBody (required=true, description="Description de la signalisation",
     *      @OA\JsonContent (required={"description"},
     *          @OA\Property(property="description", type="string", example="Ma description")),
     *      ),
     *      @OA\Response(response="200", description="Signalisation",
     *          @OA\JsonContent(ref="#/components/schemas/Signalisation")
     *     ))
     * )
     * @param Request $request
     * @param $postId
     * @param $commentId
     * @return JsonResponse
     */
    public function create(Request $request, $postId, $commentId = null)
    {
        $validator = Validator::make($request->all(), [
            'description' => ['required', 'string', 'min:30', 'max:2000']
        ]);

        if ($validator->fails()) {
            return ResponseEntity::badRequest($validator->errors());
        }

        if ($commentId != null && is_numeric($commentId)) {
            return $this->postService->reportComment($request, $postId, $commentId);
        }

        return $this->postService->reportPost($request, $postId);
    }

    /**
     * @OA\Delete (path="/api/social/posts/{post}/reports/{report}", tags={"Forum : Signalisation"}, security={{ "apiAuth": {} }},
     *     description="Supprimer une signalisation", summary="Supprimer une signalisation",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *     @OA\Parameter(
     *         name="post",
     *         in="path",
     *         description="Identifiant de la publication",
     *         required=true,
     *         style="form"
     *     ),
     *     @OA\Parameter(
     *         name="report",
     *         in="path",
     *         description="Identifiant de la signalisation",
     *         required=true,
     *         style="form"
     *     ),
     *      @OA\Response(response="200", description="Signalisation supprimée"))
     * )
     * @param $postId
     * @param $reportId
     * @return JsonResponse
     */
    public function delete($postId, $reportId)
    {
        $postReport =  PostReport::where('post_id', $postId)->where('id', $reportId)->first();
        if ($postReport == null) {
            return ResponseEntity::notFound(null, "La signalisation n'existe pas !");
        }

        $postReport->delete();

        return ResponseEntity::ok(null, "La signalisation a été supprimée");
    }
}
