<?php

namespace App\Http\Controllers\Api;

use App\Dto\ChatDto;
use App\Dto\ChatMessageDto;
use App\Http\Responses\ResponseEntity;
use App\Models\Chat;
use App\Models\ChatMember;
use App\Models\ChatMessage;
use App\Models\User;
use App\Services\ChatService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ChatController extends Controller
{
    private $chatService;
    private $filesLimitNbr = 3;

    public function __construct(ChatService $chatService)
    {
        $this->chatService = $chatService;
    }

    /**
     * @OA\Get (path="/api/social/chats", tags={"Forum : Conversations"}, security={{ "apiAuth": {} }},
     *     description="Voir mes conversations", summary="Voir mes conversations",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *      @OA\Parameter(
     *         name="page",
     *         in="query",
     *         description="Pagination",
     *         style="form"
     *      ),
     *      @OA\Response(response="200", description="Liste de mes conversations",
     *          @OA\JsonContent(
     *              @OA\Property(property="current_page", type="number", example=1),
     *              @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/Conversation")),
     *              @OA\Property(property="first_page_url", type="string", example="https://cci.univ-tours.fr/api/social/chats?page=1"),
     *              @OA\Property(property="from", type="number", example=1),
     *              @OA\Property(property="last_page", type="number", example=1),
     *              @OA\Property(property="last_page_url", type="string", example="https://cci.univ-tours.fr/api/social/chats?page=1"),
     *              @OA\Property(property="next_page_url", type="string", example=null),
     *              @OA\Property(property="path", type="string", example="https://cci.univ-tours.fr/api/social/chats"),
     *              @OA\Property(property="per_page", type="number", example=5),
     *              @OA\Property(property="prev_page_url", type="string", example=null),
     *              @OA\Property(property="to", type="number", example=2),
     *              @OA\Property(property="total", type="number", example=2),
     *     ))
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function findAll(Request $request)
    {
        $paginatedChats = ChatMember::where('user_id', $request->user()->id)
            ->orderBy('chat_id', 'desc')->paginate(10);
        $paginatedChats->getCollection()->transform(function ($userChat) {
            return new ChatDto($userChat->chat()->first());
        });

        return ResponseEntity::ok($paginatedChats);
    }

    /**
     * @OA\Post (path="/api/social/chats", tags={"Forum : Conversations"}, security={{ "apiAuth": {} }},
     *     description="Créer une conversation", summary="Créer une conversation",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *
     *      @OA\RequestBody (
     *          @OA\JsonContent(required={"name", "members"},
     *              @OA\Property(property="name", type="string", example="John Doe", description="nom de la conversation"),
     *              @OA\Property(property="members", type="array", @OA\Items(type="number", example=185,
     *              )),
     *           ),
     *      ),
     *      @OA\Response(response="201", description="Conversation",
     *          @OA\JsonContent(ref="#/components/schemas/Conversation")
     *     ))
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'min:5', 'max:100'],
            'members'   => ['required', 'array', 'min:1', 'max:10'],
            'members.*' => ['integer'],
        ]);
        $memberList = $request->input('members');
        array_push($memberList, $request->user()->id);
        $members = User::whereIn('id', $memberList)->get();
        $membersNbr = count($members);

        if ($validator->fails()) {
            return ResponseEntity::badRequest($validator->errors());
        } else if ($membersNbr == 0) {
            return ResponseEntity::badRequest(null, "Vous devez renseigner des identifiants utilisateur valides !");
        } else if ($membersNbr == 1 && $members[0]->id == $request->user()->id) {
            return ResponseEntity::badRequest(null, "Vous ne pouvez pas démmarrer une conversation avec vous même !");
        }

        return ResponseEntity::created($this->chatService->create($request, $members));
    }

    /**
     * @OA\Delete  (path="/api/social/chats/{chat}", tags={"Forum : Conversations"}, security={{ "apiAuth": {} }},
     *     description="Supprimer une de mes conversation", summary="Supprimer une de mes conversations",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *     @OA\Parameter(
     *         name="chat",
     *         in="path",
     *         description="Identifiant de ma conversation",
     *         required=true,
     *         style="form"
     *     ),
     *      @OA\Response(response="200", description="Conversation supprimée"
     *     ))
     * )
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function delete(Request $request, $id)
    {
        $chat = $request->user()->chats()->where('id', $id)->first();
        if ($chat == null) {
            return ResponseEntity::notFound(
                null,
                "La conversation n'existe pas, ou ne vous appartient pas !"
            );
        }


        $this->chatService->delete($chat->id);

        return ResponseEntity::ok(null, "Votre conversation a été supprimée");
    }

    /**
     * @OA\Patch  (path="/api/social/chats/{chat}/leave", tags={"Forum : Conversations"}, security={{ "apiAuth": {} }},
     *     description="Quitter une conversation", summary="Quitter une conversation",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *     @OA\Parameter(
     *         name="chat",
     *         in="path",
     *         description="Identifiant de la conversation",
     *         required=true,
     *         style="form"
     *     ),
     *      @OA\Response(response="200", description="Vous avez quitter la conversation"
     *     ))
     * )
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function leave(Request $request, $id)
    {
        $user = $request->user();
        $chatMember = ChatMember::where('user_id', $user->id)->where('chat_id', $id)->first();
        if ($chatMember == null) {
            return ResponseEntity::notFound(null, "La conversation n'existe !");
        } else if ($chatMember->chat()->first()->user()->first()->id == $user->id) {
            return ResponseEntity::unauthorized(null, "Vous ne pouvez pas quitter votre conversation !");
        } else if (!$chatMember->delete()) {
            return ResponseEntity::badRequest(null, "Impossible de quitter la conversation");
        }

        return ResponseEntity::ok(null, "Vous avez quitter la conversation");
    }

    /**
     * @OA\Get (path="/api/social/chats/{chat}/messages", tags={"Forum : Conversations"}, security={{ "apiAuth": {} }},
     *     description="Voir les messages de ma conversation", summary="Voir les messages de ma conversation",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *      @OA\Parameter(
     *         name="page",
     *         in="query",
     *         description="Pagination",
     *         style="form"
     *      ),
     *     @OA\Parameter(
     *         name="chat",
     *         in="path",
     *         description="Identifiant de la conversation",
     *         required=true,
     *         style="form"
     *     ),
     *      @OA\Response(response="200", description="Liste des messages de mes conversations",
     *          @OA\JsonContent(
     *              @OA\Property(property="current_page", type="number", example=1),
     *              @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/Message")),
     *              @OA\Property(property="first_page_url", type="string", example="https://cci.univ-tours.fr/api/social/chats/513/messages?page=1"),
     *              @OA\Property(property="from", type="number", example=1),
     *              @OA\Property(property="last_page", type="number", example=1),
     *              @OA\Property(property="last_page_url", type="string", example="https://cci.univ-tours.fr/api/social/chats/513/messages?page=1"),
     *              @OA\Property(property="next_page_url", type="string", example=null),
     *              @OA\Property(property="path", type="string", example="https://cci.univ-tours.fr/api/social/chats/513/messages"),
     *              @OA\Property(property="per_page", type="number", example=5),
     *              @OA\Property(property="prev_page_url", type="string", example=null),
     *              @OA\Property(property="to", type="number", example=2),
     *              @OA\Property(property="total", type="number", example=2),
     *     ))
     * )
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function messages(Request $request, $id)
    {
        $chat = Chat::where('id', $id)->first();

        if ($chat == null) {
            return ResponseEntity::notFound(null, "La conversation n'existe pas !");
        } else if (!$this->chatService->userCanAccessChat($request->user(), $chat)) {
            return ResponseEntity::unauthorized(null, "Vous n'avez pas accès à cette conversation");
        }

        $paginatedChatMessages = ChatMessage::where('chat_id', $chat->id)->paginate(10);
        $paginatedChatMessages->getCollection()->transform(function ($chatMessage) {
            return new ChatMessageDto($chatMessage);
        });

        return ResponseEntity::ok($paginatedChatMessages);
    }

    /**
     * @OA\Post (path="/api/social/chats/{chat}/messages", tags={"Forum : Conversations"}, security={{ "apiAuth": {} }},
     *     description="Envoyer un message dans une conversation", summary="Envoyer un message dans une conversation",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *      @OA\Parameter(
     *         name="chat",
     *         in="path",
     *         description="Identifiant de la conversation",
     *         required=true,
     *         style="form"
     *     ),
     *      @OA\RequestBody (
     *          @OA\MediaType(mediaType="multipart/form-data",
     *              @OA\Schema(required={"text"},
     *                  @OA\Property(property="text", type="string", description="Message à envoyer"),
     *                  @OA\Property(property="attachments", type="file", description="Pièce jointe au message")
     *              )
     *          ),
     *      ),
     *      @OA\Response(response="201", description="Message envoyé dans la socket",
     *     ))
     * )
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function send(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'text' => ['required', 'string', 'min:2', 'max:500'],
            'attachments' => ['nullable','mimes:jpeg,jpg,png,doc,pdf,docx,jpg,gif,svg', 'max:2048'],
            'attachments.*' => ['nullable','mimes:jpeg,jpg,png,doc,pdf,docx,jpg,gif,svg', 'max:2048']
        ]);

        if ($validator->fails()) {
            return ResponseEntity::badRequest($validator->errors());
        } else if ($request->hasfile('attachments') && count($request->allFiles()) > $this->filesLimitNbr) {
            return ResponseEntity::badRequest(
                null,
                "Vous ne pouvez qu'envoyer " .$this->filesLimitNbr. " fichiers"
            );
        }

        $chat = Chat::where('id', $id)->first();

        if ($chat == null) {
            return ResponseEntity::notFound(null, "La conversation n'existe pas !");
        } else if (!$this->chatService->userCanAccessChat($request->user(), $chat)) {
            return ResponseEntity::unauthorized(null, "Vous n'avez pas accès à cette conversation");
        }

        return ResponseEntity::created(null, $this->chatService->send($request, $chat));
    }
}
