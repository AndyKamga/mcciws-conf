<?php

namespace App\Http\Controllers\Api;

use App\Dto\PostCategoryDto;
use App\Dto\UserFoundDto;
use App\Http\Responses\ResponseEntity;
use App\Models\PostCategory;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class SearchController extends Controller
{
    /**
     * @OA\Get (path="/api/search/users/{name}", tags={"Recherches"}, security={{ "apiAuth": {} }},
     *     description="Rechercher un utilisateur", summary="Rechercher un utilisateur",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *      @OA\Parameter(
     *         name="name",
     *         in="path",
     *         description="Nom et/ou prénom de l'utilisateur",
     *         required=true,
     *         style="form"
     *      ),
     *      @OA\Response(response="200", description="Utilisateurs trouvés",
     *          @OA\JsonContent(
     *              @OA\Property(property="current_page", type="number", example=1),
     *              @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/UserFound")),
     *              @OA\Property(property="first_page_url", type="string", example="https://cci.univ-tours.fr/api/search/user/{name}?page=1"),
     *              @OA\Property(property="from", type="number", example=1),
     *              @OA\Property(property="last_page", type="number", example=1),
     *              @OA\Property(property="last_page_url", type="string", example="https://cci.univ-tours.fr/api/search/user/{name}?page=1"),
     *              @OA\Property(property="next_page_url", type="string", example=null),
     *              @OA\Property(property="path", type="string", example="https://cci.univ-tours.fr/api/search/user/{name}"),
     *              @OA\Property(property="per_page", type="number", example=5),
     *              @OA\Property(property="prev_page_url", type="string", example=null),
     *              @OA\Property(property="to", type="number", example=2),
     *              @OA\Property(property="total", type="number", example=2),
     *     ))
     * )
     * @param $name
     * @return JsonResponse
     */
    public function users($name)
    {
        if (!$this->isValidSearchParameter($name)) {
            return ResponseEntity::badRequest(
                null,
                "le nombre de caractère de votre saisie doit être supérieur ou égale à 3"
            );
        }

        $paginatedUsers = User::where('prenom', 'like', '%' . $name . '%')
            ->orWhere('nom', 'like', '%' . $name . '%')->paginate(10);
        $paginatedUsers->getCollection()->transform(function ($user) {
            return new UserFoundDto($user);
        });

        return ResponseEntity::ok($paginatedUsers);
    }

    /**
     * @OA\Get (path="/api/search/categories/{name}", tags={"Recherches"}, security={{ "apiAuth": {} }},
     *     description="Rechercher une catégorie", summary="Rechercher une catégorie",
     *      @OA\SecurityScheme (securityScheme="bearerAuth", in="header", name="bearerAuth", type="http",
     *       scheme="bearer", bearerFormat="JWT"),
     *      @OA\Parameter(
     *         name="name",
     *         in="path",
     *         description="Libéllé de la catégorie",
     *         required=true,
     *         style="form"
     *      ),
     *      @OA\Response(response="200", description="Catégories trouvés",
     *          @OA\JsonContent(
     *              @OA\Property(property="current_page", type="number", example=1),
     *              @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/Categorie")),
     *              @OA\Property(property="first_page_url", type="string", example="https://cci.univ-tours.fr/api/search/categories/{name}?page=1"),
     *              @OA\Property(property="from", type="number", example=1),
     *              @OA\Property(property="last_page", type="number", example=1),
     *              @OA\Property(property="last_page_url", type="string", example="https://cci.univ-tours.fr/api/search/categories/{name}?page=1"),
     *              @OA\Property(property="next_page_url", type="string", example=null),
     *              @OA\Property(property="path", type="string", example="https://cci.univ-tours.fr/api/search/categories/{name}"),
     *              @OA\Property(property="per_page", type="number", example=5),
     *              @OA\Property(property="prev_page_url", type="string", example=null),
     *              @OA\Property(property="to", type="number", example=2),
     *              @OA\Property(property="total", type="number", example=2),
     *     ))
     * )
     * @param $name
     * @return JsonResponse
     */
    public function categories($name)
    {
        if (!$this->isValidSearchParameter($name)) {
            return ResponseEntity::badRequest(
                null,
                "le nombre de caractère de votre saisie doit être supérieur ou égale à 3"
            );
        }

        $paginatedCategories = PostCategory::where('name', 'like', '%' . $name . '%')->paginate(10);
        $paginatedCategories->getCollection()->transform(function ($category) {
            return new PostCategoryDto($category);
        });

        return ResponseEntity::ok($paginatedCategories);
    }

    /**
     * @description Permet de vérifier que la recherche est correcte
     * @param $name
     * @return bool
     */
    private function isValidSearchParameter($name)
    {
        return isset($name) && strlen($name) >= 3;
    }
}
