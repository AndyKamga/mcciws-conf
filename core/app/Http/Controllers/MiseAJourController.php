<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class MiseAJourController extends Controller
{
    private $response = [];

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $results = $this->response;

        return view(
            'backoffice.mise-a-jour-site',
            compact('results')
        );
    }

    public function update()
    {
        $action = request('action');

        if ($action == 1) {
            $this->response[] = shell_exec('git pull');
            $this->response[] = shell_exec('cd laravel_app ; composer update');
            //$this->response[] = shell_exec('cd laravel_app ; php artisan migrate');
            Artisan::call('migrate');
        } elseif ($action == 2) {
            $this->response[] = Artisan::call('down');
        } elseif ($action == 3) {
            $this->response[] = Artisan::call('storage:link');
        }

        $results = $this->response;

        return redirect('mise-a-jour-site')->with('results-update', $results[0]);
    }

    public function laravel()
    {
        $commande = request('commande');
        $this->response[] = shell_exec('cd laravel_app ; '.$commande);
        $results = $this->response;

        return redirect('mise-a-jour-site')->with('results-laravel', $results[0]);
    }

    public function artisan()
    {
        $commande = request('commande');
        Artisan::call($commande);

        return redirect('mise-a-jour-site')->with('results-artisan', 'Commande réussie !');
    }

    public function commande()
    {
        $commande = request('commande');
        $this->response[] = shell_exec($commande);

        $results = $this->response;

        return redirect('mise-a-jour-site')->with('results-commande', $results[0]);
    }

    public function maintenance()
    {
        MiseAJourController::launchCommande('php artisan up');
    }

    public function makestorage()
    {
        return Artisan::call('storage:link');
    }

    public function launchCommande($commandes)
    {
        $consoleResults = [];
        foreach ($commandes as $commande) {
            $process = new Process($commande);

            $process->setWorkingDirectory(base_path());

            $process->run();

            if ($process->isSuccessful()) {
                $consoleResults[] = ['success', $process->getOutput()];
            } else {
                $consoleResults[] = ['error', new ProcessFailedException($process)];
            }
        }

        return $consoleResults;
    }
}
