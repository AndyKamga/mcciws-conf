<?php

namespace App\Http\Controllers;

use App\Models\Projet;

class ProjetsController extends Controller
{
    // Projet des étudiants

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $results1 = Projet::all();

        return view('backoffice.gestion-des-projets', compact('results1'));
    }

    public function add()
    {
        $titre = request('titre_projet');
        $description = request('description_projet');
        $langage_competence = request('langage_competence');
        $date = request('date');

        Projet::add($titre, $description, $langage_competence, $date);

        return back()->withMessage('Le projet a été ajouté !');
    }

    public function update()
    {
        Projet::where('id_projet', request('id_update'))
            ->update(
                [
                    'titre' => ucfirst(request('Titre_update')),
                    'description' => ucfirst(request('Description_update')),
                    'langage_competence' => ucfirst(request('Langage_competence_update')),
                    'date' => request('date_update')
                ]
            );

        return back()->withMessage('Le projet a été mis à jour !');
    }

    public function delete()
    {
        $id_projet = request('id_projet');
        Projet::where('id_projet', $id_projet)->delete();

        return back()->withMessage('Le projet a été supprimé !');
    }
}
