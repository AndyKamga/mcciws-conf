<?php

namespace App\Http\Controllers;

use App\Services\ProfilService;

class MonProfilController extends Controller
{
    // Mon profil

    /**
     * @var ProfilService
     */
    private $profilService;

    public function __construct(ProfilService $profilService)
    {
        $this->middleware('auth');
        $this->profilService = $profilService;
    }

    public function index()
    {
        $results = $this->profilService->getUserSituation(auth()->user()->id);

        return view('backoffice.mon-profil', compact('results'));
    }

    public function update()
    {
        return $this->profilService->updateProfil();
    }

    public function updatePassword()
    {
        return $this->profilService->updatePassword();
    }
}
