<?php

namespace App\Http\Controllers;

use App\Models\Droit;
use App\Services\RoleManagerService;

class GestionDroitsController extends Controller
{
    /**
     * @var RoleManagerService
     */
    private $roleManagerService;

    public function __construct(RoleManagerService $roleManagerService)
    {
        $this->middleware('auth');
        $this->roleManagerService = $roleManagerService;
    }

    public function index()
    {
        $listeDroits = Droit::all();
        $routeList = Droit::getRoutes();

        return view(
            'backoffice/gestion-des-droits',
            compact('listeDroits', 'routeList')
        );
    }

    public function add()
    {
        return $this->roleManagerService->add();
    }

    public function update()
    {
        return $this->roleManagerService->update();
    }
}
