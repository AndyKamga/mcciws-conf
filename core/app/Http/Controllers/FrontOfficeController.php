<?php

namespace App\Http\Controllers;

use App\Models\DatesImportante;
use App\Models\Entreprise;
use App\Models\Projet;
use App\Models\Temoignage;
use App\Services\FrontOfficeService;

class FrontOfficeController extends Controller
{
    private $frontOfficeService;

    public function __construct(FrontOfficeService $frontOfficeService)
    {
        $this->frontOfficeService = $frontOfficeService;
    }

    public function accueil()
    {
        $results = Temoignage::findSummaryTestimony();
        return view('frontoffice.accueil', compact('results'));
    }

    public function motsDuResponsable()
    {
        return view('frontoffice.mots-du-responsable');
    }

    public function temoignages()
    {
        $results = Temoignage::findTestimony();
        return view('frontoffice.temoignages', compact('results'));
    }

    public function programme()
    {
        return view('frontoffice.programme');
    }

    public function candidater()
    {
        $results = DatesImportante::findAll();
        return view('frontoffice.candidater', compact('results'));
    }

    public function entreprises()
    {
        $results1 = Entreprise::entreprises();
        $results2 = Entreprise::situations();

        return view('frontoffice.entreprises', compact('results1', 'results2'));
    }

    public function envoiOffreFichier()
    {
        return $this->frontOfficeService->envoiOffreFichier();
    }

    public function envoiOffreTexte()
    {
        return $this->frontOfficeService->envoiOffreTexte();
    }

    public function projetsDesEtudiants()
    {
        $annee = request('annee');
        $results1 = Projet::dateProjetsEtudiants();
        $results2 = Projet::projetsEtudiants($annee);

        return view(
            'frontoffice.projets-des-etudiants',
            compact('results1', 'results2', 'annee')
        );
    }

    public function nousTrouver()
    {
        return view('frontoffice.nous-trouver');
    }

    public function envoiDemande()
    {
        return $this->frontOfficeService->contact();
    }

    public function mentionsLegales()
    {
        return view('frontoffice.mentions-legales');
    }

    public function enregistrement()
    {
        return view('frontoffice.enregistrement');
    }

    public function verificationEnreg()
    {
        return $this->frontOfficeService->inscription();
    }
}
