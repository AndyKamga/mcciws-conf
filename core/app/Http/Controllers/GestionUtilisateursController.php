<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\UserManagerService;

class GestionUtilisateursController extends Controller
{
    /**
     * @var UserManagerService
     */
    private $userManagerService;

    public function __construct(UserManagerService $userManagerService)
    {
        $this->middleware('auth');
        $this->userManagerService = $userManagerService;
    }

    public function index()
    {
        $results1 = User::getAllUser();
        $results2 = User::getAccount();

        $notif1 = User::getStudentNumber();
        $notif2 = User::getOldStudentNumber();
        $notif3 = User::getAdminNumber();
        $notif4 = User::getInactifNumber();
        $notif5 = User::getUserNumber();

        return view(
            'backoffice/gestion-des-utilisateurs',
            compact(
                'results1',
                'results2',
                'notif1',
                'notif2',
                'notif3',
                'notif4',
                'notif5'
            )
        );
    }

    public function add()
    {
        return $this->userManagerService->add();
    }

    public function update()
    {
        return $this->userManagerService->update();
    }
}
