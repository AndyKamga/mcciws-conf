<?php

namespace App\Http\Controllers;

use App\Models\Offre;

class GestionOffresStageController extends Controller
{
    // Gestion des offres de stage

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $results1 = Offre::getOffres();

        $notif1 = Offre::getOffresValidNumber();
        $notif2 = Offre::getOffresNONValidNumber();
        $notif3 = Offre::getOffresPourvueNumber();

        return view(
            'backoffice/gestion-offres-de-stage',
            compact('results1', 'notif1', 'notif2', 'notif3')
        );
    }

    public function update()
    {
        $id_offre = request('id_offre');
        $lien = request('link');

        if (isset($_REQUEST['delete_offre'])) {
            Offre::supprimerOffre($id_offre);
            unlink(storage_path('app/public/'.$lien));

            return back()->withMessage("L'offre de stage a été supprimée !");
        } else {
            Offre::validerOffre($id_offre);
        }

        return back()->withMessage("L'offre de stage est active !");
    }

    public function disable()
    {
        $id_offre = request('id_offre');
        Offre::desactiverOffre($id_offre);

        return back()->withMessage("L'offre de stage a été déactivée !");
    }
}
