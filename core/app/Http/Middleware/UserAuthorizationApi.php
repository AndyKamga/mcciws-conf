<?php

namespace App\Http\Middleware;

use App\Http\Responses\ResponseEntity;
use App\Models\Droit;
use Closure;
use Http\Client\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Lcobucci\JWT\Parser;

class UserAuthorizationApi
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            $id = (new Parser())->parse($request->bearerToken())->getHeader('jti');
            $token = $request->user()->token()->find($id);

            if ($token) {
                // Récupération du droit de l'utilisation
                $droit = $request->user()->droits;

                // Vérification de la permission
                $routeNameRequest = Route::getCurrentRoute()->getName();
                $permission = Droit::where('id_droit', $droit)->first()->permission;
                $permissionArray = json_decode($permission, true);

                if (in_array($routeNameRequest, $permissionArray)) {
                    return $next($request);
                }
            }
        } catch(\Exception $e) {}

        return ResponseEntity::unauthorized();
    }
}
