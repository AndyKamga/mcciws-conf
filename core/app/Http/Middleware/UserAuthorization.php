<?php

namespace App\Http\Middleware;

use App\Models\Droit;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class UserAuthorization
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // Récupération du droit de l'utilisation
        $id = auth()->user()->droits;

        // Vérification de la permission
        $routeNameRequest = Route::getCurrentRoute()->getName();
        $permission = Droit::where('id_droit', $id)->first()->permission;

        $permissionArray = json_decode($permission, true);

        if (in_array($routeNameRequest, $permissionArray)) {
            return $next($request);
        }

        return redirect()->back();
    }
}
