<?php

namespace App\Services;

use App\Models\Entreprise;
use App\Models\Rapport;
use App\Models\Stage;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class ReportService
{
    public function findAll($id_user)
    {
        $rapportExist = User::hasRapport($id_user);
        $entrStag = $this->getEntrepriseStage($id_user);
        $confidentialite = $this->getConfidentialite($id_user);
        $dateLimite = $this->getDateLimitDepo();
        $tempsRestant = $this->getTempsRestant($dateLimite);

        // Date limite en français
        $dateLimite = $this->dateToFrench($dateLimite);

        if ($rapportExist == 0) {
            $rapportStage = null;
            $noteSynthese = null;
            $dateDepo = null;
            $confidentialite = null;

            return view(
                'backoffice.mon-rapport',
                compact(
                    'rapportExist',
                    'entrStag',
                    'dateLimite',
                    'tempsRestant',
                    'rapportStage',
                    'noteSynthese',
                    'dateDepo',
                    'confidentialite'
                )
            );
        } else {
            $rapportStage = $this->getRapportDepoUser($id_user);
            $noteSynthese = $this->getNoteSynDepoUser($id_user);
            $dateDepo = $this->getDateDepoRapportUser($id_user);

            return view(
                'backoffice.mon-rapport',
                compact(
                    'rapportExist',
                    'entrStag',
                    'dateLimite',
                    'tempsRestant',
                    'rapportStage',
                    'noteSynthese',
                    'dateDepo',
                    'confidentialite'
                )
            );
        }
    }

    public function update()
    {
        // Règles de validation du formulaire
        request()->validate(
            [
                'nomEntrep' => ['required'],
                'sujetstage' => ['required'],
            ]
        );

        $id_user = auth()->user()->id;
        $rapportExist = request('rapportExist');
        $nomEntreprise = request('nomEntrep');
        $sujetStage = request('sujetstage');
        $confidentialite = request('confidentialite');
        $pathRapport = '';
        $pathNote = '';

        if ($confidentialite == 'on') {
            $confidentialite = 1;
        } else {
            $confidentialite = 0;
        }

        //On stocke le rapport dans le répertoire "rapport de stage"

        if (request('rapportDeStage') != null) {
            $pathRapport = request('rapportDeStage')->store(
                'rapports_de_stage',
                'public'
            );
        }

        //On stocke la note de synthèse dans le répertoire "notes de synthèse"
        if (request('noteSynthese') != null) {
            $pathNote = request('noteSynthese')->store(
                'notes_de_synthese',
                'public'
            );
        }

        if ($rapportExist == 0) {
            // Si l'utilisateur n'a jamais envoyé son rapport on ajoute le rapport de stage et sa notes dans la table rapport
            $this->ajoutRapport(
                $sujetStage,
                $nomEntreprise,
                $pathRapport,
                $pathNote,
                $id_user,
                $confidentialite
            );
        } else {
            $rapportStage = $this->getRapportDepoUser($id_user);
            $noteSynthese = $this->getNoteSynDepoUser($id_user);

            // On supprime les fichiers dans le répertoire

            if (request('rapportDeStage') != null) {
                unlink(storage_path('app/public/'.$rapportStage));
            } else {
                $pathRapport = $rapportStage;
            }

            if (request('noteSynthese') != null) {
                unlink(storage_path('app/public/'.$noteSynthese));
            } else {
                $pathNote = $noteSynthese;
            }

            // Mise à jour de la table rapports et des tables liés
            $this->updateRapport(
                $sujetStage,
                $nomEntreprise,
                $pathRapport,
                $pathNote,
                $id_user,
                $confidentialite
            );

            return back()->withMessage('Vos documents ont été mis à jour !');
        }

        return back()->withMessage('Vos documents ont été envoyés !');
    }

    public function ajoutRapport($sujetStage, $nomEntreprise, $pathRapport, $pathNote, $id_user, $confidentialite)
    {
        $stages = new Stage;
        $rapports = new Rapport;

        $nomEntreprise = ucfirst($nomEntreprise);
        $sujetStage = ucfirst($sujetStage);

        // On récupère le id de l'entreprise
        $profilService = new ProfilService();
        $id_entreprise = $profilService->getIdEntreprise($nomEntreprise);

        // On insère les données dans la table stage
        $stages->sujet_stage = $sujetStage;
        $stages->id_entr = $id_entreprise;
        $stages->statut = 0;

        $stages->save();

        // On récupère le id du stage
        $request = DB::select("SELECT id_stage FROM stages WHERE id_entr = $id_entreprise");
        $id_stage = $request[0]->id_stage;

        // On ajoute le rapport de stage dans la table rapport
        $datetime = date('Y-m-d H:i:s');
        $rapports->rapport_de_stage = $pathRapport;
        $rapports->note_synthese = $pathNote;
        $rapports->id_user = $id_user;
        $rapports->id_stage = $id_stage;
        $rapports->date = $datetime;
        $rapports->confidentialite = $confidentialite;

        $rapports->save();
    }

    public function updateRapport($sujetStage, $nomEntreprise, $pathRapport, $pathNote, $id_user, $confidentialite)
    {
        $nomEntreprise = ucfirst($nomEntreprise);
        $sujetStage = ucfirst($sujetStage);

        // Mise à jour de la table rapports
        $datetime = date('Y-m-d H:i:s');

        Rapport::where('id_user', $id_user)
            ->update(['rapport_de_stage' => $pathRapport, 'note_synthese' => $pathNote, 'date' => $datetime, 'confidentialite' => $confidentialite]);

        $request = DB::select("SELECT id_stage FROM rapports WHERE id_user=$id_user");
        $id_stage = $request[0]->id_stage;

        // Mise à jour de la table stage

        Stage::where('id_stage', $id_stage)
            ->update(['sujet_stage' => $sujetStage]);

        $request = DB::select("SELECT id_entr FROM stages WHERE id_stage=$id_stage");
        $id_entr = $request[0]->id_entr;

        // Mise à jour de la table entreprise
        Entreprise::where('id_entr', $id_entr)
            ->update(['nom_entr' => $nomEntreprise]);
    }

    public function getDateDepoRapportUser($id_user)
    {
        $request = DB::select("SELECT date FROM rapports WHERE id_user=$id_user");

        $heure = substr($request[0]->date, 11);
        $date = $this->dateToFrench(substr($request[0]->date, 0, -9));

        return $date.' à '.$heure;
    }

    public function getRapportDepoUser($id_user)
    {
        $request = DB::select("SELECT rapport_de_stage FROM rapports WHERE id_user=$id_user");
        return $request[0]->rapport_de_stage;
    }

    public function getNoteSynDepoUser($id_user)
    {
        $request = DB::select("SELECT note_synthese FROM rapports WHERE id_user=$id_user");
        return $request[0]->note_synthese;
    }

    public function getEntrepriseStage($id_user)
    {
        $request = DB::select("SELECT entreprises.nom_entr, stages.sujet_stage FROM 
        ((rapports INNER JOIN stages ON rapports.id_stage = stages.id_stage) INNER JOIN entreprises  
        ON stages.id_entr = entreprises.id_entr) WHERE id_user=$id_user");

        /*
        Un traitement particulier est appliqué au résultat de la requête dans le cas actuel.
        Pour éviter un bug si jamais la requête n'a pas de résultat
        */
        if (count(array_keys($request)) > 0) {
            return [
                'nom_entr' => $request[0]->nom_entr,
                'sujet_stage' => $request[0]->sujet_stage];
        }

        return ['nom_entr' => '', 'sujet_stage' => ''];
    }

    public function getDateLimitDepo()
    {
        $request = DB::select("SELECT date FROM dates_importantes WHERE cle='depot_rapports'");
        return $request[0]->date;
    }

    public function getTempsRestant($dateLimite)
    {
        if (date('Y-m-d') >= $dateLimite) {
            return 'Temps écoulé';
        }

        $datetimeActuelle = date_create(date('Y-m-d H:i:s'));
        $dateLimite = date_create($dateLimite);

        $tempsRestant = date_diff($datetimeActuelle, $dateLimite);
        $tempsRestantFormat = $tempsRestant->format('%Y:%M:%D:%H:%I:%S');

        list($annee, $mois, $jour, $heure, $minute, $seconde) = preg_split('[:]', $tempsRestantFormat);

        // Texte au pluriel ou au singulier
        $textAnnee = ' an ';
        $textMois = ' mois ';
        $textJour = ' jour ';
        $textHeure = ' heure ';
        $textMinute = ' minute ';
        $textSeconde = ' seconde ';

        if ($annee > 1) {
            $textAnnee = ' ans ';
        }
        if ($jour > 1) {
            $textJour = ' jours ';
        }
        if ($heure > 1) {
            $textHeure = ' heures ';
        }
        if ($minute > 1) {
            $textMinute = ' minutes ';
        }
        if ($seconde > 1) {
            $textSeconde = ' secondes ';
        }

        $annee = round($annee);
        $mois = round($mois);
        $jour = round($jour);
        $heure = round($heure);
        $minute = round($minute);
        $seconde = round($seconde);

        if ($annee > 0) {
            return $annee.$textAnnee.' '.$mois.$textMois.' '.$jour.$textJour;
        }
        if ($mois > 0) {
            return $mois.$textMois.' '.$jour.$textJour.' '.$heure.$textHeure;
        }
        if ($jour > 0) {
            return $jour.$textJour.' '.$heure.$textHeure.' '.$minute.$textMinute;
        }
        if ($heure > 0) {
            return $heure.$textHeure.' '.$minute.$textMinute.' '.$seconde.$textSeconde;
        }
        if ($minute > 0) {
            return $minute.$textMinute.' '.$seconde.$textSeconde;
        }

        return $seconde.$textSeconde;
    }

    public function dateToFrench($englishDate)
    {
        $annee = substr($englishDate, 0, -6);
        $mois = substr($englishDate, 5, -3);
        $jour = substr($englishDate, 8);

        $moisFrancais = '';
        switch ($mois) {
            case 1:
                $moisFrancais = 'janvier';
                break;
            case 2:
                $moisFrancais = 'février';
                break;
            case 3:
                $moisFrancais = 'mars';
                break;
            case 4:
                $moisFrancais = 'avril';
                break;
            case 5:
                $moisFrancais = 'mai';
                break;
            case 6:
                $moisFrancais = 'juin';
                break;
            case 7:
                $moisFrancais = 'juillet';
                break;
            case 8:
                $moisFrancais = 'août';
                break;
            case 9:
                $moisFrancais = 'septembre';
                break;
            case 10:
                $moisFrancais = 'octobre';
                break;
            case 11:
                $moisFrancais = 'novembre';
                break;
            case 12:
                $moisFrancais = 'décembre';
                break;
        }

        $date = $jour.' '.$moisFrancais.' '.$annee;
        return $date;
    }

    public function getConfidentialite($id_user)
    {
        $confidentialite = 0;
        $request = DB::select("SELECT COUNT(confidentialite) as nbr FROM rapports WHERE id_user=$id_user");
        if ($request[0]->nbr > 0) {
            $request = DB::select("SELECT confidentialite FROM rapports WHERE id_user=$id_user");
            $confidentialite = $request[0]->confidentialite;
        }

        return $confidentialite;
    }
}
