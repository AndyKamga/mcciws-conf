<?php

namespace App\Services;

use App\Models\Droit;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class UserManagerService
{
    public function add()
    {
        $nom = ucfirst(request('nom'));
        $prenom = ucfirst(request('prenom'));
        $email = request('email');
        $annee = date(request('annee'));
        $typecompte = request('typecompte');
        $password = bcrypt(str_random(8));

        if ($this->userExist($email)) {
            return back()->withMessage("L'adresse email existe déjà");
        }

        // On récupère l'id du compte
        $droit = Droit::where('description', $typecompte)->first();
        $droit = $droit->id_droit;

        // On insère l'utilisateur dans la base de données
        $utilisateur = new User;

        $utilisateur->nom = $nom;
        $utilisateur->prenom = $prenom;
        $utilisateur->annee = $annee;
        $utilisateur->email = $email;

        $utilisateur->password = $password;
        $utilisateur->photo = 'avatars/default.png';
        $utilisateur->droits = $droit;

        $utilisateur->save();

        return back()->withMessage("L'utilisateur a été ajouté !");
    }

    public function update()
    {
        $id_user = request('id');
        $nom = ucfirst(request('nom'));
        $prenom = ucfirst(request('prenom'));
        $email = request('email');
        $annee = date(request('annee'));
        $typecompte = request('typecompte');
        $password = request('password');

        // On récupère l'id de la description du compte
        $droit = Droit::where('description', $typecompte)->first();
        $droit = $droit->id_droit;

        // On met à jour l'utilisateur dans la base de données (Requête de type eloquent-> plus rapide)
        DB::table('users')
            ->where('id', $id_user)
            ->update(['nom' => $nom, 'prenom' => $prenom, 'annee' => $annee, 'droits' => $droit]);

        // On met à jour le mot de passe de l'utilisateur si il est renseigné

        if ($password != null) {
            DB::table('users')
                ->where('id', $id_user)
                ->update(
                    ['password' => bcrypt($password)]
                );
        }

        DB::table('users')
            ->where('id', $id_user)
            ->update(
                ['email' => $email]
            );

        return back()->withMessage("L'utilisateur a été mis à jour !");
    }

    private function userExist($email)
    {
        return User::where('email', $email)->count() > 0;
    }
}
