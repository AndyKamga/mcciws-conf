<?php


namespace App\Services;

use App\Dto\PostCategoryDto;
use App\Http\Responses\ResponseEntity;
use App\Models\PostCategory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PostCategoryService extends Service
{
    /**
     * @description Permet de créer une catégorie
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        try {
            $category = PostCategory::create([
                'name' => $request->input('name'),
                'description' => $request->input('description')
            ]);
        } catch (\Exception $e) {
            return ResponseEntity::badRequest(
                null,
                "Impossible de créer la catégorie '".$request->input('name').
                "'. Il se peut quelle existe déjà !"
            );
        }

        return ResponseEntity::created(new PostCategoryDto($category));
    }

    /**
     * @description Permet de modifier une catégorie
     * @param Request $request
     * @param $categoryId
     * @return JsonResponse
     */
    public function update(Request $request, $categoryId)
    {
        $category = PostCategory::where('id', $categoryId)->first();
        if ($category == null) {
            return ResponseEntity::notFound(null, "La catégorie n'existe pas !");
        }

        try {
            $category->name = $request->input('name');
            $category->description = $request->input('description');
            $category->save();
        } catch (\Exception $e) {
            return ResponseEntity::badRequest(
                null,
                "Impossible de modifier la catégorie '".$request->input('name')."'"
            );
        }

        return ResponseEntity::ok(new PostCategoryDto($category));
    }
}
