<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class MessagesService
{
    public function add()
    {
        $To = request('To');
        $repl = request('repl');
        $subj = request('subj');
        $msg = request('msg');
        $pathToFile = request('piece-jointe');

        if ($this->sendMail($To, $repl, $subj, $msg, $pathToFile)) {
            return redirect()->back();
        }

        $this->mailSended($To, $repl, $subj, $msg, $pathToFile);

        return back()->withMessage('Message envoyé !');
    }

    private function sendMail($To, $repl, $subj, $msg, $pathToFile)
    {
        // On récupère les adresses email
        if ($To < 0) {
            $request = DB::select('SELECT email FROM users');
        } elseif ($To == 2) {
            $request = DB::select('SELECT email FROM users WHERE droits NOT IN (0, 1, 4, 5)');
        } else {
            $request = DB::select("SELECT email FROM users WHERE droits = $To");
        }

        // On stocke les adresses email dans un tableau
        $ToA = [];
        $ok = false;
        $i = 0;
        foreach ($request as $r) {
            $ToA[$i] = ($r->email);
            $ok = true;
            $i++;
        }

        if ($ok) {
            Mail::raw($msg, function ($message) use ($ToA, $repl, $subj, $pathToFile) {
                $message->from('no-reply@mastercci.fr', 'Master CCI Tours');

                $message->subject($subj);

                //$message->to('foo@example.com')->cc('bar@example.com');
                //$message->bcc('bar@example.com');

                $message->bcc($ToA);

                $message->replyTo($repl);

                if ($pathToFile != null) {
                    foreach ($pathToFile as $file) {
                        $message->attach(
                            $file->getRealPath(),
                            [
                                'as' => $file->getClientOriginalName(),
                                'mime' => $file->getMimeType()]
                        );
                    }
                }
            });

            // check for failures
            return Mail::failures();
        } else {
            return true;
        }
    }

    private function mailSended($To, $repl, $subj, $msg, $pathToFile)
    {
        if ($To < 0) {
            $request = DB::select('SELECT email FROM users');
            $ToAl = 'Tous';
        } else {
            $request = DB::select("SELECT email FROM users WHERE droits = $To");
            $ToAld = DB::select("SELECT * FROM droits WHERE id_droit =$To");
            $ToAl = ($ToAld[0]->description);
        }

        $ToA = '';
        $i = 0;
        foreach ($request as $r) {
            if ($i > 0) {
                $ToA = "$ToA ; ";
            }
            $ToA = "$ToA".($r->email);
            $i++;
        }

        $mails = new \App\Models\Mail;

        $mails->correspondant = $repl;
        $mails->dest_alias = $ToAl;
        $mails->destinataires = $ToA;
        $mails->sujet = $subj;
        $mails->message = $msg;

        $mails->save();
    }
}
