<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;

class HeaderService
{
    public static function getCompte($id_user)
    {
        $request = DB::select("SELECT description FROM droits INNER JOIN users ON
         droits.id_droit = users.droits WHERE users.id = $id_user");

        return $request[0]->description;
    }

    public static function getRapport()
    {
        $result = DB::select('SELECT COUNT(id_user) as nbr FROM rapports 
        WHERE id_user IN (SELECT id FROM users WHERE droits != 1 AND droits != 4 AND droits != 5)');

        return $result[0]->nbr;
    }

    public static function getTemoignages()
    {
        $request
            = DB::select('SELECT COUNT(*) as nbr FROM temoignages WHERE statut=0');
        return $request[0]->nbr;
    }

    public static function getSituations()
    {
        $request = DB::select('SELECT COUNT(*) as nbr FROM postes WHERE 
        poste_occupe IS NOT NULL AND id_entr IS NOT NULL AND statut != 1');

        return $request[0]->nbr;
    }

    public static function getOffreValid()
    {
        $request
            = DB::select('SELECT COUNT(*) as nbr FROM offres WHERE statut=1');

        return $request[0]->nbr;
    }

    public static function getOffreNonValid()
    {
        $request
            = DB::select('SELECT COUNT(*) as nbr FROM offres WHERE statut=0');

        return $request[0]->nbr;
    }

    public static function getInscriptions()
    {
        $request
            = DB::select('SELECT COUNT(id_inscrip) as nbr FROM inscriptions');

        return $request[0]->nbr;
    }
}
