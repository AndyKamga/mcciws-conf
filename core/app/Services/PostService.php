<?php


namespace App\Services;

use App\Dto\PostDto;
use App\Dto\PostReportDto;
use App\Http\Responses\ResponseEntity;
use App\Models\Post;
use App\Models\PostComment;
use App\Models\PostLike;
use App\Models\PostReport;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PostService extends Service
{
    /**
     * @description Répertoire de stockage des pièces jointes
     * @var string
     */
    private $postFilesFolder = 'post_files';

    /**
     * @description Permet de créer une publication
     * @param Request $request
     * @return PostDto
     */
    public function create(Request $request)
    {
        $user = $request->user();
        $post = new Post([
            'title' => $request->input('title'),
            'html_text' => $request->input('html_text'),
            'iframe_code' => $request->input('iframe_code'),
            'is_pinned' => false
        ]);

        $post->user()->associate($user);
        $post->save();

        if ($request->hasfile('attachments')) {
            foreach ($request->allFiles() as $file) {
                $post->files()->create([
                    'name' => $file->getClientOriginalName(),
                    'path' => $file->store($this->postFilesFolder, 'public'),
                    'media_type' => $file->getClientMimeType(),
                    'post_id' => $post->id
                ]);
            }
        }

        return new PostDto($post, $user);
    }

    /**
     * @description Permet de supprimer une publication
     * @param $id
     * @return true | false
     */
    public function delete($id)
    {
        $post = Post::find($id);

        if ($post == null || !$this->userCanDelete($post->user()->first(), request()->user())) {
            return false;
        }

        foreach ($post->files()->get() as $postFile) {
            unlink(storage_path('app/public/'.$postFile->path));
        }

        return $post->delete();
    }

    /**
     * @description Aimer une publication
     * @param Request $request
     * @param $postId
     * @return bool
     */
    public function addLike(Request $request, $postId)
    {
        $userId = $request->user()->id;

        $likeNbr = PostLike::where('user_id', $userId)
            ->where('post_id', $postId)
            ->count();

        if ($likeNbr > 0) {
            return false;
        }

        PostLike::create([
            'user_id' => $userId,
            'post_id' => $postId
        ]);

        return true;
    }

    /**
     * @description Enlever le like d'une publication
     * @param Request $request
     * @param $postId
     * @return false
     */
    public function removeLike(Request $request, $postId)
    {
        $userId = $request->user()->id;

        $postLike = PostLike::where('user_id', $userId)
            ->where('post_id', $postId);

        if ($postLike == null) {
            return false;
        }

        return $postLike->delete();
    }

    /**
     * @description Permet de supprimer un commentaire
     * @param Request $request
     * @param $postId
     * @param $commentId
     * @return false | true
     */
    public function deleteComment(Request $request, $postId, $commentId)
    {
        $postComment =  PostComment::where('post_id', $postId)->where('id', $commentId)->first();
        if ($postComment == null || !$this->userCanDelete($postComment->user()->first(), $request->user())) {
            return false;
        }

        return $postComment->delete();
    }

    /**
     * @description Permet de vérifier si un utilisateur peurt supprimer une publication ou un commentaire
     * une publication ou un commentaire peut être supprimée par l'auteur de celle si ou pas un administrateur
     * @param User $user Utilisateur ayant créé la publication ou le commentaire
     * @param User $userRequest Utilisateur envoyant la requête
     * @return bool True / False
     */
    private function userCanDelete(User $user, User $userRequest)
    {
        return $user->id == $userRequest->id || in_array($userRequest->droit, [3, 4, 5]);
    }

    /**
     * @description Permet de signaler une publication
     * @param Request $request
     * @param $postId
     * @return JsonResponse
     */
    public function reportPost(Request $request, $postId)
    {
        $post = Post::find($postId);
        $user = $request->user();

        if ($post == null) {
            return ResponseEntity::notFound(null, "La publication n'existe pas !");
        }

        if (PostReport::where('user_id', $user->id)->where('post_id', $postId)->count() > 0) {
            return ResponseEntity::badRequest(null, "Vous avez déjà signalé cette publication !");
        }

        $postReport = new PostReport($request->all());
        $postReport->user()->associate($user);
        $postReport->post()->associate($post);

        $postReport->save();

        return ResponseEntity::created(new PostReportDto($postReport));
    }

    /**
     * @description Permet de signaler le commentaire d'une publication
     * @param Request $request
     * @param $postId
     * @param $commentId
     * @return JsonResponse
     */
    public function reportComment(Request $request, $postId, $commentId)
    {
        $postComment =  PostComment::where('post_id', $postId)->where('id', $commentId)->first();
        $user = $request->user();

        if ($postComment == null) {
            return ResponseEntity::notFound(null, "Le commentaire n'existe pas !");
        }

        $post = $postComment->post()->first();

        if ($post == null) {
            return ResponseEntity::notFound(null, "La publication n'existe pas !");
        }

        if (PostReport::where('user_id', $user->id)->where('post_id', $postId)->where('comment_id', $commentId)->count()) {
            return ResponseEntity::badRequest(null, "Vous avez déjà signalé ce commentaire !");
        }

        $postReport = new PostReport($request->all());
        $postReport->user()->associate($user);
        $postReport->comment()->associate($postComment);
        $postReport->post()->associate($post);

        $postReport->save();


        return ResponseEntity::created(new PostReportDto($postReport));
    }

    /**
     * Permet de déclarer un commentaire comme étant la meilleur réponse à une publication
     * @param Request $request
     * @param $commentId
     * @param $postComment
     * @return string
     */
    public function bestAnswer(Request $request, $commentId, $postComment)
    {
        $postComment->is_best_answer = $request->input('is_best_answer');
        $postComment->save();

        $post = $postComment->post()->first();

        if (!$postComment->is_best_answer) {
            $post->is_solved = false;
            return "Le commentaire n'est plus la meilleur réponse !";
        }

        PostComment::where('id', '!=', $commentId)
            ->update(['is_best_answer' => false]);

        $post->is_solved = true;
        $post->save();

        return "Le commentaire a été déclaré comme étant la meilleure réponse !";
    }
}
