<?php

namespace App\Services;

use App\Models\Demande;
use App\Models\Inscription;
use App\Models\Offre;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class FrontOfficeService
{
    public function envoiOffreFichier()
    {
        $validator = Validator::make(request()->all(), [
            'titre' => ['required'],
            'entreprise' => ['required'],
            'localisation' => ['required'],
            'fichier' => ['required', 'mimes:pdf', 'max:2048']
        ]);

        if ($validator->fails()) {
            return back()->withMessage('Veuillez vérifier le formulaire !');
        }

        $titre = request('titre');
        $entreprise = request('entreprise');
        $localisation = request('localisation');

        /*
            On stocke le fichier dans le répertoire "offres de stage"
            Si il n'existe pas, on Laravel le crée
        */
        $path = request('fichier')->store('offres_de_stage', 'public');

        Offre::ajouterOffreFichier($titre, $entreprise, $localisation, $path);

        // Notifition aux l'administrateurs par email
        $email = 'no-reply-offre@master-cci.fr';
        $sujet = 'Offre de stage ['.$titre.']';
        $message = "Bonjour,\n\nUne offre de stage de l'entreprise "
            .$entreprise." vient d'être déposée depuis le site du Master CCI.
        \nConnectez-vous à votre espace administrateur pour gérer l'offre :
        \nhttp://www.cci.univ-tours.fr/gestion-offres-de-stage.";

        $this->mailAdmins('Master CCI', $email, $sujet, $message);

        return back()->withMessage('Votre offre de stage a été envoyée !');
    }

    public function envoiOffreTexte()
    {
        $validator = Validator::make(request()->all(), [
            'titre' => ['required'],
            'entreprise' => ['required'],
            'localisation' => ['required'],
            'debut' => ['required'],
            'fin' => ['required'],
            'description' => ['required'],
        ]);

        if ($validator->fails()) {
            return back()->withMessage('Veuillez vérifier le formulaire !');
        }

        $titre = request('titre');
        $entreprise = request('entreprise');
        $localisation = request('localisation');
        $debut = request('debut');
        $fin = request('fin');
        $contact = request('contact');
        $mail = request('mail');
        $telephone = request('telephone');
        $description = request('description');

        Offre::ajouterOffreTexte(
            $titre,
            $entreprise,
            $localisation,
            $debut,
            $fin,
            $contact,
            $mail,
            $telephone,
            $description
        );

        // Notifition aux l'administrateurs par email
        $email = 'no-reply-offre@master-cci.fr';
        $sujet = 'Offre de stage ['.$titre.']';
        $message = "Bonjour,\n\nUne offre de stage de l'entreprise "
            .$entreprise." vient d'être déposée depuis le site du Master CCI.
        \nConnectez-vous à votre espace administrateur pour gérer l'offre :
        \nhttp://www.cci.univ-tours.fr/gestion-offres-de-stage.";

        $this->mailAdmins('Master CCI', $email, $sujet, $message);

        return back()->withMessage('Votre offre de stage a été envoyée !');
    }

    public function contact()
    {
        $validator = Validator::make(request()->all(), [
            'nom' => ['required'],
            'email' => ['required'],
            'sujet' => ['required'],
            'description' => ['required']
        ]);

        if ($validator->fails()) {
            return back()->withMessage('Veuillez vérifier votre saisie !');
        }

        $nom = request('nom');
        $email = request('email');
        $sujet = request('sujet');
        $description = request('description');

        $this->mailAdmins($nom, $email, $sujet, $description);

        Demande::ajouterDemande($nom, $email, $sujet, $description);

        return back()->withMessage('Votre message a été envoyée !');
    }

    public function inscription()
    {
        $validator = Validator::make(request()->all(), [
            'nom' => ['required'],
            'prenom' => ['required'],
            'email' => ['required', 'email'],
            'description' => ['required'],
            'annee' => ['required'],
            'password' => ['required', 'confirmed', 'min:6']
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator->errors());
        }

        $nom = ucfirst(request('nom'));
        $prenom = ucfirst(request('prenom'));
        $email = request('email');
        $annee = request('annee');
        $password = bcrypt(request('password'));
        $description = request('description');

        // Vérifier que l'adresse email ou le nom + prenom n'existe pas dans la table users de la base de données

        $userEmail = '';
        $verif1 = User::where('email', $email)->count() > 0;
        $verif2 = User::where('prenom', $prenom)->where('nom', $nom)->count() > 0;

        if ($verif1 == true) {
            $userEmail = $email;
        }

        if ($verif2 == true) {
            $user = User::where('prenom', $prenom)->where('nom', $nom)->first();
            $userEmail = $user->email;
        }

        if ($verif1 == true || $verif2 == true) {
            return redirect()->action('FrontOfficeController@enregistrement')
                ->with(
                    'erreur',
                    "Votre compte a déjà été crée avec l'adresse \""
                    .$userEmail."\". 
            Reinitialisé votre mot de passe pour vous connecter, ou contacté l'administrateur !"
                );
        }

        // Vérifier que que l'adresse email n'existe pas dans la table users_queue de la base de données
        if (Inscription::isInQueue($email)) {
            return back()->with(
                'erreur',
                'Vous avez déjà envoyé une demande de création de compte !'
            );
        }

        // Ajouter les informations dans la base de données (users_queue)
        Inscription::addToQueue(
            $nom,
            $prenom,
            $email,
            $description,
            $annee,
            $password
        );

        return back()->with(
            'correct',
            'Votre demande a été prise en compte. Vous serez notifié par email lorsque votre compte sera crée !'
        );
    }

    private function mailAdmins($nom, $email, $sujet, $msg)
    {
        $listMail = $request = DB::select('SELECT email FROM users WHERE droits=4');

        foreach ($listMail as $destinataires) {
            $adminMail = $destinataires->email;

            Mail::raw(
                $msg,
                function ($message) use ($nom, $email, $sujet, $adminMail) {
                    $message->from($email, $nom);
                    $message->to($adminMail)->subject($sujet);

                    // check for failures
                    return Mail::failures();
                }
            );
        }
    }
}
