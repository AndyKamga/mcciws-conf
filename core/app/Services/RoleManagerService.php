<?php

namespace App\Services;

use App\Models\Droit;

class RoleManagerService
{
    public function add()
    {
        $description = request('description');
        $routes = json_encode(request('routes'));

        $droit = Droit::orderBy('id_droit', 'desc')->first();
        $id_droit = $droit->id_droit + 1;

        $droits = new Droit();
        $droits->id_droit = $id_droit;
        $droits->description = $description;
        $droits->permission = $routes;

        $droits->save();

        return back()->withMessage('Le droit a été ajouté !');
    }

    public function update()
    {
        $id = request('idData');
        $description = request('description');
        $routes = json_encode(request('routes'));

        Droit::where('id_droit', $id)->update(
            [
                'description' => $description,
                'permission' => $routes,
            ]
        );

        return back()->withMessage('Le droit a été mis à jour !');
    }
}
