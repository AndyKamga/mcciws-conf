<?php


namespace App\Services;

use App\Dto\ChatDto;
use App\Dto\ChatMessageDto;
use App\Events\MessageCreatedEvent;
use App\Models\Chat;
use App\Models\ChatMember;
use App\Models\ChatMessage;
use App\Models\ChatMessageFile;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class ChatService extends Service
{
    /**
     * @description Répertoire de stockage des pièces jointes
     * @var string
     */
    private $postFilesFolder = 'chat_files';

    /**
     * Permet de créer un conversation
     *
     * @param Request $request
     * @param $members
     * @return ChatDto
     */
    public function create(Request $request, $members)
    {
        $user = $request->user();

        $chat = Chat::create([
            'name' => $request->input('name'),
            'user_id' => $user->id
        ]);

        foreach ($members as $member) {
            ChatMember::create([
                'user_id' => $member->id,
                'chat_id' => $chat->id
            ]);
        }

        return new ChatDto($chat, $members);
    }

    /**
     * @description Permet de vérifier qu'un uitilisateur a accès à un chat
     * @param User $user
     * @param Chat $chat
     * @return bool
     */
    public static function userCanAccessChat(User $user, Chat $chat)
    {
        $members = ChatMember::where('chat_id', $chat->id)->get();
        foreach ($members as $chatMember) {
            if ($chatMember->user()->first()->id == $user->id) {
                return true;
            }
        }

        return false;
    }

    /**
     * @description Permet d'envoyer un message
     * @param Request $request
     * @param $chat
     * @return string
     */
    public function send(Request $request, $chat)
    {
        $user = $request->user();
        $chatMessage = ChatMessage::create([
            'text' => $request->input('text'),
            'chat_id' => $chat->id,
            'user_id' => $user->id
        ]);

        if ($request->hasfile('attachments')) {
            foreach ($request->allFiles() as $file) {
                $chatMessage->files()->create([
                    'name' => $file->getClientOriginalName(),
                    'path' => $file->store($this->postFilesFolder, 'public'),
                    'media_type' => $file->getClientMimeType(),
                    'chat_message_id' => $chatMessage->id
                ]);
            }
        }

        $message = new ChatMessageDto($chatMessage);
        broadcast(new MessageCreatedEvent($message, $chat))->toOthers();

        return "Le message a été envoyé dans la socket !";
    }

    /**
     * @description Permet de supprimer les fichiers des messages sur le serveur
     * @param $id
     * @return void
     */
    public function delete($id)
    {
        try {
            foreach (ChatMessage::where('chat_id', $id)->get() as $message) {
                foreach (ChatMessageFile::where('chat_message_id', $message->id)->get() as $file) {
                    unlink(storage_path('app/public/'.$file->path));
                }
            }
            Chat::where('id', $id)->first()->delete();
        } catch (\Exception $e) {
            Chat::where('id', $id)->first()->delete();
        }
    }
}
