<?php

namespace App\Services;

use App\Models\Poste;
use App\Models\Temoignage;

class TestimonyService
{
    public function update($id, $id_user)
    {
        if (isset($_REQUEST['delete_temoignage'])) {
            Temoignage::where('id_temoignage', $id)->delete();

            return back()->withMessage('Le témoignage a été supprimé !');
        }

        $metier = request('metier');
        $resume = request('resume');
        $description = request('description');

        // On mets à jour le témoignage au cas ou l'administrateur a corrigé des fautes d'orthographe par exemple
        $this->updateUserTestimony($id_user, $resume, $metier, $description);

        // On active le témoignage
        Temoignage::validerTemoignage($id);

        return back()->withMessage('Le témoignage a été validé !');
    }

    private function updateUserTestimony($id_user, $resume, $metier, $description)
    {
        // On insère l'utilisateur dans la base de données
        $temoignages = new Temoignage;
        $postes = new Poste;

        // Mise à jour ou ajout du poste de l'utilisateur dans la base de données
        if (Poste::where('id_user', $id_user)->count() == 0) {
            $postes->poste_occupe = $metier;
            $postes->id_user = $id_user;
            $postes->save();
        } else {
            Poste::where('id_user', $id_user)->update(
                [
                    'poste_occupe' => $metier,
                    'statut' => 0
                ]
            );
        }

        // Mise à jour ou ajout du témoignage de l'utilisateur dans la base de données
        if (Temoignage::where('id_user', $id_user)->count() == 0) {
            $temoignages->resume = $resume;
            $temoignages->description = $description;
            $temoignages->id_user = $id_user;
            $temoignages->statut = 0;

            $temoignages->save();
        } else {
            Temoignage::where('id_user', $id_user)->update(
                [
                    'resume' => $resume,
                    'description' => $description,
                    'statut' => 0
                ]
            );
        }
    }

    public function setTemoignage($id_user)
    {
        $resume = request('resume');
        $metier = request('metier');
        $description = request('description');

        // On insère l'utilisateur dans la base de données
        $temoignages = new Temoignage;
        $postes = new Poste;

        // Mise à jour ou ajout du poste de l'utilisateur dans la base de données
        $postNbr = Poste::where('id_user', $id_user)->count();
        if ($postNbr == 0) {
            $postes->poste_occupe = $metier;
            $postes->id_user = $id_user;
            $postes->save();
        } else {
            Poste::where('id_user', $id_user)->update(
                [
                    'poste_occupe' => $metier,
                    'statut' => 0]
            );
        }

        // Mise à jour ou ajout du témoignage de l'utilisateur dans la base de données
        $temoignNbr = Temoignage::where('id_user', $id_user)->count();
        if ($temoignNbr == 0) {
            $temoignages->resume = $resume;
            $temoignages->description = $description;
            $temoignages->id_user = $id_user;
            $temoignages->statut = 0;

            $temoignages->save();
        } else {
            Temoignage::where('id_user', $id_user)
                ->update([
                    'resume' => $resume,
                    'description' => $description,
                    'statut' => 0
                ]);
        }
    }
}
