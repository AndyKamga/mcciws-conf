<?php

namespace App\Services;

use App\Models\Entreprise;
use App\Models\Poste;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class ProfilService
{
    public function updateProfil()
    {
        $id_user = auth()->user()->id;
        $email = request('email_user');
        $poste_occupe = ucfirst(request('poste_occupe'));
        $entreprise = ucfirst(request('nom_entr'));
        $statut = 0;

        // Mise à jour de l"adresse email

        $this->setEmail($id_user, $email);

        // Si l'utilisateur a saisi son poste on met le à jour

        if (! empty($poste_occupe)) {
            $this->setUserPoste($id_user, $poste_occupe, $statut);
        }

        // Si l'utilisateur a saisi le nom de son entreprise on le met à jour
        if (! empty($entreprise)) {
            $this->setUserEntreprise($id_user, $entreprise, $statut);
        }

        // Si l'utilisateur a uploadé une image on met à jour sa photo de profil

        if (request()->hasFile('avatar')) {
            request()->validate(
                [
                    'avatar' => ['image'],
                ]
            );

            //On supprime l'ancienne photo si elle n'est pas la photo par default

            if (auth()->user()->photo != 'avatars/default.png') {
                unlink(storage_path('app/public/'.auth()->user()->photo));
            }

            // On ajoute la nouvelle
            $path = request('avatar')->store('avatars', 'public');
            $this->setUserPhoto($id_user, $path);
        }

        return back()->withMessage('Votre profil a été mis à jour !');
    }

    public function updatePassword()
    {
        // Validation : Vérification des mots de passe

        $mdpActuel = request('password_actuel');

        if (\Hash::check(
            $mdpActuel,
            auth()->user()->password
        )
        ) {
            request()->validate(
                [
                    'password' => ['required', 'confirmed', 'min:6'],
                    'password_confirmation' => ['required'],
                ]
            );

            $mdpNouveau = request('password');

            $password = bcrypt($mdpNouveau);
            $id_user = auth()->user()->id;

            $this->setMotsDePasse($id_user, $password);

            return back()->withMessage('Votre mot de passe a été mis à jour !');
        } else {
            return back()->with(
                'erreur',
                "Le mot de passe saisi ne correspond pas à l'actuel."
            );
        }
    }

    public function getIdEntreprise($entreprise)
    {
        /*
        Verifie que l'entreprise existe dans la base de données
        Si l'entreprise existe on récupère son id, si non on l'insère et on récupère l'id
        */

        $entreprise = ucfirst($entreprise);

        $nombreEntr = DB::table('entreprises')
            ->where('nom_entr', $entreprise)
            ->count();

        if ($nombreEntr == 0) {
            $entreprises = new Entreprise;
            $entreprises->nom_entr = $entreprise;
            $entreprises->save();
        }

        $request = DB::table('entreprises')
            ->where('nom_entr', $entreprise)
            ->select('id_entr')->get();

        return $request[0]->id_entr;
    }

    public function getUserSituation($id_user)
    {
        // On vas effectuer 2 requêtes au lieu 1 requête avec jointure.
        $poste = DB::select(
            "SELECT poste_occupe FROM postes WHERE id_user = $id_user"
        );

        $entreprise = DB::select(
            "SELECT nom_entr FROM entreprises WHERE id_entr IN 
        (SELECT id_entr FROM postes WHERE id_user = $id_user)"
        );

        $resultat = ['poste_occupe' => '', 'nom_entr' => ''];

        /*Un traitement particulier est appliqué au résultat de la requête dans le cas actuel.
        Pour éviter un bug si jamais la requête n'a pas de résultat*/

        if (count(array_keys($poste)) > 0) {
            $resultat['poste_occupe'] = $poste[0]->poste_occupe;
        }

        if (count(array_keys($entreprise)) > 0) {
            $resultat['nom_entr'] = $entreprise[0]->nom_entr;
        }

        return $resultat;
    }

    public function setUserPhoto($id_user, $path)
    {
        User::where('id', $id_user)
            ->update(['photo' => $path]);
    }

    public function setUserEntreprise($id_user, $entreprise, $statut)
    {
        $postes = new Poste;

        $entreprise = ucfirst($entreprise);

        $id_entr = $this->getIdEntreprise($entreprise);

        // On vérifie que l'utilisateur a déjà renseigné sa situation
        $nbrEntr = Poste::where('id_user', $id_user)->count();

        // Si l'utilisateur a déjà saisi le nom de son entreprise ou son poste on le met à jour. Si non on l'ajoute
        if ($nbrEntr > 0) {
            $postes::where('id_user', $id_user)
                ->update(['id_entr' => $id_entr, 'statut' => $statut]);
        } else {
            $postes->id_entr = $id_entr;
            $postes->statut = $statut;
            $postes->id_user = $id_user;

            $postes->save();
        }
    }

    public function setUserPoste($id_user, $poste_occupe, $statut)
    {
        $postes = new Poste;

        $poste_occupe = ucfirst($poste_occupe);

        // On vérifie que l'utilisateur a déjà renseigné sa situation
        $nbrPoste = Poste::where('id_user', $id_user)->count();

        // Si l'utilisateur a déjà renseigné sa situation on la met à jour. Si non on l'ajoute
        if ($nbrPoste > 0) {
            $postes::where('id_user', $id_user)
                ->update(
                    ['poste_occupe' => $poste_occupe, 'statut' => $statut]
                );
        } else {
            $postes->poste_occupe = $poste_occupe;
            $postes->statut = $statut;
            $postes->id_user = $id_user;

            $postes->save();
        }
    }

    public function setMotsDePasse($id_user, $password)
    {
        User::where('id', $id_user)
            ->update(['password' => $password]);
    }

    public function setEmail($id_user, $email)
    {
        User::where('id', $id_user)
            ->update(['email' => $email]);
    }
}
