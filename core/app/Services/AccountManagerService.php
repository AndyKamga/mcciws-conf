<?php

namespace App\Services;

use App\Models\Chat;
use App\Models\DatesImportante;
use App\Models\Post;
use Illuminate\Support\Facades\DB;

class AccountManagerService
{
    private $postService;
    private $chatService;

    public function __construct(PostService $postService, ChatService $chatService)
    {
        $this->postService = $postService;
        $this->chatService = $chatService;
    }

    public function updateAccount()
    {
        // Fonction qui change les étudiants en anciens à partir d'une certaine date

        $dateactuelle = date('Y-m-d');
        $anneeActuelle = date('Y');

        $dateImportante = DatesImportante::where('cle', 'changement_compte')->first();
        $dateMAJ = $dateImportante->date;

        if ($dateactuelle >= $dateMAJ) {
            // Tout ceux qui ne sont pas administrateurs ou développeurs sont transformés en anciens

            DB::update("UPDATE users SET droits=1 WHERE droits NOT IN (0, 1, 4, 5) AND annee < $anneeActuelle");

            // On avance l'année d'un ans au cas ou l'administrateur oubli de modifier la date pour l'année suivante
            $dateMAJ = date('Y-m-d', strtotime('+1 year'));

            // On met à jour la date de changement des comptes
            DB::update("UPDATE dates_importantes set date='$dateMAJ' where cle = 'changement_compte'");

            // On supprimer toutes les publications à l'exception de celles épinglées
            $posts = Post::where('is_pinned', false)->get();
            foreach ($posts as $post) {
                $this->postService->delete($post->id);
            }

            // On supprimer toutes les conversations
            foreach (Chat::all() as $chat) {
                $this->chatService->delete($chat->id);
            }
        }
    }
}
