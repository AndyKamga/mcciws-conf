<?php

namespace App\Services;

use App\Models\Droit;
use App\Models\Inscription;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class RegistrationService
{
    public function update()
    {
        $id = request('id');
        if (isset($_REQUEST['rejeter_inscript'])) {
            Inscription::where('id_inscrip', $id)->delete();

            return back()->withMessage("L'inscription de l'utilisateur a été rejetée !");
        }

        $inscription = Inscription::where('id_inscrip', $id)->first();

        $utilisateur = new User;

        $utilisateur->nom = $inscription->nom;
        $utilisateur->prenom = $inscription->prenom;
        $utilisateur->annee = date($inscription->annee);
        $utilisateur->email = $inscription->email;

        $utilisateur->password = $inscription->password;
        $utilisateur->photo = 'avatars/default.png';

        $typecompte = request('typecompte');
        $droit = Droit::where('description', $typecompte)->first();
        $droit = $droit->id_droit;
        $utilisateur->droits = $droit;

        //$utilisateur->save();

        $this->notificationMail($utilisateur->email);

        dd($utilisateur->email);

        Inscription::where('id_inscrip', $id)->delete();

        return back()->withMessage("L'inscription de l'utilisateur a été validée !");
    }

    private function notificationMail($email)
    {
        $sujet = 'Compte activé [Master CCI Tours]';
        $msg = "Bonjour,\n\nVotre compte a été activé. Vous pouvez désormais accèder à votre espace privé.
        \n\nCordialement,\nLe responsable de la formation";
        $repl = 'no-reply@mastercci.fr';

        Mail::raw($msg, function ($message) use ($email, $repl, $sujet) {
            $message->from('no-reply@mastercci.fr', 'Master CCI Tours');
            $message->to($email)->subject($sujet);

            // check for failures
            return Mail::failures();
        });
    }
}
