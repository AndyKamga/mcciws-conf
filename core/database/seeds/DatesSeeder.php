<?php

use Illuminate\Database\Seeder;

class DatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*----------------------------------
                Dates importantes
        -----------------------------------*/
        //DB::table('dates_importantes')->delete();

        DB::table('dates_importantes')->insert(['cle' => 'debut_candidature', 'date' => date('Y-m-d')]);
        DB::table('dates_importantes')->insert(['cle' => 'prerentree', 'date' => date('Y-m-d'), ]);
        DB::table('dates_importantes')->insert(['cle' => 'debut_cours', 'date' => date('Y-m-d'), ]);
        DB::table('dates_importantes')->insert(['cle' => 'fin_candidature', 'date' => date('Y-m-d'), ]);
        DB::table('dates_importantes')->insert(['cle' => 'depot_rapports', 'date' => date('Y-m-d'), ]);

        $new_date = date('Y-m-d', strtotime('1 year', strtotime(date('Y-m-d'))));
        DB::table('dates_importantes')->insert(['cle' => 'changement_compte', 'date' => $new_date, ]);
    }
}
