<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            DroitsSeeder::class,
            DatesSeeder::class,
            UsersSeederStd::class,
            PostCategorySeeder::class
        ]);
    }
}
