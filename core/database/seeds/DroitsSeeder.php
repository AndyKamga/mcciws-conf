<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DroitsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*----------------------------------
            Création des droits
        -----------------------------------*/
        //DB::table('droits')->delete();

        DB::table('droits')->insert(
            [
                'id_droit' => 1,
                'description' => 'Ancien',
                'permission' => json_encode([
                    'mon-profil', 'mon-rapport', 'mon-temoignage',
                    'voir-publications', 'commenter-publication', 'chatter',
                    'rechercher-utilisateur'
                ]),
            ]
        );
        DB::table('droits')->insert(
            [
                'id_droit' => 2,
                'description' => 'Etudiant',
                'permission' => json_encode([
                    'mon-profil','voir-offres-de-stage',
                    'mon-rapport','rapports-stage-anciens',
                    'voir-publications', 'commenter-publication',
                    'rechercher-utilisateur', 'chatter',
                ]),
            ]
        );
        DB::table('droits')->insert(
            [
                'id_droit' => 3,
                'description' => 'Semi administrateur',
                'permission' => json_encode([
                    'gestion-des-inscriptions','mon-profil',
                    'gestion-offres-de-stage', 'voir-offres-de-stage',
                    'mon-rapport', 'gestion-des-temoignages',
                    'rapports-stage-anciens', 'gestion-situation-anciens',
                    'gestion-des-projets', 'voir-publications',
                    'commenter-publication', 'epingler-publication',
                    'liste-signalisation','chatter','rechercher-utilisateur',
                ]),
            ]
        );
        DB::table('droits')->insert(
            [
                'id_droit' => 4,
                'description' => 'Administrateur',
                'permission' => json_encode([
                    'login', 'logout', 'register',
                    'gestion-des-utilisateurs',
                    'gestion-des-droits',
                    'gestion-des-inscriptions',
                    'mon-profil',
                    'gestion-des-dates-importantes',
                    'gestion-offres-de-stage',
                    'gestion-des-temoignages',
                    'gestion-rapports-stages',
                    'gestion-situation-anciens',
                    'gestion-des-projets', 'chatter',
                    'messagerie', 'voir-publications',
                    'commenter-publication', 'epingler-publication',
                    'liste-signalisation', 'rechercher-utilisateur'
                ]),
            ]
        );
        DB::table('droits')->insert(
            [
                'id_droit' => 5,
                'description' => 'Développeur',
                'permission' => json_encode([
                    'login',
                    'logout',
                    'register',
                    'gestion-des-utilisateurs',
                    'gestion-des-droits',
                    'gestion-des-inscriptions',
                    'mon-profil',
                    'gestion-des-dates-importantes',
                    'gestion-offres-de-stage',
                    'voir-offres-de-stage',
                    'mon-rapport',
                    'mon-temoignage',
                    'gestion-des-temoignages',
                    'gestion-rapports-stages',
                    'rapports-stage-anciens',
                    'gestion-situation-anciens',
                    'gestion-des-projets',
                    'messagerie','chatter',
                    'mise-a-jour-site', 'voir-publications',
                    'commenter-publication', 'epingler-publication',
                    'liste-signalisation', 'rechercher-utilisateur'
                ]),
            ]
        );
        DB::table('droits')->insert(
            [
                'id_droit' => 6,
                'description' => 'Gestion des anciens',
                'permission' => json_encode([
                    'mon-profil',
                    'voir-offres-de-stage',
                    'mon-rapport',
                    'rapports-stage-anciens',
                    'gestion-situation-anciens',
                    'liste-publication',
                    'voir-publication',
                    'poster-publication',
                    'epingler-publication',
                    'supprimer-publication', 'voir-publications',
                    'commenter-publication', 'chatter',
                    'rechercher-utilisateur'
                ]),
            ]
        );
        DB::table('droits')->insert(
            [
                'id_droit' => 7,
                'description' => 'Gestion des stages',
                'permission' => json_encode([
                    'mon-profil',
                    'gestion-offres-de-stage',
                    'voir-offres-de-stage',
                    'mon-rapport',
                    'rapports-stage-anciens',
                    'liste-publication',
                    'voir-publication',
                    'poster-publication',
                    'epingler-publication',
                    'supprimer-publication','voir-publications',
                    'commenter-publication', 'chatter',
                    'rechercher-utilisateur'
                ]),
            ]
        );
        DB::table('droits')->insert(
            [
                'id_droit' => 8,
                'description' => 'Gestion des projets',
                'permission' => json_encode([
                    'mon-profil',
                    'voir-offres-de-stage',
                    'mon-rapport',
                    'rapports-stage-anciens',
                    'gestion-des-projets',
                    'liste-publication',
                    'voir-publication',
                    'poster-publication',
                    'epingler-publication',
                    'supprimer-publication','voir-publications',
                    'commenter-publication', 'chatter',
                    'rechercher-utilisateur'
                ]),
            ]
        );

        DB::table('droits')->insert(
            [
                'id_droit' => 9,
                'description' => 'Administrateur du forum',
                'permission' => json_encode([
                    'mon-profil','voir-offres-de-stage',
                    'mon-rapport','rapports-stage-anciens',
                    'voir-publications', 'commenter-publication',
                    'voir-publications','commenter-publication',
                    'epingler-publication','liste-signalisation',
                    'chatter','rechercher-utilisateur'
                ]),
            ]
        );
    }
}
