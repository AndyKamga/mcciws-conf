<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersSeederStd extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Création du compte développeur

        DB::table('users')->insert(
            [
                'id' => 1,
                'nom' => 'Root',
                'prenom' => 'Root',
                'annee' => 1,
                'email' => 'root@mastercci.fr',
                'photo' => 'avatars/default.png',
                'password' => bcrypt('masterccidev'),
                'droits' => 5,
            ]
        );
    }
}
