<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ["Examen", "Conférences", "Événement", "Conseil", "Stage", "Offre d'emploi",
            "Structure de données", "HTML/CSS/SCSS", "Webpack", "Algorithmique", "Java", "Java EE", "Javascript/Typescript", "JQuery",
            "Angular", "React JS", "Vue JS", "C#", "Python", "Django", "Base de données", "SQL", "MySQL/MariaDB", "PostgreSQL", "MongoDB", "Oracle", "Redis",
            "Apache", "Ngnix", "Réseaux", "Génie logiciel", "Firebase", "PhpMyAdmin",
            "Gestion de projet", "Internet / Intranet", "Linux", "Applications mobiles", "Flutter", "Ionic", "Android",
            "Swift", "React native", "Développement Web", "PHP", "Laravel", "Symphony", "Node JS", "Spring / Springboot",
            "Git", "Github", "Gitlab", "CI/CD", "Docker", "kubernetes", "Amazon web service", "Heroku", "Hébergement web", "Jenkins",
            "Bonne pratiques", "Qualité de code", "Machine learning", "Deep learning", "Windows", "Mac OS", "Machine virtuelle",
            "Installation de logiciel", "Bug", "Idée", "UI/UX"
        ];
        $descriptions = ["Sujet, date, mise à jour d'examen", "Destinée aux conférences", "Relative aux événements",
            "Conseil d'étudiants ou d'anciens", "Stage", "Proposition d'emploi", "Structure de données", "HTML/CSS/SCSS", "Webpack",
            "Algorithmique", "Java", "Java EE", "Javascript/Typescript", "JQuery", "Angular", "React JS", "Vue JS", "C#", "Python", "Django",
            "Base de données", "SQL", "MySQL/MariaDB", "PostgreSQL", "MongoDB", "Oracle", "Redis", "Apache",  "Ngnix", "Réseaux", "Génie logiciel",
            "Firebase", "PhpMyAdmin", "Gestion de projet", "Internet / Intranet", "Linux",
            "Applications mobiles", "Flutter", "Ionic", "Android", "Swift", "React native", "Développement Web", "PHP", "Laravel", "Symphony",
            "Node JS", "Spring / Springboot", "Git", "Github", "Gitlab", "Intégration et déploiement continue",
            "Docker", "kubernetes", "Amazon web service", "Heroku", "Hébergement web", "Jenkins", "Bonne pratiques de programmation", "Code quality",
            "Machine learning", "Deep learning", "Windows", "Mac OS", "Machine virtuelle", "Installation de logiciel", "Bug", "Idée", "UX/UI Design"
        ];
        $nbr = sizeof($categories);

        for ($i = 0; $i < $nbr; $i++) {
            DB::table('post_categories')->insert(
                [
                    'id' => $i + 1,
                    'name' =>  $categories[$i],
                    'description' => $descriptions[$i]
                ]
            );
        }
    }
}
