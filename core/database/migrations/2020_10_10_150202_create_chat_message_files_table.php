<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatMessageFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_message_files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 1000);
            $table->text('path');
            $table->string('media_type', 30);
            $table->unsignedBigInteger('chat_message_id');

            $table->foreign('chat_message_id')
                ->references('id') ->on('chat_messages')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_message_files');
    }
}
