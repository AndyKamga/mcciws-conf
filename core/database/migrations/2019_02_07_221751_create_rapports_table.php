<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRapportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rapports', function (Blueprint $table) {
            $table->integer('id_rapport', true);
            $table->text('rapport_de_stage', 65535)->nullable();
            $table->text('note_synthese', 65535);
            $table->integer('id_user')->unsigned()->index('fk_user_rapport');
            $table->integer('id_stage')->index('fk_stage_rapport');
            $table->boolean('confidentialite')->default(0);
            $table->dateTime('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rapports');
    }
}
