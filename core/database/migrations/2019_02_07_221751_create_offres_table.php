<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offres', function (Blueprint $table) {
            $table->integer('id_offre', true);
            $table->string('titre', 50);
            $table->string('entreprise', 30);
            $table->string('localisation', 50)->nullable();
            $table->string('fichier', 500)->nullable();
            $table->date('debut')->nullable();
            $table->date('fin')->nullable();
            $table->string('contact', 50)->nullable();
            $table->string('mail', 50)->nullable();
            $table->integer('telephone')->nullable();
            $table->text('description', 65535)->nullable();
            $table->boolean('statut')->nullable()->default(0);
            $table->integer('id_user')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('offres');
    }
}
