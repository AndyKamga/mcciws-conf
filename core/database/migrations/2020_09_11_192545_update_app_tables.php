<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAppTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('droits')
                ->references('id_droit')->on('droits');
        });


        Schema::table('offres', function (Blueprint $table) {
            $table->unsignedInteger('id_user')->nullable()->change();
            $table->foreign('id_user')
                ->references('id')->on('users');
        });

        Schema::table('postes', function (Blueprint $table) {
            $table->foreign('id_entr')
                ->references('id_entr')->on('entreprises');

            $table->unsignedInteger('id_user')->nullable()->change();
            $table->foreign('id_user')
                ->references('id') ->on('users');
        });

        Schema::table('rapports', function (Blueprint $table) {
            $table->unsignedInteger('id_user')->nullable()->change();
            $table->foreign('id_user')
                ->references('id') ->on('users');

            $table->foreign('id_stage')
                ->references('id_stage')->on('stages');
        });

        Schema::table('stages', function (Blueprint $table) {
            $table->foreign('id_entr')
                ->references('id_entr')->on('entreprises');
        });

        Schema::table('temoignages', function (Blueprint $table) {
            $table->unsignedInteger('id_user')->nullable()->change();
            $table->foreign('id_user')
                ->references('id') ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
