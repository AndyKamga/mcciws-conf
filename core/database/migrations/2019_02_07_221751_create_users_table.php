<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom', 50);
            $table->string('prenom', 50);
            $table->integer('annee');
            $table->string('email', 50)->unique();
            $table->dateTime('email_verified_at')->nullable();
            $table->string('password', 200);
            $table->string('remember_token', 100)->nullable();
            $table->text('photo')->nullable();
            $table->smallInteger('droits')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
