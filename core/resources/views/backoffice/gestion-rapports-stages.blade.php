@extends('layouts.back')

<?php $page_en_cours = 'gestion-rapports-stages';?>

@section('content')
        <!---Statistiques --->
        <div class="row">
            <div class="col-lg">
                <div id="stat_zone_rouge" class="card card-body text-center">
                    <h3><B>{{$notif1}}</B></h3>
                    <p>Rapport(s) en attente(s)</p>
                </div>
            </div>
            <div class="col-lg">
                <div id="stat_zone_bleu" class="card card-body text-center">
                    <h3><B>{{$notif2}}</B></h3>
                    <p>Etudiant(s)</p>
                </div>
            </div>
            <div class="col-lg">
                <div class="card card-body text-center">
                    <h3><B>{{$notif3}}</B></h3>
                        <p>Rapport(s) déposé(s)</p>
                </div>
            </div>
    
        </div>

        <!--Rapport de stage des étudiants --->
        <div class="card card-body">
            <div class="table-responsive">
                <table id="dataTable" class="table table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th class="th-sm">Nom</th>
                            <th class="th-sm">Prénom</th>
                            <th class="th-sm">Année</th>
                            <th class="th-sm">Entreprise</th>
                            <th class="th-sm">Sujet de stage</th>
                            <th class="th-sm">Date de remise</th>
                            <th class="th-sm text-center">Rapport</th>
                            <th class="th-sm text-center">Note de synthèse</th>
                            <th class="th-sm text-center">Option</th>
                            <th style="display:none" class="th-sm">id_entreprise</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php // On sélectionne les étudiants qui ont déposés leur rapport de stage?>
                        @foreach($results1 as $result)
                            <tr id="{{$result->id_stage}}">
                                <td data-target="nom">{{$result->nom}}</td>
                                <td data-target="prenom">{{$result->prenom}}</td>
                                <td data-target="annee">{{$result->annee}}</td>
                                <td data-target="nom_entr">{{$result->nom_entr}}</td>
                                <td data-target="sujet_stage">{{$result->sujet_stage}}</td>
                                <td>{{$result->date}}</td>
                                <td align="center">
                                    <a href="storage/{{$result->rapport_de_stage}}" class="btn-sm btn-success" role="button" download style="text-decoration:none">Télécharger</a>
                                </td>
                                <td align="center">
                                    <a href="storage/{{$result->note_synthese}}" class="btn-sm btn-primary" role="button" download style="text-decoration:none">Télécharger</a>
                                </td>
                                @if($result->statut==0)
                                    <td align="center"><a href="#" data-role="activeRapport" data-id="{{$result->id_stage}}" class="btn-sm btn-primary" 
                                        role="button" style="text-decoration:none">Activer</a></td>
                                @else
                                    <td align="center"><a href="#" data-role="desactiveRapport" data-id="{{$result->id_stage}}" class="btn-sm btn-dark" 
                                        role="button" style="text-decoration:none">Desactiver</a></td>
                                @endif

                                <td style="display:none" data-target="id_entreprise">{{$result->id_entr}}</td>
                            </tr>
                        @endforeach
                        
                        <?php // On sélectionne les étudiants qui n'ont pas déposés leur rapport de stage?>
                        @foreach($results2 as $result)
                            <tr>
                                <td data-target="nom">{{$result->nom}}</td>
                                <td data-target="prenom">{{$result->prenom}}</td>
                                <td data-target="annee">{{$result->annee}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td align="center">
                                    <a style="color:red">Non déposé</a>
                                </td>
                                <td align="center">
                                        <a style="color:red">Non déposé</a>
                                </td>
                                <td align="center">
                                    <a disabled class="btn-sm btn-default" role="button" style="text-decoration:none">Activer</a>
                                </td>
                                <td style="display:none"></td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>

        </div>
@endsection

@section('modals')
    <!-- Modal activer les informations de dépôt de stage-->
    <div id="rapportActive" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="memberModalLabel"><B>Activer les informations</B></h5>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">

                    <p>Les informations suivantes ne sont pas visibles sur la page entreprise. Cliquer sur le bouton
                        activer pour les rendre visibles.</p>

                    <form method="POST" action="{{url('rendre-visible-stage')}}" role="form" autocomplete="off">
                        @csrf
                        <input type="hidden" class="form-control" id="id_stage" name="id_stage" />
                        <input type="hidden" class="form-control" id="id_entreprise" name="id_entreprise" />

                        <div class="form-group">
                            <label>Nom de l'entreprise</label>
                            <input type="text" class="form-control" required pattern=".*\S+.*" id="nom_entreprise" name="nom_entreprise"
                                maxlength="49"/>
                        </div>
                        <div class="form-group">
                            <label>Sujet du stage</label>
                            <input type="text" class="form-control" required pattern=".*\S+.*" id="sujet_stage" name="sujet_stage"
                                maxlength="99"/>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                            <input type="submit" class="btn btn-success" name="active_rapport" value="Activer" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal désactiver les informations de dépôt de stage-->
    <div id="rapportDesactive" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="memberModalLabel"><B>Desactiver les informations</B></h5>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">

                    <p>Les informations suivantes sont visibles sur la page entreprise. Cliquer sur le bouton
                        désactiver pour les rendre invisibles.</p>

                    <form method="POST" action="{{url('rendre-invisible-stage')}}" role="form" autocomplete="off">
                        @csrf
                        <input type="hidden" class="form-control" id="id_stage_desac" name="id_stage_desac" />

                        <div class="form-group">
                            <label>Nom de l'entreprise</label>
                            <input type="text" class="form-control" id="nom_entreprise2" name="nom_entreprise" disabled />
                        </div>
                        <div class="form-group">
                            <label>Sujet du stage</label>
                            <input type="text" class="form-control" id="sujet_stage2" name="sujet_stage" disabled />
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                            <input type="submit" class="btn btn-danger" name="desactive_rapport" value="Désactiver" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // Chargement du tableau dynamique
        $(document).ready(function () {
            $('#dataTable').DataTable();
        });

        $(document).ready(function () {

            $(document).on('click', 'a[data-role=activeRapport]', function () {
                var id = $(this).data('id');
                var id_entreprise = $('#' + id).children('td[data-target=id_entreprise]').text();
                var sujet_stage = $('#' + id).children('td[data-target=sujet_stage]').text();
                var nom_entreprise = $('#' + id).children('td[data-target=nom_entr]').text();


                // On socke les informations (variables) récupérées dans le modal=> $('#id dans le modal').val(nom de la variable);

                $('#rapportActive').modal('toggle');
                $('#id_stage').val(id);
                $('#id_entreprise').val(id_entreprise);
                $('#sujet_stage').val(sujet_stage);
                $('#nom_entreprise').val(nom_entreprise);

            });

            $(document).on('click', 'a[data-role=desactiveRapport]', function () {
                var id = $(this).data('id');
                var sujet_stage = $('#' + id).children('td[data-target=sujet_stage]').text();
                var nom_entreprise = $('#' + id).children('td[data-target=nom_entr]').text();


                // On socke les informations (variables) récupérées dans le modal=> $('#id dans le modal').val(nom de la variable);

                $('#rapportDesactive').modal('toggle');
                $('#id_stage_desac').val(id);
                $('#sujet_stage2').val(sujet_stage);
                $('#nom_entreprise2').val(nom_entreprise);

            });


        });

    </script>
@endsection