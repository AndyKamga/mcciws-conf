@extends('layouts.back')

<?php $page_en_cours = 'historique-des-mails';?>

@section('content')
    <div class="row">
        <div class="col-lg-3">
            <a id="nouveau-message" class="btn btn-primary btn-sm" href="messagerie" role="button">Nouveau message</a>
            <br/><br>
            <div class="card">
                <div id="options-mail" class="card-body">
                    <h5 class="option-titre">Options</h5>
                    <hr>
                    <a href="{{url('historique-des-mails')}}">Historique des mails <span
                                class="badge badgeRouge pull-right">{{$nbrHistorique}}</span></a>
                </div>
            </div>

        </div>
        <div class="col-lg-9">
            <div class="card card-body back_mess">
                <table id="dataTable" class="table table-striped" style="width:100%">
                    <thead>
                    <tr>
                        <th class="th-sm">Destinataire</th>
                        <th class="th-sm">Sujet</th>
                        <th style="display:none" class="th-sm">Message</th>
                        <th style="display:none" class="th-sm">Expediteur</th>
                        <th class="th-sm">Date</th>
                        <th class="th-sm text-center">Option</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($results1 as $result)
                        <tr id="{{$result->id_mail}}">
                            <td data-target="destinataires">{{$result->destinataires}}</td>
                            <td data-target="sujet">{{$result->sujet}}</td>
                            <td style="display:none" data-target="message">{{$result->message}}</td>
                            <td style="display:none" data-target="expediteur">{{$result->correspondant}}</td>
                            <td data-target="date">{{$result->created_at}}</td>
                            <td align="center"><a href="#" data-role="voir-mail" data-id="{{$result->id_mail}}">
                                    <i class="far fa-edit"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    <!-- Modal message -->
    <div id="supprimer-message" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="memberModalLabel"><B>Message</B></h6>
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{url('supprimer-message')}}" role="form" autocomplete="off">
                        @csrf

                        <input type="hidden" class="form-control" id="id_mail" name="id_mail" value=""/>

                        <div class="form-group">
                            <label>Destinataire(s)</label>
                            <textarea rows="2" disabled id="destinataires" name="destinataires"
                                      class="form-control"></textarea>
                        </div>

                        <div class="form-group">
                            <label>Expéditeur</label>
                            <input disabled id="expediteur" name="expediteur" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label>Sujet</label>
                            <textarea rows="1" disabled id="sujet" name="sujet" class="form-control"></textarea>
                        </div>

                        <div class="form-group">
                            <label>Message</label>
                            <textarea rows="5" disabled id="message" name="message" class="form-control"></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="button d-flex" class="btn btn-default" data-dismiss="modal">Annuler</button>
                            <input class="btn btn-danger" type="submit" value="Supprimer" name="SuprimerMessage"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // Chargement du tableau dynamique
        $(document).ready(function () {
            $('#dataTable').DataTable();
        });

        $(document).ready(function () {
            $(document).on('click', 'a[data-role=voir-mail]', function () {
                var id = $(this).data('id');
                var destinataires = $('#' + id).children('td[data-target=destinataires]').text();
                var expediteur = $('#' + id).children('td[data-target=expediteur]').text();
                var sujet = $('#' + id).children('td[data-target=sujet]').text();
                var message = $('#' + id).children('td[data-target=message]').text();
                var date = $('#' + id).children('td[data-target=date]').text();

                // On socke les informations (variables) récupérées dans le modal=> $('#id dans le modal').val(nom de la variable);

                $('#supprimer-message').modal('toggle');
                $('#id_mail').val(id);
                $('#message').val(message);
                $('#sujet').val(sujet);
                $('#expediteur').val(expediteur);
                $('#destinataires').val(destinataires);


            });


        });
    </script>
@endsection
