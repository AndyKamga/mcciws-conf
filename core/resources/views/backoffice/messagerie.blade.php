@extends('layouts.back')

<?php $page_en_cours = 'messagerie';

set_time_limit(0);
?>

@section('content')
    <div class="row">
        <div class="col-lg-3">
            <a id="nouveau-message" class="btn btn-primary btn-sm" href="messagerie" role="button">Nouveau message</a>
            <br/><br>
            <div class="card">
                <div id="options-mail" class="card-body">
                    <h5 class="option-titre">Options</h5>
                    <hr>
                    <a href="{{url('historique-des-mails')}}">Historique des mails <span
                                class="badge badgeRouge pull-right">{{$nbrHistorique}}</span></a>
                </div>
            </div>
        </div>
        <div class="col-lg-9">
            <div class="card card-body back_mess">
                <form action="{{url('envoyer-mail')}}" method="post" enctype="multipart/form-data"
                      name="Messagere_form">
                    @csrf
                    <label class="sr-only" for="dest">Destinataires</label>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <label class="input-group-text" for="dest">@</label>
                        </div>
                        <select name="To" id="dest" class="form-control">
                            @foreach($comptes as $result)
                                <option value="{{$result->id_droit}}">{{$result->description}}</option>');
                            @endforeach
                            <option value="-1">Tous</option>
                        </select>
                    </div>

                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <label class="input-group-text">Adresse de retour : </label>
                        </div>
                        <input type="email" name="repl" value="{{auth()->user()->email}}" class="form-control" readonly>
                    </div>

                    <label class="sr-only" for="subj">Sujet</label>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <label class="input-group-text" for="subj">Sujet :</label>
                        </div>
                        <input type="text" class="form-control" id="subj" name="subj" placeholder="Aucun sujet"
                               required>
                    </div>

                    <textarea rows="10" name="msg" id="msg" class="form-control corps_message " required></textarea>
                    <div class="piece-jointe">
                        <input type="file" class="form-control-file" name="piece-jointe[]" id="piece-jointe" multiple>
                        <p>Max. 20MB</p>
                        <p id="error_message_attached" style="color:red; font-size:10pt;"></p>
                    </div>
                    <hr>
                    <div class="col-auto text-right">
                        <button type="submit" value="Envoyer" name="send_grouped_mail" class="btn btn-primary mt-2"
                                onclick="return validateForm()">Envoyer
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    <div class="modal fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Envoi des mails en cours ...</h4>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function validateForm() {
            var x = document.forms["Messagere_form"]["subj"].value;
            if (x.trim() == "") {
                return true;
            }
            var y = document.forms["Messagere_form"]["msg"].value;
            if (y.trim() == "") {
                return true;
            }
            $('#myModal').modal('toggle');

            return true;
        }

        $('#piece-jointe').bind('change', function () {

            // Vérification de la taille du fichier
            var sizeInKB = 0;
            var sizeLimit = 20001; // 20MB
            for (var i = 0; i < this.files.length; i++) {
                sizeInKB = sizeInKB + this.files[i].size / 1024;
            }

            if (sizeInKB >= sizeLimit) {
                document.getElementById("error_message_attached").innerHTML = "La taille de la pièce jointe doit être inférieure ou égale à 20MB !";
                $('#piece-jointe').val('');
                return false;
            } else {
                document.getElementById("error_message_attached").innerHTML = "";
            }
        });
    </script>
@endsection
