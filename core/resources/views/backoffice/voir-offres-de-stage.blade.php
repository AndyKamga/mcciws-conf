@extends('layouts.back')

<?php $page_en_cours = 'voir-offres-de-stage';?>

@section('content')
        <!---Tableau --->
        <div class="card card-body">
            <div class="table-responsive">
                <table id="dataTable" class="table table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th class="th-sm">Entreprise</th>
                            <th class="th-sm">Localisation</th>
                            <th class="th-sm">Début</th>
                            <th class="th-sm">Fin</th>
                            <th class="th-sm">Personne à contacter</th>
                            <th class="th-sm">Email</th>
                            <th class="th-sm">Téléphone</th>

                            <!-- Entête de colonnes cachées contenant la description des témoignages-->
                            <th style="display:none" class="th-sm">TitreStage</th>
                            <th style="display:none" class="th-sm">Description</th>
                            <th style="display:none" class="th-sm">Fichier</th>

                            <!-- options -->
                            <th  class="th-sm text-center">Options</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($results1 as $result)
    
                            <tr id="{{$result->id_offre}}">
                                <td data-target="NomEntreprise">{{$result->entreprise}}</td>
                                <td data-target="Localisation">{{$result->localisation}}</td>
                                <td data-target="DateDebut">{{$result->debut}}</td>
                                <td data-target="DateFin">{{$result->fin}}</td>
                                <td data-target="Contact">{{$result->contact}}</td>
                                <td data-target="Mail">{{$result->mail}}</td>
                                <td data-target="Telephone">{{$result->telephone}}</td>


                                <?php // Colonnes cachées contenant la description du stage?>
                                <td style="display:none" data-target="TitreStage"><b>{{$result->titre}}</b></td>
                                <td style="display:none" data-target="description"><b>{{$result->description}}</b></td>
                                <td style="display:none" data-target="fichier"><b>{{$result->fichier}}</b></td>
                                
                          
                                <td align="center">
                                    <a href="#" data-role="voiroffre" data-id="{{$result->id_offre}}" class="btn-sm btn-info" role="button">Voir</a> 
                                    <a href="#" data-role="desactiver" data-id="{{$result->id_offre}}" class="btn-sm btn-danger" role="button">Désactiver</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
@endsection

@section('modals')
    <!-- Modal visualiser une offre de stage-->
    <div id="offreView" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="memberModalLabel"><B>Offre de stage</B></h5>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="form/traitement_formulaires.php" role="form" autocomplete="off">
                        @csrf
                        <input type="hidden" class="form-control" id="id_offre" name="id" value="" />

                        <div class="form-group">
                            <label for="comment">Titre du stage</label>
                            <textarea class="form-control" disabled="disabled" rows="2" id="titre" maxlength="49"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="comment">Description</label>
                            <textarea class="form-control" disabled="disabled" rows="5" id="description" maxlength="999"></textarea>
                        </div>

                        <div class="form-group">
                            <label>Pièce jointe</label>
                            <input type="hidden" id="link" name="link" />
                            <a id="piecej" name="piecej" target="_blank" class="btn btn-outline-primary form-control" href="" role="button" download>Télécharger la pièce jointe</a>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal désactiver une offre de stage-->
    <div id="deactivateOffre" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="memberModalLabel"><B>Voulez-vous vraiment désactiver l'offre ?</B></h6>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="confirmer-offre" role="form" autocomplete="off">
                        @csrf
                        <div class="form-group">
                            <input type="hidden" class="form-control" id="id_deactivate" name="id_offre" value="" />
                            <input type="hidden" class="form-control" id="id_user" name="id_user" value="{{Auth::user()->id}}" />
                        </div>

                        <p>En cliquant sur le bouton "confirmer", vous déclarez avoir été accepté en stage par l'entreprise
                            de manière officielle.</p>
                        <p style="color:red"><b>NB : L'offre de stage ne sera plus accessible à vos collègues, ainsi qu'à vous !</b></p><br/>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                            <input class="btn btn-danger" type="submit" value="Confirmer" name="confirmer_offre" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // Chargement du tableau dynamique
        $(document).ready(function () {
            $('#dataTable').DataTable();
        });

        $(document).ready(function () {
            // Lorsqu'on click sur le bouton mettre à jour, on récupère les informations de la ligne
            // et on les stocke dans des variables
            $(document).on('click', 'a[data-role=voiroffre]', function(){
                var id = $(this).data('id');
                var titre =  $('#' + id).children('td[data-target=TitreStage]').text();
                var description =  $('#' + id).children('td[data-target=description]').text();
                var fichier =  $('#' + id).children('td[data-target=fichier]').text();

                // On socke les informations (variables) récupérées dans le modal=> $('#id dans le modal').val(nom de la variable);
 
                $('#offreView').modal('toggle');
                $('#id_offre').val(id);
                $('#titre').val(titre);
                $('#description').val(description);
                $('#link').val(fichier);
                $('#piecej').attr("href", 'storage/'.concat(fichier));
            });

            $(document).on('click', 'a[data-role=desactiver]', function(){
                var id = $(this).data('id');
                $('#deactivateOffre').modal('toggle');
                $('#id_deactivate').val(id);
                
            });

        });
    </script>
@endsection