@extends('layouts.back')

<?php $page_en_cours = 'gestion-situation-anciens';?>

@section('content')
    <!---Statistiques --->
    <div class="row">
        <div class="col-lg">
            <div id="stat_zone_bleu" class="card card-body text-center">
                <h3><B>{{$notif1}}</B></h3>
                <p>Situation(s) validée(s)</p>
            </div>
        </div>
        <div class="col-lg">
            <div id="stat_zone_rouge" class="card card-body text-center">
                <h3><B>{{$notif2}}</B></h3>
                <p>Situation(s) non validée(s)</p>
            </div>
        </div>
    </div>

    <!---Tableau --->
    <div class="card card-body">
        <div class="table-responsive">
            <table id="dataTable" class="table table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th class="th-sm">Nom</th>
                    <th class="th-sm">Prénom</th>
                    <th class="th-sm">Email</th>
                    <th class="th-sm">Année</th>
                    <th class="th-sm">Poste occupé</th>
                    <th class="th-sm">Entreprise</th>
                    <th class="th-sm">Statut</th>

                    <!-- options -->
                    <th class="th-sm text-center">Option</th>
                </tr>
                </thead>
                <tbody>
                @foreach($results1 as $result)

                    <?php $statut = 'Validée'; if ($result->statut == 0) {
    $statut = 'Non validée';
} ?>

                    <tr id="{{$result->id}}">
                        <td data-target="nom">{{$result->nom}}</td>
                        <td data-target="prenom">{{$result->prenom}}</td>
                        <td data-target="email">{{$result->email}}</td>
                        <td data-target="annee">{{$result->annee}}</td>
                        <td data-target="poste_occupe"><b>{{$result->poste_occupe}}</b></td>
                        <td data-target="nom_entr"><b>{{$result->nom_entr}}</b></td>
                        <td data-target="statut"><b>{{$statut}}</b></td>


                        <td align="center">
                            @if($result->statut == 0)
                                <a style="text-decoration:none" href="#" data-role="editer" data-id="{{$result->id}}"
                                   class="btn-sm btn-info" role="button">Editer</a>
                            @else
                                <a style="text-decoration:none" href="#" data-role="desactiver"
                                   data-id="{{$result->id}}" class="btn-sm btn-dark" role="button">Désactiver</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('modals')
    <!-- Modal valider une situation-->
    <div id="situationView" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="memberModalLabel"><B>Situation de l'ancien</B></h5>
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{url('valider-situation')}}" role="form" autocomplete="off">
                        @csrf
                        <input type="hidden" class="form-control" id="id_user" name="id_user" value=""/>
                        <div class="form-group">
                            <label>Nom</label>
                            <input type="text" class="form-control" disabled="disabled" id="nom" name="nom"
                                   maxlength="49"/>
                        </div>
                        <div class="form-group">
                            <label>prénom</label>
                            <input type="text" class="form-control" disabled="disabled" id="prenom" name="prenom"
                                   maxlength="49"/>
                        </div>
                        <div class="form-group">
                            <label>Poste occupé</label>
                            <input maxlength="99" type="text" class="form-control" required pattern=".*\S+.*"
                                   id="poste_occupe" name="poste_occupe"/>
                        </div>
                        <div class="form-group">

                            <label>Entreprise</label>
                            <input maxlength="49" type="text" class="form-control" required pattern=".*\S+.*"
                                   id="entreprise" name="entreprise"/>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                            <input class="btn btn-success" type="submit" value="Valider" name="valide_situation"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal désactiver une situation-->
    <div id="desactiverSituation" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="memberModalLabel"><B>Désactiver une situation</B></h6>
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{url('desactiver-situation')}}" role="form" autocomplete="off">
                        @csrf
                        <div class="form-group">
                            <input type="hidden" class="form-control" id="id_user_desact" name="id_user_desact"
                                   value=""/>
                        </div>

                        <h6>Voulez vraiment désactiver la situation ?</h6>
                        <p>La situation sera concervée dans la base de donnée, mais ne sera plus consultable
                            par les visiteurs du site.
                        </p><br/>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                            <input class="btn btn-danger" type="submit" value="Désactiver" name="desactiver_situation"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // Chargement du tableau dynamique
        $(document).ready(function () {
            $('#dataTable').DataTable();
        });

        $(document).ready(function () {
            // Lorsqu'on click sur le bouton mettre à jour, on récupère les informations de la ligne
            // et on les stocke dans des variables
            $(document).on('click', 'a[data-role=editer]', function () {
                var id = $(this).data('id');
                var nom = $('#' + id).children('td[data-target=nom]').text();
                var prenom = $('#' + id).children('td[data-target=prenom]').text();
                var poste_occupe = $('#' + id).children('td[data-target=poste_occupe]').text();
                var nom_entr = $('#' + id).children('td[data-target=nom_entr]').text();


                $('#situationView').modal('toggle');
                $('#id_user').val(id);
                $('#nom').val(nom);
                $('#prenom').val(prenom);

                $('#poste_occupe').val(poste_occupe);
                $('#entreprise').val(nom_entr);

            });

            $(document).on('click', 'a[data-role=desactiver]', function () {
                var id = $(this).data('id');
                $('#desactiverSituation').modal('toggle');
                $('#id_user_desact').val(id);

            });


        });
    </script>
@endsection
