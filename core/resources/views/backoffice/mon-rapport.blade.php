@extends('layouts.back')

<?php $page_en_cours = 'mon-rapport';
$droits = auth()->user()->droits;?>

@section('content')

    <div class="card">
        <!-- Formulaire de dépôt de rapport de stage-->
        <form method="POST" action="{{url('poster-rapport')}}" role="form" enctype="multipart/form-data">
            @csrf

            <div class="card-body">
                <p>
                    Déposer votre rapport de stage ainsi que votre note de synthèse ici.
                    N'oubliez pas de renseigner le nom de l'entreprise d'accueil et le sujet de votre stage.
                </p>
                <br>
                <h6>Statut de remise</h6>
                <br>
                <!-- Statut du dépôt-->
                <table class="table table-striped">
                    <tr>
                        <td width="40%">Statut du rapport</td>
                        <?php if ($rapportExist
                            > 0
                        ) {
    echo('<td class="vert">Remis pour évaluation </td>');
} else {
    echo('<td class="rouge">Non déposé </td>');
}?>
                    </tr>
                    <tr>
                        <td>Date limite de remise</td>
                        <td>
                            <?php echo($dateLimite); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Temps restant</td>
                        <td>
                            <?php echo($tempsRestant); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Date de remise</td>
                        <td>
                            <?php if ($rapportExist > 0) {
    echo($dateDepo);
} else {
    echo('-');
}?>
                        </td>
                    </tr>
                    @if($rapportExist > 0)
                        <tr>
                            <td>Rapport de stage</td>
                            <td>
                                <a id="download_button" href="storage/{{$rapportStage}}" target="_blank"
                                   class="btn btn-primary btn-sm" role="button" download>Télécharger</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Note de synthèse</td>
                            <td>
                                <a id="download_button" href="storage/{{$noteSynthese}}" target="_blank"
                                   class="btn btn-primary btn-sm" role="button" download>Télécharger</a>
                            </td>
                        </tr>
                    @endif
                </table>
                <hr>
                <div class="row">
                    <div class="col-lg">
                        <h6>Informations sur le stage</h6>
                        <br>
                        <input type="hidden" class="form-control" id="id_user" name="id"
                               value="{{auth()->user()->id}}"/>
                        <input type="hidden" class="form-control" id="rapportExist" name="rapportExist"
                               value="<?php echo($rapportExist) ?>"/>

                        <div class="form-group">
                            <label>Nom de l'entreprise</label>
                            <input type="text" <?php if ($tempsRestant == 'Temps écoulé'
                                || $droits == 1
                            ) {
    echo('disabled');
}?> class="form-control" id="nomEntrep" name="nomEntrep"
                                   value="{{$entrStag['nom_entr']}}"
                                   required pattern=".*\S+.*" maxlength="49"/>
                            @if($errors->has('nomEntrep'))
                                <p class="erreurs">Vous devez saisir le nom de l'entreprise qui vous a accueillie.</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Sujet de stage</label>
                            <input type="text" <?php if ($tempsRestant == 'Temps écoulé'
                                || $droits == 1
                            ) {
    echo('disabled');
}?> class="form-control" id="sujetstage" name="sujetstage"
                                   value="{{$entrStag['sujet_stage']}}"
                                   required pattern=".*\S+.*" maxlength="254"/>
                            @if($errors->has('sujetstage'))
                                <p class="erreurs">Vous devez saisir votre sujet de stage.</p>
                            @endif
                        </div>
                        <div class="form-check">
                            <input <?php if ($tempsRestant == 'Temps écoulé'
                                || $droits == 1
                            ) {
    echo('disabled');
}?> type="checkbox" class="form-check-input"
                                   name="confidentialite" <?php if ($confidentialite > 0) {
    echo('checked');
} ?>>
                            <label class="form-check-label">Cochez si votre rapport est confidentiel !</label>
                        </div>
                    </div>
                    <div class="col-lg">
                        <h6>Rapport de stage et note de synthèse</h6>
                        <br>
                        <div class="form-group">
                            <p>Importer votre rapport de stage (PDF)</p>
                            <input id="rapportDeStage" type="file" <?php if ($tempsRestant == 'Temps écoulé'
                                || $droits == 1
                            ) {
    echo('disabled');
}?> class="form-control-file" name="rapportDeStage" accept=".pdf"
                            <?php if ($rapportExist == 0) {
    echo('required');
} ?>><br/>
                            <p id="error_message_rapport" style="color:red; font-size:10pt;"></p>

                            @if($errors->has('rapportDeStage'))
                                <p class="erreurs">Votre rapport de stage doit être au format PDF !</p>
                            @endif
                        </div>
                        <hr>
                        <div class="form-group">
                            <p>Importer votre note de synthèse (PDF)</p>
                            <input id="noteSynthese" type="file" <?php if ($tempsRestant == 'Temps écoulé'
                                || $droits == 1
                            ) {
    echo('disabled');
}?> class="form-control-file" name="noteSynthese" accept=".pdf"
                            <?php if ($rapportExist == 0) {
    echo('required');
} ?>><br/>
                            <p id="error_message_note" style="color:red; font-size:10pt;"></p>

                            @if($errors->has('noteSynthese'))
                                <p class="erreurs">Votre note de synthèse doit être au format PDF !</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-center">
                <div class="form-group">
                    <input <?php if ($tempsRestant == 'Temps écoulé'
                        || $droits == 1
                    ) {
    echo('disabled');
}?>  id="boutonRapport"
                           class="btn <?php if ($rapportExist > 0) {
    echo(' btn-primary');
} else {
    echo(' btn-success');
}?>"
                           type="submit" value=" <?php if ($rapportExist
                        > 0
                    ) {
    echo('Mettre à jour mon rapport et ma note');
} else {
    echo('Déposer mon rapport et ma note');
}?>"
                           name="<?php if ($rapportExist > 0) {
    echo('MAJRapport');
} else {
    echo('envoyerRapport');
}?>"/>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <script>
        $('#rapportDeStage').bind('change', function () {

            // Vérification de l'extension
            var ext = $('#rapportDeStage').val().split('.').pop().toLowerCase();
            if ($.inArray(ext, ['pdf']) == -1) {
                document.getElementById("error_message_rapport").innerHTML = "Le format du fichier est incorrect !";
                $('#rapportDeStage').val('');
            } else {
                document.getElementById("error_message_rapport").innerHTML = "";
            }

            // Vérification de la taille du fichier
            var sizeInKB = this.files[0].size / 1024;
            var sizeLimit = 5000; // 5MB

            if (sizeInKB >= sizeLimit) {
                document.getElementById("error_message_rapport").innerHTML = "La taille du fichier doit être inférieure 5MB !";
                $('#rapportDeStage').val('');
                return false;
            } else {
                document.getElementById("error_message_rapport").innerHTML = "";
            }
        });
    </script>

    <script>
        $('#noteSynthese').bind('change', function () {

            // Vérification de l'extension
            var ext = $('#noteSynthese').val().split('.').pop().toLowerCase();
            if ($.inArray(ext, ['pdf']) == -1) {
                document.getElementById("error_message_note").innerHTML = "Le format du fichier est incorrect !";
                $('#noteSynthese').val('');
            } else {
                document.getElementById("error_message_note").innerHTML = "";
            }

            // Vérification de la taille du fichier
            var sizeInKB = this.files[0].size / 1024;
            var sizeLimit = 5000; // 5MB

            if (sizeInKB >= sizeLimit) {
                document.getElementById("error_message_note").innerHTML = "La taille du fichier doit être inférieure 5MB !";
                $('#noteSynthese').val('');
                return false;
            } else {
                document.getElementById("error_message_note").innerHTML = "";
            }
        });
    </script>
@endsection
