@extends('layouts.back')

<?php $page_en_cours = 'gestion-des-droits';?>


@section('action')
    <div class="ajoutUtilisateur">
        <button type="button" class="btn-sm btn-primary" onclick="addModal()">Ajouter un droits
        </button>
    </div>
@endsection

@section('content')
    <!---Tableau --->
    <div class="card card-body">
        <div class="alert alert-warning" role="alert">
            <strong>Attention !</strong>
            Veuillez à ne jamais modifier les ids des droits allant de 1 à 5. Il s'agit de droits par défaut, nécéssaires
            au bon fonctionnement du site.
        </div>
        <div class="table-responsive">
            <table id="dataTable" class="table table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th class="th-sm">Id</th>
                    <th class="th-sm">Description</th>
                    <th class="th-sm text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($listeDroits as $result)
                    <tr id="{{$result->id_droit}}">
                        <td>{{$result->id_droit}}</td>
                        <td>{{$result->description}}</td>
                        <td align="center"><a href="#" role="button"
                                              onclick="editModal({{$result}})"><i class="far fa-edit"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('modals')
    <div id="crudModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" style="width: 1200px">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title"><B>Créer un droit</B></h5>
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">
                    <form id="crudForm" method="POST" action="tes" role="form" autocomplete="off">
                        @csrf

                        <div class="form-group">
                            <input id="idData" type=hidden required pattern=".*\S+.*"
                                   name="idData"/>
                        </div>

                        <div class="form-group">
                            <label>Description</label>
                            <input id="description" type="text" class="form-control" required pattern=".*\S+.*"
                                   name="description" maxlength="49"/>
                        </div>

                        <div class="form-group">
                            <label>Permissions (Routes)</label>
                            <select id="routeList" class="form-control routeList" name="routes[]" multiple="multiple"
                                    style="width:100%" required>
                                @foreach($routeList as $result)
                                    <option value="{{$result}}">{{$result}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Selectionner toutes les routes</label>
                            <input type="checkbox" id="checkbox" >
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                            <input id="submit" class="btn btn-primary" type="submit" value="Ajouter"/>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // Chargement du tableau dynamique
        $(document).ready(function () {

            // Activation du dataTable
            $('#dataTable').DataTable();

            // Activation du select 2
            $('.routeList').select2({
                theme: "bootstrap4",
                placeholder : "Placeholder",
                tags: true
            });

            // Selectionner toutes les routes
            $("#checkbox").click(function(){
                if($("#checkbox").is(':checked') ){
                    $(".routeList > option").prop("selected","selected");
                    $(".routeList").trigger("change");
                }else{
                    $(".routeList > option").prop("selected", "");
                    $(".routeList").trigger("change");
                }
            });
        });


    </script>

    <script type="text/javascript">

        function editModal(data){

            let myModal = $('#crudModal');
            $(".routeList > option").prop("selected", "");
            $(".routeList").trigger("change");

            $('#crudForm').attr('action', 'modifier-un-droit');

            $('#modal-title').text('Modifier un droit');
            $('#submit').val('Modifier');

            $('#idData').val(data.id_droit);
            $('#description').val(data.description);


            let permissions = JSON.parse(data.permission);
            $.each(permissions, function(i,e){
                $(".routeList option[value='" + e + "']").prop("selected", true);
                $(".routeList").trigger("change");
            });

            myModal.modal('show');

        }

        function addModal(){
            $('#idData').val("");
            let myModal = $('#crudModal');

            $('#crudForm').attr('action', 'ajouter-un-droit');

            $(".routeList > option").prop("selected", "");
            $(".routeList").trigger("change");

            $('#modal-title').text('Ajouter un droit');
            $('#submit').val('Ajouter');
            $('#description').val('');
            myModal.modal('show');
        }


    </script>


@endsection
