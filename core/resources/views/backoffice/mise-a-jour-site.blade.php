@extends('layouts.back')

<?php $page_en_cours = 'mise-a-jour-site';?>

@section('content')
    <div class="card">
        <div class="card-header">
            Commandes automatiques
        </div>
        <div class="card-body">
            <form method="POST" action="{{url('commande-auto')}}" role="form" autocomplete="off">
                @csrf

                <div class="form-group">
                    <label>Sélectionner une action</label>
                    <select class="form-control" name="action" required>
                        <option value="1">Mettre à jour le site</option>
                        <option value="2" disabled>Mettre le site en maintenance</option>
                        <option value="3">Activer la gestion de fichiers</option>
                    </select>
                </div>
                <input style="float: right" class="btn btn-success" type="submit" value="Executer"/>
            </form>
            @if ($message = Session::get('results-update'))
                <div class="console" style="background-color: black; margin-top:20px;">
                    <div style="padding: 20px;">
                        <h5>Résultats</h5>
                        <p style="color: #008000;">{{$message}}</p>
                    </div>
                </div>
            @endif
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            Commandes Artisan
        </div>
        <div class="card-body">
            @if ($message = Session::get('results-artisan'))
            <div class="alert alert-success" role="alert">
                {{$message}}
            </div>
            @endif
            <form method="POST" action="{{url('commande-artisan')}}" role="form" autocomplete="off">
                @csrf
                <div class="form-group">
                    <label for="commande">Commande :</label>
                    <input type="text" class="form-control" id="commande" name="commande"
                           placeholder="ex : 'storage:link' ou 'migrate'" required>
                </div>
                <input style="float: right" class="btn btn-secondary" type="submit" value="Executer"/>
            </form>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            Commandes dans laravel
        </div>
        <div class="card-body">
            <p>Les commandes saisies sont executées dans le répertoire "laravel_app"</p>
            <form method="POST" action="{{url('commande-laravel')}}" role="form" autocomplete="off">
                @csrf
                <div class="form-group">
                    <label for="commande">Commande :</label>
                    <input type="text" class="form-control" id="commande" name="commande"
                           placeholder="Veuillez saisir une commande (ex : 'composer update')" required>
                </div>
                <input style="float: right" class="btn btn-primary" type="submit" value="Executer"/>
            </form>
            <br>
            @if ($message = Session::get('results-laravel'))
                <div class="console" style="background-color: black;">
                    <div style="padding: 20px;">
                        <h5>Résultats</h5>
                        <p style="color: #008000;">{{$message}}</p>
                    </div>
                </div>
            @endif
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            Commandes libres
        </div>
        <div class="card-body">
            <div class="alert alert-danger" role="alert">
                Veuillez faire attention aux commandes saisies !
            </div>
            <form method="POST" action="{{url('commande-site')}}" role="form" autocomplete="off">
                @csrf
                <div class="form-group">
                    <label for="commande">Commande :</label>
                    <input type="text" class="form-control" id="commande" name="commande"
                           placeholder="Veuillez saisir une commande (ex : 'cd laravel_app ; composer update')" required>
                </div>
                <input style="float: right" class="btn btn-danger" type="submit" value="Executer"/>
            </form>
            <br>
            @if ($message = Session::get('results-commande'))
                <div class="console" style="background-color: black;">
                    <div style="padding: 20px;">
                        <h5>Résultats</h5>
                        <p style="color: #008000;">{{$message}}</p>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection

@section('modals')

@endsection

@section('scripts')

@endsection
