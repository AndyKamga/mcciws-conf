@extends('layouts.back')

<?php $page_en_cours = 'mon-temoignage' ?>

@section('content')
        <div class="card card-body">
                
            @if($results1==1)
                <div class="alert alert-success">Votre témoignage a été validé !</div>
            @endif
            @if($results1==0)
                <div class="alert alert-info">Votre témoignage est en attente de validation...</div>
            @endif


            <!-- Contenu de la page-->
            <form method="POST" action="{{url('poster-temoignage')}}" role="form">
                @csrf
                <div class="form-group">
                    <label>Poste occupé</label>
                    <input maxlength="99" type="text" class="form-control" required="true" id="metier" name="metier" value="{{$results2['metier']}}" <?php if ($statut > 0) {
    echo('disabled');
}?>/>
                </div>

                <div class="form-group">
                    <label for="comment">Résumé du témoignage</label>
                    <textarea class="form-control" required="true" maxlength="210" rows="5" id="resume" name="resume" <?php if ($statut > 0) {
    echo('disabled');
}?>>{{$results2['resume']}}</textarea>
                </div>

                <div class="form-group">
                    <label for="comment">Témoignage complet</label>
                    <textarea maxlength="4999" class="form-control" required="true" rows="5" id="description" name="description" <?php if ($statut > 0) {
    echo('disabled');
}?>>{{$results2['description']}}</textarea>
                </div>

                <div class="modal-footer">
                    <input class="btn btn-primary" type="submit"
                    value="<?php if ($existUser) {
    echo('Mettre à jour mon témoignage');
} else {
    echo('Envoyer mon témoignage');
} ?>" name="envoyer" <?php if ($statut > 0) {
    echo('disabled');
}?>/>
                </div>
            </form>

        </div>
@endsection
