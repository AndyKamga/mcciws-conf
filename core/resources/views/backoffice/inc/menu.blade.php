<?php

/*---------------------------------------------------------------
  On récupère le droit de l'utilisateur connecté et on vérifie sa permission.
  En fonction de sa permission, on affiche l'onglet.
  --------------------------------------------------------------*/

use App\Models\Droit;

$id = auth()->user()->droits;
$permission = Droit::where('id_droit', $id)->first()->permission;
$permissionArray = json_decode($permission, true);

//--------------------------------------------------------------------------------------------
?>

<li <?php if ($page_en_cours == 'mon-espace') {
    echo('class="active"');
}?>>
    <a href="{{url('home')}}"><i class="fas fa-home"></i>Accueil</a>
</li>


@if(in_array('gestion-des-utilisateurs', $permissionArray))
    <li <?php if ($page_en_cours == 'gestion-des-utilisateurs') {
    echo('class="active"');
}?>>
        <a href="{{url('gestion-des-utilisateurs')}}"><i class="fas fa-users"></i>Utilisateurs</a>
    </li>
@endif

@if(in_array('gestion-des-droits', $permissionArray))
    <li <?php if ($page_en_cours == 'gestion-des-droits') {
    echo('class="active"');
}?>>
        <a href="{{url('gestion-des-droits')}}"><i class="fas fa-shield-alt"></i>Droits</a>
    </li>
@endif

@if(in_array('gestion-des-inscriptions', $permissionArray))
    <li <?php if ($page_en_cours == 'gestion-des-inscriptions') {
    echo('class="active"');
}?>>
        <a href="{{url('gestion-des-inscriptions')}}"><i class="fas fa-user-plus"></i>
            Inscriptions <span class="badge badgeVertB pull-right">{{$notifInscriptions}}</span></a>
    </li>
@endif

@if(in_array('gestion-des-dates-importantes', $permissionArray))
    <li <?php if ($page_en_cours == 'gestion-des-dates-importantes') {
    echo('class="active"');
}?>>
        <a href="{{url('gestion-des-dates-importantes')}}"><i class="fas fa-calendar-alt"></i>Dates importantes</a>
    </li>
@endif

@if(in_array('messagerie', $permissionArray))
    <li <?php if ($page_en_cours == 'messagerie') {
    echo('class="active"');
}?>>
        <a href="{{url('messagerie')}}"><i class="fas fa-envelope"></i>Messagerie</a>
    </li>
@endif

@if(in_array('gestion-des-projets', $permissionArray))
    <li <?php if ($page_en_cours == 'gestion-des-projets') {
    echo('class="active"');
}?>>
        <a href="{{url('gestion-des-projets')}}"><i class="fas fa-tasks"></i>Projets des étudiants</a>
    </li>
@endif

@if(in_array('gestion-situation-anciens', $permissionArray))
    <li <?php if ($page_en_cours == 'gestion-situation-anciens') {
    echo('class="active"');
}?>>
        <a href="{{url('gestion-situation-anciens')}}"><i class="fas fa-graduation-cap"></i>
            Situations des anciens<span class="badge badgeRose pull-right">{{$notifsituation}}</span></a>
    </li>
@endif

@if(in_array('gestion-des-temoignages', $permissionArray))
    <li <?php if ($page_en_cours == 'gestion-des-temoignages') {
    echo('class="active"');
}?>>
        <a href="{{url('gestion-des-temoignages')}}"><i class="fas fa-comments"></i>
            Témoignages <span class="badge badgeVert pull-right">{{$notiftemoignages}}</span></a>
    </li>
@endif

@if(in_array('gestion-offres-de-stage', $permissionArray))
    <li <?php if ($page_en_cours == 'gestion-offres-de-stage') {
    echo('class="active"');
}?>>
        <a href="{{url('gestion-offres-de-stage')}}"><i class="far fa-building"></i>
            Gestion des offres de stage <span
                    class="badge badgeJaune pull-right">{{$notifOffresNonValid}}</span></a>
    </li>
@endif

@if(in_array('gestion-rapports-stages', $permissionArray))
    <li <?php if ($page_en_cours == 'gestion-rapports-stages') {
    echo('class="active"');
}?>>
        <a href="{{url('gestion-rapports-stages')}}"><i class="fas fa-book"></i>
            Rapports de stage <span class="badge pull-right">{{$notifRapportStage}}</span></a>
    </li>
@endif

@if(in_array('rapports-stage-anciens', $permissionArray))
    <li <?php if ($page_en_cours == 'rapports-stage-anciens') {
    echo('class="active"');
}?>>
        <a href="{{url('rapports-stage-anciens')}}"><i class="fas fa-book"></i>Rapports de stage déposés</a>
    </li>
@endif

@if(in_array('mon-rapport', $permissionArray))
    <li <?php if ($page_en_cours == 'mon-rapport') {
    echo('class="active"');
}?>>
        <a href="{{url('mon-rapport')}}"><i class="fas fa-file-pdf"></i>Mon rapport</a>
    </li>
@endif

@if(in_array('voir-offres-de-stage', $permissionArray))
    <li <?php if ($page_en_cours == 'voir-offres-de-stage') {
    echo('class="active"');
}?>>
        <a href="{{url('voir-offres-de-stage')}}"><i class="fas fa-building"></i>
            Voir les offres de stage <span class="badge badgeRouge pull-right">{{$notifOffresValid}}</span></a>
    </li>
@endif

@if(in_array('mon-temoignage', $permissionArray))
    <li <?php if ($page_en_cours == 'mon-temoignage') {
    echo('class="active"');
}?>>
        <a href="{{url('mon-temoignage')}}"><i class="far fa-comments"></i>Mon témoignage</a>
    </li>
@endif

@if(in_array('mon-profil', $permissionArray))
    <li <?php if ($page_en_cours == 'mon-profil') {
    echo('class="active"');
}?>>
        <a href="{{url('mon-profil')}}"><i class="fas fa-user"></i>Mon profil</a>
    </li>
@endif

@if(in_array('mise-a-jour-site', $permissionArray))
    <li <?php if ($page_en_cours == 'mise-a-jour-site') {
    echo('class="active"');
}?>>
        <a href="{{url('mise-a-jour-site')}}"><i class="fab fa-git-square"></i>Mise à jour du site</a>
    </li>
@endif
<br><br>