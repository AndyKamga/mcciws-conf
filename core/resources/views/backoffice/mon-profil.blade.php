@extends('layouts.back')

<?php $page_en_cours = 'mon-profil';?>

@section('content')
    <div class="card card-body">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs">
        <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" href="#infos_perso">Informations générales</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#mdp">Changer mon mot de passe</a>
        </li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">

        <div id="infos_perso" class="container tab-pane active"><br>
          <form method="POST" action="{{url('mettre-a-jour-profil')}}" autocomplete="off" role="form" enctype="multipart/form-data">
            @csrf
            <h6>Informations personnelles</h6>
            <hr />
            <!--Formulaire avec les champs préremplis-->
            <input type="hidden" class="form-control" id="id_user" name="id_user" readonly value="{{Auth::user()->id}}" />
            <input type="hidden" class="form-control" id="photo" name="photo" readonly value="{{Auth::user()->photo}}" />
            <div class="form-group">
              <div class="row">
                <div class="col-lg form-group">
                  <label for="nom_user">Nom</label>
                  <input maxlength="49" name="nom_user" id="nom_user" class="form-control" readonly value="{{Auth::user()->nom}}">
                </div>
                <div class="col-lg form-group">
                  <label for="nom_user">Prénom</label>
                  <input maxlength="49" name="prenom_user" id="prenom_user" class="form-control" readonly value="{{Auth::user()->prenom}}">
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="email_user">Email</label>
              <input type="email" maxlength="49" name="email_user" class="form-control" value="{{Auth::user()->email}}" required>
            </div>
            <br />

            <h6>Ma situation professionnelle</h6>
            <hr />
            <!--Formulaire avec les champs préremplis-->
            <div class="form-group">
              <div class="row">
                <div class="col-lg form-group">
                  <label>Poste occupé</label>
                  <?php $droits = Auth::user()->droits;?>
                  <input name="poste_occupe" id="poste_occupe" class="form-control" <?php if ($droits != 1 && $droits != 5) {
    echo('readonly');
} ?> 
                  value="{{$results['poste_occupe']}}" maxlength="99">
                </div>
                <div class="col-lg form-group">
                  <label>Entreprise</label>
                  <input name="nom_entr" id="nom_entr" class="form-control" <?php if ($droits != 1 && $droits != 5) {
    echo('readonly');
} ?> value="{{$results['nom_entr']}}" maxlength="49">
                </div>
              </div>
            </div>

            <br />

            <div class="form-group">
              <h6>Changer ma photo de profil</h6>
              <hr />
              <input id="fichierImage" type="file" class="form-control-file" name="avatar" accept="image/*">
              <p id="error_message_image" style="color:red; font-size:10pt;"></p>
              @if($errors->has('avatar'))
                <p class="erreurs">Le fichier choisi n'est pas une image valide.</p><br />
              @endif
            </div>
            <div id="bouton" class="form-group">
              <input class="btn btn-primary" type="submit" id="modif" name="modif_profil" value="Modifier mes informations">
            </div>

          </form>
        </div>

        <div id="mdp" class="container tab-pane fade"><br>
            <form method="POST" action="{{url('mettre-a-jour-mdp')}}" autocomplete="off" role="form" enctype="multipart/form-data">
              @csrf
              <input type="hidden" class="form-control" id="id_user" name="id_user" readonly value="{{Auth::user()->id}}" />
              <div class="form-group">
                <label for="mdp_user">Ancien mot de passe</label>
                <input type="password" name="password_actuel" class="form-control" required />
   
                <p class="erreurs">{!! Session::get('erreur') !!}</p>
  
              </div>
              <div class="form-group">
                <label for="mdp1">Nouveau mot de passe (6 caractères minimum)</label>
                <input type="password" class="form-control" name="password" required/>
                @if($errors->has('password'))
                  <p class="erreurs">Les mots de passe sont incorrects.</p>
                @endif
              </div>
              <div class="form-group">
                <label for="mdp2">Confirmer un nouveau mot de passe</label>
                <input type="password" class="form-control" name="password_confirmation" required/>
              </div>

              <div id="bouton" class="form-group">
                <input class="btn btn-primary" type="submit" id="modif_mdp" name="modif_mdp" value="Sauvegarder">
              </div>

            </form>
        </div>
      </div>
    </div>
@endsection

@section('scripts')
  <script>
      $('#fichierImage').bind('change', function() {
              
          // Vérification de l'extension
          var ext = $('#fichierImage').val().split('.').pop().toLowerCase();
          if($.inArray(ext, ['png', 'jpg']) == -1) {
              document.getElementById("error_message_image").innerHTML = "Le fichier doit être une image !";
              $('#fichierImage').val('');
          }else{document.getElementById("error_message_image").innerHTML = "";}

      
          // Vérification de la taille du fichier
          var sizeInKB = this.files[0].size/1024; 
          var sizeLimit= 1000; // 1MB
      
          if (sizeInKB >= sizeLimit) {
              document.getElementById("error_message_image").innerHTML = "La taille de l'image doit être inférieure 1MB !";
              $('#fichierImage').val('');
              return false;
          }else{document.getElementById("error_message_image").innerHTML = "";}
      });
  </script>
@endsection