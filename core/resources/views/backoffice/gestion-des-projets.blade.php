@extends('layouts.back')

<?php $page_en_cours = 'gestion-des-projets';?>

@section('action')
    <div class="ajoutProjet">
        <button type="button" id="add" name="add" class="btn-sm btn-primary" data-toggle="modal"
                data-target="#ajoutProjet">Ajouter un projet
        </button>
    </div>
@endsection

@section('content')
    <div class="card card-body">
        <div class="table-responsive">
            <table id="dataTable" class="table table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th class="th-sm">Titre</th>
                    <th style="display:none" class="th-sm">Description</th>
                    <th class="th-sm">Langages et compétences</th>
                    <th class="th-sm">Date</th>

                    <!-- options -->
                    <th class="th-sm text-center">Options</th>
                </tr>
                </thead>
                <tbody>
                @foreach($results1 as $result)
                    <tr id="{{$result->id_projet}}">
                        <td data-target="Titre">{{$result->titre}}</td>
                        <td style="display:none" data-target="Description">{{$result->description}}</td>
                        <td data-target="Langage_competence">{{$result->langage_competence}}</td>
                        <td data-target="date">{{$result->date}}</td>

                        <td align="center">
                            <a style="text-decoration: none" href="#" data-role="editer"
                               data-id="{{$result->id_projet}}" class="btn-sm btn-info" role="button">Editer</a>
                            <a style="text-decoration: none" href="#" data-role="supprimer"
                               data-id="{{$result->id_projet}}" class="btn-sm btn-danger" role="button">Supprimer</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('modals')
    <!-- Modal ajouter un projet-->
    <div id="ajoutProjet" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="memberModalLabel"><B>Ajouter un projet</B></h5>
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{url('ajout-projet')}}" role="form" autocomplete="off">
                        @csrf
                        <div class="form-group">
                            <label>Titre</label>
                            <input type="text" class="form-control" required pattern=".*\S+.*" id="Titre_add"
                                   name="titre_projet" maxlength="49"/>
                        </div>
                        <div class="form-group">
                            <label>Année</label>
                            <select class="form-control" id="date" name="date">
                                <?php $i = 0;  $annee_actuelle = date('Y'); ?>
                                @for($i=$annee_actuelle; $i>1995; $i--)
                                    <option>{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Langages et compétences</label>
                            <input type="text" class="form-control" required pattern=".*\S+.*"
                                   id="Langage_competence_add" name="langage_competence" maxlength="99"/>
                        </div>

                        <div class="form-group">
                            <label>Description</label>
                            <textarea type="text" rows="6" class="form-control" required pattern=".*\S+.*"
                                      id="Description_add" name="description_projet" maxlength="599"></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                            <input class="btn btn-primary" type="submit" value="Ajouter" name="addProjet"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal modifier un projet-->
    <div id="editer" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="memberModalLabel"><B>Description du projet</B></h5>
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{url('modifier-projet')}}" role="form" autocomplete="off">
                        @csrf
                        <div class="form-group">
                            <input type="hidden" class="form-control" id="id_update" name="id_update" value=""/>
                        </div>
                        <div class="form-group">
                            <label>Titre</label>
                            <input type="text" class="form-control" required pattern=".*\S+.*" id="Titre_update"
                                   name="Titre_update" maxlength="49"/>
                        </div>
                        <div class="form-group">
                            <label>Année</label>
                            <select class="form-control" id="date_update" name="date_update">
                                <?php $i = 0;  $annee_actuelle = date('Y'); ?>
                                @for($i=$annee_actuelle; $i>1995; $i--)
                                    <option>{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Langages et compétences</label>
                            <input type="text" class="form-control" required pattern=".*\S+.*"
                                   id="Langage_competence_update" name="Langage_competence_update" maxlength="99"/>
                        </div>

                        <div class="form-group">
                            <label>Description</label>
                            <textarea rows="6" class="form-control" required pattern=".*\S+.*" id="Description_update"
                                      name="Description_update" maxlength="599"></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                            <input class="btn btn-primary" type="submit" value="Mettre à jour" name="updateProjet"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal suprimer un projet -->
    <div id="supprimer" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="memberModalLabel"><B>Voulez-vous vraiment supprimer le projet ?</B></h6>
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{url('supprimer-projet')}}" role="form" autocomplete="off">
                        @csrf
                        <div class="form-group text-right">
                            <input type="hidden" class="form-control" id="id_projet" name="id_projet" value=""/>
                            <button type="button d-flex" class="btn btn-default" data-dismiss="modal">Annuler</button>
                            <input class="btn btn-danger" type="submit" value="Confirmer" name="SuprimerProjet"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // Chargement du tableau dynamique id_projet`, `Titre`, `Description`, `Langage_competence
        $(document).ready(function () {
            $('#dataTable').DataTable();
        });

        $(document).ready(function () {
            // Lorsqu'on click sur le bouton mettre à jour, on récupère les informations de la ligne
            // et on les stocke dans des variables
            $(document).on('click', 'a[data-role=editer]', function () {
                var id = $(this).data('id');
                var Titre = $('#' + id).children('td[data-target=Titre]').text();
                var Description = $('#' + id).children('td[data-target=Description]').text();
                var Langage_competence = $('#' + id).children('td[data-target=Langage_competence]').text();
                var date = $('#' + id).children('td[data-target=date]').text();


                // On socke les informations (variables) récupérées dans le modal=> $('#id dans le modal').val(nom de la variable);

                $('#editer').modal('toggle');
                $('#id_update').val(id);
                $('#Titre_update').val(Titre);
                $('#Description_update').val(Description);
                $('#Langage_competence_update').val(Langage_competence);
                $('#date_update').val(date);
            });

            $(document).on('click', 'a[data-role=supprimer]', function () {
                var id = $(this).data('id');
                $('#supprimer').modal('toggle');
                $('#id_projet').val(id);

            });


        });
    </script>
@endsection