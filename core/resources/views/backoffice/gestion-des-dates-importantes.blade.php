@extends('layouts.back')

<?php $page_en_cours = 'gestion-des-dates-importantes';

$dateChangementCompte = $results[0];
$dateOuverture = $results[1];
$dateRentree = $results[2];
$dateRapport = $results[3];
$dateLimite = $results[4];
$datePreRentree = $results[5];
?>
@section('content')
    <div class="card card-body">
        <form method="POST" action="{{url('mettre-a-jour-dates')}}">
            @csrf
            <h6>Candidatures</h6>
            <hr/>
            <div class="row">
                <div class="col-lg form-group">
                    <label>Ouverture des candidatures</label>
                    <input type="date" name="dateOuverture" value="<?php echo $dateOuverture ?>" class="form-control"
                           required/>
                </div>
                <div class="col-lg form-group">
                    <label>Limite de réception des dossiers</label>
                    <input type="date" name="dateLimite" value="<?php echo $dateLimite ?>" class="form-control"
                           required/>
                </div>
            </div>

            <h6>Scolarité</h6>
            <hr/>
            <div class="row">
                <div class="col-lg form-group">
                    <label>Réunion de pré-rentrée</label>
                    <input type="date" name="datePreRentree" value="<?php echo $datePreRentree; ?>" class="form-control"
                           required/>
                </div>
                <div class="col-lg form-group">
                    <label>Rentrée et début des cours</label>
                    <input type="date" name="dateRentree" value="<?php echo $dateRentree; ?>" class="form-control"
                           required/>
                </div>
                <div class="col-lg form-group">
                    <label>Date limite de dépôt des rapports de stage</label>
                    <input type="date" name="dateRapport" value="<?php echo $dateRapport; ?>" class="form-control"
                           required/>
                </div>
            </div>

            <br>
            <h6>Mise à jour des comptes</h6>
            <hr/>
            <div class="row">
                <div class="col-lg form-group">
                    A partir de cette date, les comptes étudiants et semi administrateurs deviendront des comptes
                    anciens. Les publications du forum n'ont épinglées, les conversations (chats) seront supprimées.
                    <br><br>
                    La prochaine date de mise à jour sera redéfinie de manière automatique en incrémentant la date
                    actuelle
                    d'un an.
                </div>
                <div class="col-lg form-group">
                    <label>Date de changement des comptes</label>
                    <input type="date" name="dateComptes" value="<?php echo $dateChangementCompte; ?>"
                           class="form-control" required/>
                </div>
            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" value="Envoyer" name="MAJDates"/>
                Appliquer les modifications</button>
            </div>
        </form>
    </div>
@endsection
