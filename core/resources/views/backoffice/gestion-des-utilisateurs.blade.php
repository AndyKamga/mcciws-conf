@extends('layouts.back')

<?php $page_en_cours = 'gestion-des-utilisateurs';?>

@section('action')
    <div class="ajoutUtilisateur">
        <button type="button" id="add" name="add" class="btn-sm btn-primary" data-toggle="modal"
                data-target="#dataAdd">Ajouter un utilisateur
        </button>
    </div>
@endsection

@section('content')

    <!---Statistiques --->
    <div class="row">
            <div class="col-lg">
                <div id="stat_zone_bleu" class="card card-body text-center">
                    <h3><B>{{$notif1}}</B></h3>
                    <p>Etudiant(s)</p>
                </div>
            </div>
            <div class="col-lg">
                <div id="stat_zone_rouge" class="card card-body text-center">
                    <h3><B>{{$notif2}}</B></h3>
                    <p>Ancien(s)</p>
                </div>
            </div>
            <div class="col-lg">
                <div id="stat_zone_vert" class="card card-body text-center">
                    <h3><B>{{$notif3}}</B></h3>
                    <p>Administrateur(s)</p>
                </div>
            </div>
            <div class="col-lg">
                <div id="stat_zone_grise" class="card card-body text-center">
                    <h3><B>{{$notif4}}</B></h3>
                    <p>Inactif(s)</p>
                </div>
            </div>
            <div class="col-lg">
                <div id="stat_zone_orange" class="card card-body text-center">
                    <h3><B>{{$notif5}}</B></h3>
                    <p>Total</p>
                </div>
            </div>
        </div>

    <!---Tableau --->
    <div class="card card-body">
        <div class="table-responsive">
            <table id="dataTable" class="table table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th class="th-sm">Nom</th>
                    <th class="th-sm">Prénom</th>
                    <th class="th-sm">Email</th>
                    <th class="th-sm">Année</th>
                    <th class="th-sm">Statut</th>
                    <th class="th-sm text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($results1 as $result)
                    <tr id="{{$result->id}}">
                        <td data-target="nom">{{$result->nom}}</td>
                        <td data-target="prenom">{{$result->prenom}}</td>
                        <td data-target="email">{{$result->email}}</td>
                        <td data-target="annee">{{$result->annee}}</td>
                        <td data-target="statut">{{$result->description}}</td>
                        <td align="center"><a href="#" data-role="mettreAJour" data-id="{{$result->id}}" role="button"
                                              style="text-decoration:none"><i class="far fa-edit"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('modals')
    <!-- Modal ajouter un utilisateur-->
    <div id="dataAdd" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="memberModalLabel"><B>Ajouter un utilisateur</B></h5>
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{url('ajouter-utilisateur')}}" role="form" autocomplete="off">
                        @csrf
                        <div class="form-group">
                            <label>Nom</label>
                            <input type="text" class="form-control" required pattern=".*\S+.*" id="nom_add" name="nom"
                                   maxlength="49"/>
                        </div>
                        <div class="form-group">
                            <label>Prénom</label>
                            <input type="text" class="form-control" required pattern=".*\S+.*" id="prenom_add"
                                   name="prenom" maxlength="49"/>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" required pattern=".*\S+.*" id="email_add"
                                   name="email" maxlength="49"/>
                        </div>
                        <div class="form-group">
                            <label>Année</label>
                            <select class="form-control" id="annee_add" name="annee">
                                <?php $i = 0;  $annee_actuelle = date('Y'); ?>
                                @for($i=$annee_actuelle; $i>1995; $i--)
                                    <option>{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Type de compte</label>
                            <select class="form-control" name="typecompte" required="true" id="compte_add">
                                @foreach($results2 as $result)
                                    <option>{{$result->description}}</option>');
                                @endforeach
                            </select>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                            <input class="btn btn-primary" type="submit" value="Ajouter" name="add_gesUser"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal modifier un utilisateur-->
    <div id="dataUpdate" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="memberModalLabel"><B>Modifier un utilisateur</B></h5>
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{url('modifier-utilisateur')}}" role="form" autocomplete="off">
                        @csrf
                        <div class="form-group">
                            <input type="hidden" class="form-control" id="id_update" name="id"/>
                        </div>
                        <div class="form-group">
                            <label>Nom</label>
                            <input type="text" class="form-control" required pattern=".*\S+.*" id="nom_update"
                                   name="nom" maxlength="49"/>
                        </div>
                        <div class="form-group">
                            <label>Prénom</label>
                            <input type="text" class="form-control" required pattern=".*\S+.*" id="prenom_update"
                                   name="prenom" maxlength="49"/>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" required pattern=".*\S+.*" id="email_update"
                                   name="email" maxlength="49"/>
                        </div>
                        <div class="form-group">
                            <label>Année</label>
                            <select class="form-control" id="annee_update" name="annee" required>
                                <?php $i = 0;  $annee_actuelle = date('Y'); ?>
                                @for($i=$annee_actuelle; $i>=1992; $i--)
                                    <option>{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Type de compte</label>
                            <select class="form-control" name="typecompte" required id="droit_update">
                                @foreach($results2 as $result)
                                    <option>{{$result->description}}</option>');
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Définir un nouveau mot de passe</label>
                            <input id="password_update" type="text" class="form-control" name="password" placeholder="facultatif"/>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                            <input class="btn btn-primary" type="submit" value="Mettre à jour" name="update_gesUser"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // Chargement du tableau dynamique
        $(document).ready(function () {
            $('#dataTable').DataTable();
        });

        $(document).ready(function () {
            // Lorsqu'on click sur le bouton mettre à jour, on récupère les informations de la ligne
            // et on les stocke dans des variables
            $(document).on('click', 'a[data-role=mettreAJour]', function () {
                let id = $(this).data('id');
                let nom = $('#' + id).children('td[data-target=nom]').text();
                let prenom = $('#' + id).children('td[data-target=prenom]').text();
                let email = $('#' + id).children('td[data-target=email]').text();
                let annee = $('#' + id).children('td[data-target=annee]').text();
                let statut = $('#' + id).children('td[data-target=statut]').text();


                // On socke les informations (variables) récupérées dans le modal=> $('#id dans le modal').val(nom de la variable);

                $('#dataUpdate').modal('toggle');
                $('#id_update').val(id);
                $('#nom_update').val(nom);
                $('#prenom_update').val(prenom);
                $('#email_update').val(email);
                $('#annee_update').val(annee);
                $('#droit_update').val(statut);
            });

        });
    </script>
@endsection

