@extends('layouts.back')

<?php $page_en_cours = 'gestion-des-temoignages';?>

@section('content')
    <!---Statistiques --->
    <div class="row">
        <div class="col-lg">
            <div id="stat_zone_vert" class="card card-body text-center">
                <h3><B>{{$notif1}}</B></h3>
                <p>Témoignage(s) validé(s)</p>
            </div>
        </div>
        <div class="col-lg">
            <div id="stat_zone_rouge" class="card card-body text-center">
                <h3><B>{{$notif2}}</B></h3>
                <p>Témoignage(s) non validé(s)</p>
            </div>
        </div>
    </div>

    <!---Tableau --->
    <div class="card card-body">
        <div class="table-responsive">
            <table id="dataTable" class="table table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th class="th-sm">Nom</th>
                    <th class="th-sm">Prénom</th>
                    <th class="th-sm">Email</th>
                    <th class="th-sm">Année</th>
                    <th class="th-sm">Statut</th>

                    <!-- Entête de colonnes cachées contenant la description des témoignages-->
                    <th style="display:none" class="th-sm">id_user</th>
                    <th style="display:none" class="th-sm">resume</th>
                    <th style="display:none" class="th-sm">metier</th>
                    <th style="display:none" class="th-sm">description</th>

                    <!-- options -->
                    <th class="th-sm text-center">Option</th>
                </tr>
                </thead>
                <tbody>

                @foreach($results1 as $result)
                    <?php $statut = 'Validé'; if ($result->statut == 0) {
    $statut = 'Non validé';
} ?>
                    <tr id="{{$result->id_temoignage}}">
                        <td data-target="nom">{{$result->nom}}</td>
                        <td data-target="prenom">{{$result->prenom}}</td>
                        <td data-target="email">{{$result->email}}</td>
                        <td data-target="annee">{{$result->annee}}</td>
                        <td data-target="statut"><b>{{$statut}}</b></td>


                        <?php // Colonnes cachées contenant la description des témoignages?>
                        <td style="display:none" data-target="id_user"><b>{{$result->id_user}}</b></td>
                        <td style="display:none" data-target="resume"><b>{{$result->resume}}</b></td>
                        <td style="display:none" data-target="metier"><b>{{$result->metier}}</b></td>
                        <td style="display:none" data-target="description"><b>{{$result->description}}</b></td>

                        <td align="center">
                            @if($result->statut==0)
                                <a style="text-decoration: none" href="#" data-role="editer"
                                   data-id="{{$result->id_temoignage}}" class="btn-sm btn-info" role="button">Editer</a>
                            @else
                                <a style="text-decoration: none" href="#" data-role="desactiver"
                                   data-id="{{$result->id_temoignage}}" class="btn-sm btn-dark"
                                   role="button">Désactiver</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('modals')
    <!-- Modal valider un témoignage-->
    <div id="temoignageView" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="memberModalLabel"><B>Témoignage</B></h5>
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{url('valider-temoignage')}}" role="form" autocomplete="off">
                        @csrf
                        <input type="hidden" class="form-control" id="id_temoignage" name="id" value=""/>
                        <input type="hidden" class="form-control" id="id_user" name="id_user" value=""/>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg ">
                                    <label>Nom</label>
                                    <input type="text" class="form-control" disabled="disabled" required="true"
                                           id="nom_update" name="nom" maxlength="49"/>
                                </div>
                                <div class="col-lg ">
                                    <label>Prenom</label>
                                    <input type="text" class="form-control" disabled="disabled" required="true"
                                           id="prenom_update" name="prenom" maxlength="49"/>
                                </div>

                            </div>
                        </div>

                        <div class="form-group">
                            <label>Metier</label>
                            <input maxlength="99" type="text" class="form-control" required pattern=".*\S+.*"
                                   id="metier" name="metier" value=""/>
                        </div>

                        <div class="form-group">
                            <label for="comment">Résumé du témoignage</label>
                            <!-- Le nombre de caractère maximum est de 210. De cette manière on a pas de débordement en mode mobile-->
                            <textarea class="form-control" required pattern=".*\S+.*" maxlength="210" rows="5"
                                      id="resume" name="resume"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="comment">Témoignage complet</label>
                            <textarea maxlength="4999" class="form-control" required pattern=".*\S+.*" rows="5"
                                      id="description" name="description"></textarea>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                            <input class="btn btn-danger" type="submit" value="Supprimer" name="delete_temoignage"/>
                            <input class="btn btn-success" type="submit" value="Valider" name="valide_temoign"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal désactiver un témoignage-->
    <div id="deactivateTemoign" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="memberModalLabel"><B>Désactiver un témoignage</B></h6>
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{url('desactiver-temoignage')}}" role="form" autocomplete="off">
                        @csrf
                        <div class="form-group">
                            <input type="hidden" class="form-control" id="id_deactivate" name="id" value=""/>
                            <input type="hidden" class="form-control" id="id_user_deactivate" name="id_user" value=""/>
                        </div>

                        <h6>Voulez vraiment désactiver le témoignage ?</h6>
                        <p>Le témoignage sera concervé dans la base de donnée, mais ne sera plus consultable
                            par les visiteurs du site.
                        </p><br/>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                            <input class="btn btn-danger" type="submit" value="Désactiver"
                                   name="deactivate_temoignage"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // Chargement du tableau dynamique
        $(document).ready(function () {
            $('#dataTable').DataTable();
        });

        $(document).ready(function () {
            // Lorsqu'on click sur le bouton mettre à jour, on récupère les informations de la ligne
            // et on les stocke dans des variables
            $(document).on('click', 'a[data-role=editer]', function () {
                var id = $(this).data('id');
                var id_user = $('#' + id).children('td[data-target=id_user]').text();
                var nom = $('#' + id).children('td[data-target=nom]').text();
                var prenom = $('#' + id).children('td[data-target=prenom]').text();
                var resume = $('#' + id).children('td[data-target=resume]').text();
                var metier = $('#' + id).children('td[data-target=metier]').text();
                var description = $('#' + id).children('td[data-target=description]').text();


                // On socke les informations (variables) récupérées dans le modal=> $('#id dans le modal').val(nom de la variable);

                $('#temoignageView').modal('toggle');
                $('#id_temoignage').val(id);
                $('#id_user').val(id_user);
                $('#nom_update').val(nom);
                $('#prenom_update').val(prenom);
                $('#metier').val(metier);
                $('#resume').val(resume);
                $('#description').val(description);
            });

            $(document).on('click', 'a[data-role=desactiver]', function () {
                var id = $(this).data('id');
                $('#deactivateTemoign').modal('toggle');
                $('#id_deactivate').val(id);
                $('#id_user_deactivate').val(id_user);

            });
        });
    </script>
@endsection