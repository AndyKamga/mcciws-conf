@extends('layouts.back')

<?php $page_en_cours = 'gestion-des-inscriptions';?>

@section('content')
    <!---Tableau --->
    <div class="card card-body">
        <div class="table-responsive">
            <table id="dataTable" class="table table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th class="th-sm">Nom</th>
                    <th class="th-sm">Prénom</th>
                    <th class="th-sm">Email</th>
                    <th class="th-sm">Année</th>

                    <th style="display:none" class="th-sm">Description</th>
                    <!-- options -->
                    <th class="th-sm text-center">Option</th>
                </tr>
                </thead>
                <tbody>

                @foreach($results1 as $result)
                    <tr id="{{$result->id_inscrip}}">
                        <td data-target="nom">{{$result->nom}}</td>
                        <td data-target="prenom">{{$result->prenom}}</td>
                        <td data-target="email">{{$result->email}}</td>
                        <td data-target="annee">{{$result->annee}}</td>

                        <td style="display:none" data-target="description"><b>{{$result->description}}</b></td>

                        <td align="center">
                            <a style="text-decoration: none" href="#" data-role="valider"
                               data-id="{{$result->id_inscrip}}" class="btn-sm btn-info" role="button">Voir</a>
                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('modals')
    <!-- Modal valider une inscription-->
    <div id="voirInscription" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="memberModalLabel"><B>A propos de l'utilisateur</B></h5>
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{url('modifier-inscription')}}" role="form" autocomplete="off">
                        @csrf
                        <input type="hidden" class="form-control" id="id_inscrip" name="id"/>
                        <div class="alert alert-warning" role="alert">
                            <strong>Attention !</strong>
                            Avant de valider l'inscription, vérifié manuellement
                            que l'utilisateur n'est pas déjà inscrit, via le menu gestion des utilisateurs !
                        </div>

                        <div class="form-group">
                            <label for="comment">Nom</label>
                            <input class="form-control" disabled="disabled" rows="5" id="nom" name="nom"
                                   maxlength="49"/>
                        </div>
                        <div class="form-group">
                            <label for="comment">prénom</label>
                            <input class="form-control" disabled="disabled" rows="5" id="prenom" name="prenom"
                                   maxlength="49"/>
                        </div>
                        <div class="form-group">
                            <label for="comment">Description</label>
                            <textarea maxlength="399" class="form-control" disabled="disabled" rows="5" id="description"
                                      name="description"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Type de compte</label>
                            <select class="form-control" name="typecompte" required="true" id="compte_add">
                                @foreach($results2 as $result)
                                    <option>{{$result->description}}</option>');
                                @endforeach
                            </select>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                            <input class="btn btn-danger" type="submit" value="Rejeter" name="rejeter_inscript"/>
                            <input class="btn btn-success" type="submit" value="Valider" name="valider_inscript"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // Chargement du tableau dynamique
        $(document).ready(function () {
            $('#dataTable').DataTable();
        });

        $(document).ready(function () {

            $(document).on('click', 'a[data-role=valider]', function () {
                var id = $(this).data('id');
                var description = $('#' + id).children('td[data-target=description]').text();
                var nom = $('#' + id).children('td[data-target=nom]').text();
                var prenom = $('#' + id).children('td[data-target=prenom]').text();

                $('#voirInscription').modal('toggle');
                $('#id_inscrip').val(id);
                $('#nom').val(nom);
                $('#prenom').val(prenom);
                $('#description').val(description);


            });


        });
    </script>
@endsection
