@extends('layouts.back')

<?php $page_en_cours = 'mon-espace';?>

@section('content')
    <!-- Contenu de la page-->

    <div class="card card-body">
        <h5>Accès rapide</h5>
        <hr>
        <br>
        <div class="row text-center">
            <div class="col-lg form-group">
                <a href="http://10.132.4.51" target=_blank><img src="{{ asset('images/backoffice/gogs.png') }}"
                                                                width="210px" height="70px" alt="Gogs"></a>
            </div>
            <div class="col-lg form-group">
                <a href="http://ent.univ-tours.fr/" target=_blank><img src="{{ asset('images/backoffice/ent.png') }}"
                                                                       width="210px" height="70px" alt="ENT"></a>
            </div>
            <div class="col-lg form-group">
                <a href="https://celene.univ-tours.fr/" target=_blank><img
                            src="{{ asset('images/backoffice/celene.png') }}" width="210px" height="70px" alt="Celene"></a>
            </div>
        </div>
    </div>
    <div id="fb-root"></div>
@endsection

@section('scripts')
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v3.2';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
@endsection
