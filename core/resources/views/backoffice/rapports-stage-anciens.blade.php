@extends('layouts.back')

<?php $page_en_cours = 'rapports-stage-anciens';?>

@section('content')
    <!---Tableau --->
    <div class="card card-body">
        <div class="table-responsive">
            <table id="dataTable" class="table table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th class="th-sm">Nom</th>
                    <th class="th-sm">Prenom</th>
                    <th class="th-sm">Année</th>
                    <th class="th-sm">Entreprise</th>
                    <th class="th-sm">Sujet de stage</th>
                    <th class="th-sm text-center">Rapport</th>
                    <th class="th-sm text-center">Note de synthèse</th>
                </tr>
                </thead>
                <tbody>
                @foreach($results1 as $result)
                    <tr>
                        <td>{{$result->nom}}</td>
                        <td>{{$result->prenom}}</td>
                        <td>{{$result->annee}}</td>
                        <td>{{$result->nom_entr}}</td>
                        <td>{{$result->sujet_stage}}</td>
                        <td align="center">
                            <a href="storage/{{$result->rapport_de_stage}}" target="_blank" class="btn-sm btn-success"
                               role="button" download style="text-decoration:none">Télécharger</a>
                        </td>

                        <td align="center">
                            <a href="storage/{{$result->note_synthese}}" target="_blank" class="btn-sm btn-primary"
                               role="button" download style="text-decoration:none">Télécharger</a>
                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        // Chargement du tableau dynamique
        $(document).ready(function () {
            $('#dataTable').DataTable();
        });

    </script>
@endsection