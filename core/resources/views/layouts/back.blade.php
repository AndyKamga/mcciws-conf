<!doctype html>
<html lang="fr">
<head>
    <?php

    // Importation des class necessaires au fonctionnement du header

    use App\Services\HeaderService;

    $id_user = Auth::user()->id;

    $typeCompte = HeaderService::getCompte($id_user);
    $notifRapportStage = HeaderService::getRapport();
    $notiftemoignages = HeaderService::getTemoignages();
    $notifsituation = HeaderService::getSituations();
    $notifOffresValid = HeaderService::getOffreValid();
    $notifOffresNonValid = HeaderService::getOffreNonValid();
    $notifInscriptions = HeaderService::getInscriptions();

    // Informations concernant les pages

    $infos_pages = [
        'mon-espace' => [
            'titre' => 'Mon espace personnel',
            'auteurs' => ['Andy_W_Kamga', 'Remy_Gomet', 'Thomas_Clique']
        ],
        'gestion-des-utilisateurs' => [
            'titre' => 'Gestion des utilisateurs',
            'auteurs' => ['Andy_W_Kamga', 'Remy_Gomet', 'Thomas_Clique']
        ],
        'gestion-rapports-stages' => [
            'titre' => 'Gestion des rapports de stage',
            'auteurs' => ['Andy_W_Kamga', 'Remy_Gomet', 'Thomas_Clique']
        ],
        'rapports-stage-anciens' => [
            'titre' => 'Rapport de stages des etudiants et des anciens',
            'auteurs' => ['Andy_W_Kamga', 'Remy_Gomet', 'Thomas_Clique']
        ],
        'gestion-des-temoignages' => [
            'titre' => 'Gestion des temoignages',
            'auteurs' => ['Andy_W_Kamga', 'Remy_Gomet', 'Thomas_Clique']
        ],
        'gestion-offres-de-stage' => [
            'titre' => 'Gestion des offres de stages',
            'auteurs' => ['Andy_W_Kamga', 'Remy_Gomet', 'Thomas_Clique']
        ],
        'voir-offres-de-stage' => [
            'titre' => 'Offre de stages disponibles',
            'auteurs' => ['Andy_W_Kamga', 'Remy_Gomet', 'Thomas_Clique']
        ],
        'mon-profil' => ['titre' => 'Mon profil', 'auteurs' => ['Danuta_Wojcik']],
        'messagerie' => ['titre' => 'Messagerie', 'auteurs' => ['Louis Forestier']],
        'historique-des-mails' => ['titre' => 'Historique des mails',
            'auteurs' => ['Louis Forestier']
        ],
        'gestion-des-dates-importantes' => ['titre' => 'Dates importantes', 'auteurs' => ['Rudy_Kajdan']],
        'gestion-des-projets' => ['titre' => 'Gestion des projets des étudiants',
            'auteurs' => ['Sandy_Chéry']
        ],
        'mon-rapport' => ['titre' => 'Déposer son rapport de stage',
            'auteurs' => ['Rajae_Boutouala']
        ],
        'mon-temoignage' => ['titre' => 'Poster un témoignage', 'auteurs' => ['Aïcha_Gamal']],
        'gestion-situation-anciens' => ['titre' => 'Gestion des situations professionnelles des anciens',
            'auteurs' => ['Andy_W_Kamga']
        ],
        'gestion-des-inscriptions' => ['titre' => 'Gestion des inscriptions',
            'auteurs' => ['Andy_W_Kamga']
        ],
        'gestion-des-droits' => ['titre' => 'Gestion des droits', 'auteurs' => ['Andy_W_Kamga']],
        'mise-a-jour-site' => ['titre' => 'Mise à jour du site', 'auteurs' => ['Andy_W_Kamga']],
    ];

    echo('<title>'.$infos_pages[$page_en_cours]['titre'].' - Espace privé</title>');

    foreach ($infos_pages[$page_en_cours]['auteurs'] as $auteur) {
        echo('<meta name="author" content="'.$auteur.'">');
    }

    ?>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <script type="text/javascript" src="{{ asset('js/backoffice/jquery.min.js') }}"></script>
    <link rel="icon" type="image/gif" href="{{ asset('images/frontend/favicon.png') }}"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css"
          integrity="sha384-3AB7yXWz4OeoZcPbieVW64vVXEwADiYyAEhwilzWsLw+9FgqpyjjStpPnpBO8o8S" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('backoffice') }}"/>
    <link href="{{ asset('css/backoffice/select2.min.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/backoffice/bootstrap.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/backoffice/dataTables.bootstrap.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/backoffice/style.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/backoffice/select2-bootstrap4.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/backoffice/'.$page_en_cours.'.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/backoffice/toastr.min.css') }}"/>

</head>
<body class="fixed-sn white-skin" aria-busy="true">

@yield('style')

<div class="loading">
    <img class="img-fluid text-center" src="{{ asset('images/backoffice/loading.gif') }}" alt="loading">
</div>
<header>
    <!-- Barre de navigation verticale -->
    <div id="slide-out" class="side-nav fixed">
        <ul class="collapsible">
            <!-- Photo de profil -->
            <div class="info_utilisateur text-center">
                <img src="storage/{{Auth::user()->photo}}" class="rounded-circle img-thumbnail" alt="avatar">
                <p class="nom_utilisateur">{{Auth::user()->prenom}} {{Auth::user()->nom}}</p>
                <p class="statut_utilisateur">{{$typeCompte}}</p>
                <hr color="(61, 61, 71)">
            </div>

            @include('backoffice/inc/menu')

        </ul>
    </div>

    <!-- Barre de navigation horizontale principale -->
    <nav id="main-navbar" class="navbar fixed-top navbar-toggleable-md navbar-expand-md double-nav">
        <div class="float-left">
            <a href="#" data-activates="slide-out" class="button-collapse black-text"><img
                        src="{{ asset('images/backoffice/menu-toggle.png') }}" width="25px" alt=""></a>
        </div>
        <b></b>
        <div class="breadcrumb-dn mr-auto">
            <p><img src="{{ asset('images/backoffice/logo.png') }}" width="20px" class="img-fluid flex-center"><b
                        class="espace_prive">MON ESPACE</b></p>
        </div>

        <ul class="nav navbar-nav nav-flex-icons ml-auto">
            <li>
                <img src="storage/{{Auth::user()->photo}}" class="rounded-circle" alt="avatar">
                <a class="dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" href="#">
                    Bienvenue {{Auth::user()->prenom}}</a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item divider" href="mon-profil"><i style="margin-right: 10px"
                                                                          class="fas fa-user"></i>Mon profil</a>
                    <a class="dropdown-item divider" href="/"><i style="margin-right: 10px" class="fas fa-backward"></i>Retour
                        au site</a>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i>
                        {{ __('Se déconnecter') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>
    </nav>

    <!-- Barre de navigation horizontale secondaire-->
    <nav id="second-navbar" class="navbar navbar-toggleable-md navbar-expand-md double-nav">
        <div class="breadcrumb-dn mr-auto">
            <p class="page-title animated slideInLeft"><?php echo($infos_pages[$page_en_cours]['titre'])?></p>
        </div>

        <ul class="nav navbar-nav nav-flex-icons ml-auto">
            <li>
                @yield('action')
            </li>
        </ul>

    </nav>
</header>
<div style="height: 50px"></div>

<main id="content">
    @yield('content')
</main>

@yield('modals')

<script type="text/javascript" src="{{ asset('js/backoffice/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/backoffice/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/backoffice/popper.min.js') }}"></script>
<script src="{{ asset('js/backoffice/bmd.js') }}"></script>
<div class="hiddendiv common"></div>
<script type="text/javascript" src="{{ asset('js/backoffice/toastr.min.js') }}"></script>
<script type="text/javascript">
    // SideNav Initialization
    $(".button-collapse").sideNav();
    new WOW().init();
</script>
<script type="text/javascript" src="{{ asset('js/backoffice/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/backoffice/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/backoffice/jquery.nicescroll.min.js') }}"></script>

<script type="text/javascript">
    @if(Session::has('message'))
    toastr.info("{{Session::get('message')}}");
    @endif
</script>

@yield('scripts')

<script type="text/javascript">

    $(window).on('load', function () {
        $('.loading').fadeOut();
        $("#content").addClass("animated slideInUp");
    });

    $("#slide-out ul li a").on('click', function () {
        $('.loading').show();
    });
</script>

</body>
</html>
