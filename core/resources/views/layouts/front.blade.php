<!doctype html>
<html lang="fr">
<head>
    <title>@yield('title') - Master Compétence complémentaire en informatique</title>
    @section('auteurs')
    @show

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="icon" type="image/png" href="{{ asset('images/frontoffice/favicon.png') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontoffice/bootstrap.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontoffice/style.css') }}"/>
    @if($page_en_cours == "entreprises" || $page_en_cours == "nous-trouver")
        <link rel="stylesheet" type="text/css" href="{{ asset('css/frontoffice/toastr.min.css') }}"/>
    @endif
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontoffice/'.$page_en_cours.'.css') }}"/>

</head>
<body>

<header>

    @if ($page_en_cours == 'accueil')
        <div class="background"><img src="{{ asset('images/frontoffice/background.png') }}"></div>
        <img id="waveimage" src="{{ asset('images/frontoffice/wave.png') }}" alt="" width="100%" height="100%">
    @endif

<!-- Barre de navigation -->
    <nav class="navbar navbar-expand-md sidebarNavigation">
        <div class="container">

            <!-- Contenu à gauche-->
            <!-- Bouton pour dérouler le menu en mode responsive -->
            <button class="navbar-toggler leftNavbarToggler" type="button" data-toggle="collapse"
                    data-target="#collapsibleNavbar"><span><img
                            src="{{ asset('images/frontoffice/menu-toggle.svg') }}" style="width:30px;"
                            alt="menu"></span>
            </button>

            <a class="navbar-brand" href="{{url('/')}}">
                @if ($page_en_cours == 'accueil')
                    <img src="{{ asset('images/frontoffice/logo_white.png')}}" alt="Logo" style="width:70px;">
                @else
                    <img src="{{ asset('images/frontoffice/logo.png')}}" alt="Logo" style="width:70px;">
                @endif
            </a>

            <!-- Contenu au centre -->
            <div class="collapse navbar-collapse justify-content-center" id="collapsibleNavbar">
                <ul class="nav nav-pills">
                    <!-- Dropdown -->
                    <li class="nav-item dropdown ">
                        <a class="nav-link dropdown-toggle
                        <?php if ($page_en_cours == 'accueil' || $page_en_cours == 'mots-du-responsable'
                            || $page_en_cours == 'temoignages'
                        ) {
    echo('active');
} ?>" data-toggle="dropdown">LE MASTER</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{url('/')}}">Accueil</a>
                            <a class="dropdown-item" href="{{url('/mots-du-responsable')}}">Mot du responsable</a>
                            <a class="dropdown-item" href="{{url('/temoignages')}}">Témoignages</a>
                        </div>
                    </li>
                    <li class="nav-item"><a class="nav-link <?php if ($page_en_cours == 'programme') {
    echo('active');
} ?>" href="{{url('/programme')}}">PROGRAMME</a></li>
                    <li class="nav-item"><a class="nav-link <?php if ($page_en_cours == 'candidater') {
    echo('active');
} ?>" href="{{url('/candidater')}}">CANDIDATER</a></li>
                    <li class="nav-item"><a class="nav-link <?php if ($page_en_cours == 'entreprises'
                            || $page_en_cours == 'projets-des-etudiants'
                        ) {
    echo('active');
} ?>" href="{{url('/entreprises')}}">ENTREPRISES</a></li>
                    <li class="nav-item"><a class="nav-link <?php if ($page_en_cours == 'nous-trouver') {
    echo('active');
} ?>" href="{{url('/nous-trouver')}}">NOUS TROUVER</a></li>
                </ul>
            </div>

            <!-- Contenu à droite sur PC -->
            <div class="topnav-right">
                <a class="nav-link" id="logo-text" href="home">CONNEXION
                    <span><img src="{{ asset('images/frontoffice/login.png') }}"
                               alt="Login">
                        </span>
                </a>
            </div>
            <!-- Contenu à droite sur Mobile -->
            <div class="topnav-right">
                <a class="nav-link logo-login" href="home">
                    <img src="{{ asset('images/frontoffice/login.png') }}"
                         alt="Login">
                </a>
            </div>
        </div>
    </nav>

    <!-- Message de bienvenue page d'accueil -->

    @if ($page_en_cours == 'accueil') {
    <div class="container text-center" id="messg_accueil">
        <fieldset class="scheduler-border">
            <legend class="scheduler-border">BIENVENUE SUR LE SITE DU</legend>
            <div class="control-group">
                <h1>MASTER CCI</h1>
                <p>Compétence Complémentaire en Informatique</p>
                <p class="ville">TOURS</p>
            </div>
        </fieldset>
    </div>
    @endif

</header>

<!-- Contenu de la page -->
@yield('content')
<!-- Fin du contenu de la page -->

<script src="{{ asset('js/frontoffice/jquery.min.js') }}"></script>
<script src="{{ asset('js/frontoffice/popper.min.js') }}"></script>
<script src="{{ asset('js/frontoffice/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/frontoffice/sidebar.js') }}"></script>
@if($page_en_cours == "entreprises" || $page_en_cours == "nous-trouver")
    <script src="{{ asset('js/frontoffice/toastr.min.js') }}"></script>
    <script type="text/javascript">
        @if (Session:: has('message'))
        toastr.info("{{Session::get('message')}}");
        @endif
    </script>
    <script type="text/javascript">
        $('#fichier').bind('change', function () {

            // Vérification de l'extension
            let ext = $('#fichier').val().split('.').pop().toLowerCase();
            if ($.inArray(ext, ['pdf', 'doc', 'docx']) === -1) {
                document.getElementById("error_message").innerHTML = "Le format du fichier est incorrect !";
                $('#fichier').val('');
            } else {
                document.getElementById("error_message").innerHTML = "";
            }

            // Vérification de la taille du fichier
            let sizeInKB = this.files[0].size / 1024;
            let sizeLimit = 2000; // 2MB

            if (sizeInKB >= sizeLimit) {
                document.getElementById("error_message").innerHTML = "La taille du fichier doit être inférieure à 2MB !";
                $('#fichier').val('');
                return false;
            } else {
                document.getElementById("error_message").innerHTML = "";
            }
        });
    </script>
@endif

<footer>
    <div class="container">
        <div class="row text-center">
            <div class="col-lg">
                <a href="https://www.univ-tours.fr/" target="_blank"><img
                            src="{{ asset('images/frontoffice/univ.png') }}"
                            alt="Master CCI" width="120"></a>
                <a href="https://sciences.univ-tours.fr/" target="_blank"><img
                            src="{{ asset('images/frontoffice/ufr.png') }}" alt="Master CCI"
                            width="110"></a>
            </div>
            <div class="col-lg">
                <p class="zone_centre">Copyright © 2018-2019. <a href="mentions-legales">Mentions légales</a></p>
            </div>
            <div id="reseaux" class="col-lg">
                <a href="https://www.facebook.com/Master-2-CCI-Tours-Comp%C3%A9tence-Compl%C3%A9mentaire-en-Informatique-1948671108542437/"
                   target="_blank"><img src="{{ asset('images/frontoffice/facebook.svg') }}" alt="Facebook" width="33"></a>
                <a href="https://www.linkedin.com/groups/8517340/" target="_blank"><img
                            src="{{ asset('images/frontoffice/linkedin.svg') }}" alt="Linkedin" hspace="20"
                            width="33"></a>
            </div>
        </div>
    </div>
</footer>
</body>
</html>
