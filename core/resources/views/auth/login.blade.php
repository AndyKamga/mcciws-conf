@extends('layouts.front')

<?php $page_en_cours = 'login';?>

@section('title', "Connexion à l'espace privé")

@section('auteurs')
    <meta name="author" content="Andy_W_KAMGA">
@show

@section('content')
    <style>
        header, footer{
            display: none;
        }
    </style>
    <!-- Contenu de la page -->
    <div class="container">
        <div class="logo">
            <img src="images/frontoffice/logo_white.png" alt="Logo MCCI">
            <h5>Connexion à l'espace privé</h5>
        </div>
        
        <div id="connexion_modal" class="card">
            <div class="card-body">
                <div class="container">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group"><br/>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Adresse email" required autofocus>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Mot de passe" name="password" required>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    {{ __('Se souvenir de moi') }}
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-success">
                                {{ __('Se connecter') }}
                            </button>
                        </div>
                    </form>
                    <div class="senregistrer">
                        <a href="enregistrement" class="btn btn-outline-primary btn-sm" role="button">S'enregistrer</a>
                    </div>
                    <br>
                    <div class="form-group text-center">
                        @if (Route::has('password.request'))
                            <a class="btn btn-link forgot_password" href="{{ route('password.request') }}">
                                {{ __('Mot de passe oublié ?') }}
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="retour">
            <a href="/">Retourner à la page d'accueil</a>
        </div>
    </div>

@endsection