@extends('layouts.front')

<?php $page_en_cours = 'reset';?>

@section('title', "Reinitialiser son mots de passe")

@section('auteurs')
    <meta name="author" content="Andy_W_KAMGA">
@show

@section('content')
    <style>
        header, footer{
            display: none;
        }
    </style>
    <!-- Contenu de la page -->
    <div class="container">
        <div class="logo">
            <img src="{{ asset('images/frontend/logo_white.png')}}" alt="Logo MCCI">
            <h5>Reinitialisation du mot de passe</h5>
            <p>
                Saisissez votre adresse email d'inscription et nous vous enverrons des
                instructions pour réinitialiser votre mot de passe ou activer votre compte.
                  </p>
        </div>
        
        <div id="connexion_modal" class="card">
            <div class="card-body">
                <div class="container">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="form-group">
                            <p></p>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Adresse email" required>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Réinitialiser le mot de passe') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="retour">
            <a href="{{ route('login') }}">Retourner à la page de connexion</a>
        </div>
    </div>

@endsection


    


