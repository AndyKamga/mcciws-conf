@extends('layouts.front')

<?php $page_en_cours = 'reset';?>

@section('title', "Reinitialiser son mots de passe")

@section('auteurs')
    <meta name="author" content="Andy_W_KAMGA">
@show

@section('content')
    <style>
        header, footer{
            display: none;
        }
    </style>
    <br/><br/><br/><br/>
<div class="container">
    <div class="row justify-content-center">
        <div id="connexion_modal" class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reinitialiser votre mot de passe') }}</div>

                <div class="card-body text-center">
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group">
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" placeholder="Adresse email" required autofocus>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Nouveau mot de passe" required>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirmer le mot de passe" required>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Reinitialiser le mot de passe') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection