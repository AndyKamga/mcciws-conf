@extends('layouts.front')

<?php $page_en_cours = 'entreprises';?>

@section('title', 'Entreprise')

@section('auteurs')
    <meta name="author" content="Thomas_Clique">
    <meta name="author" content="Danuta_Wojcik">
@show

@section('content')
    <div id="prem_section_blanche" class="container-fluid">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-3">
                    <img src="{{asset('images/frontoffice/entreprise_illust.svg')}}" alt=photo width=160px/>
                </div>
                <div class="col-lg-9">
                    <h2>Comment devenir partenaire du Master CCI ?</h2>
                    <p>À travers le Master CCI, la Faculté de Sciences et Techniques s'est toujours attachée à
                        construire
                        des relations privilégiées avec les entreprises permettant ainsi de répondre
                        mutuellement aux besoins spécifiques
                        de chacun. Soutenez notre développement et nos projets par le versement de votre taxe
                        d'apprentissage et ainsi :
                    </p>

                    <li>
                        Contribuer de manière efficace au développement d'une formation de haut niveau.
                    </li>
                    <li>Favoriser le développement de talents.</li>
                    <li>
                        Soutener la politique de stages et facilitez l'insertion des jeunes diplômés dans le milieu
                        professionnel.
                    </li>
                    <li>Investisser sur la formation de vos futurs collaborateurs : diplômés, stagiaires, apprentis.
                    </li>

                </div>
            </div>
        </div>
    </div>

    <div id="section_grise" class="container-fluid">
        <div class="container">

            <!--article n°1-->
            <div class="card">
                <div class="card-header collapsed card-link" data-toggle="collapse" href="#collapseComm1">
                    <div class="detail">ENTREPRISES AYANT ACCUEILLI NOS ÉTUDIANTS EN STAGE<span
                                class="badge badge-light badge-pill float-right">
                            <img src="{{asset('images/frontoffice/arrow off.svg')}}" style="width:15px;"
                                 alt="menu"></span>
                    </div>
                </div>
                <div id="collapseComm1" class="collapse">
                    <div class="card-body">
                        <div class="detail table-responsive">
                            <!--connexion avec base des donnes-->

                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Entreprises</th>
                                    <th>Sujets de stage</th>
                                </tr>
                                </thead>

                                @foreach($results1 as $result)
                                    <tr>
                                        <td>{{$result->nom_entr}}</td>
                                        <td>{{$result->sujet_stage}}</td>
                                    </tr>
                                @endforeach
                            </table>

                        </div>
                    </div>
                </div>
            </div>

            <!--article n°2-->
            <div class="card">
                <div class="card-header collapsed card-link" data-toggle="collapse" href="#collapseComm2">
                    <div class="detail">SITUATION PROFESSIONNELLE DES ANCIENS DU MASTER<span
                                class="badge badge-light badge-pill float-right">
                            <img src="{{asset('images/frontoffice/arrow off.svg')}}" style="width:15px;"
                                 alt="menu"></span>
                    </div>
                </div>
                <div id="collapseComm2" class="collapse">
                    <div class="card-body">
                        <div class="detail table-responsive">

                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Postes occupés</th>
                                    <th>Entreprises</th>
                                </tr>
                                </thead>
                                @foreach($results2 as $result)
                                    <tr>
                                        <td>{{$result->poste_occupe}}</td>
                                        <td>{{$result->nom_entr}}</td>
                                    </tr>
                                @endforeach
                            </table>

                        </div>
                    </div>
                </div>
            </div>

            <!--article n°3-->
            <div class="card">
                <div class="card-header collapsed card-link" data-toggle="collapse" href="#collapseComm3">
                    <div class="detail" id="ta">TAXE D'APPRENTISSAGE<span
                                class="badge badge-light badge-pill float-right"><img
                                    src="{{asset('images/frontoffice/arrow off.svg')}}" style="width:15px;" alt="menu"></span>
                    </div>
                </div>
                <div id="collapseComm3" class="collapse">
                    <div class="card-body">
                        <div class="detail">
                            <h3>Qu'est-ce que la taxe d’apprentissage ?</h3>
                            <p>Il s’agit d’un impôt versé par les entreprises, qui permet de perfectionner la
                                qualité de l’insertion professionnelle de nos étudiants.</p>

                            <h3>Comment procéder ?</h3>
                            <p>Le Master Compétence Complementaire en Informatique de l’université de Tours est
                                habilité à percevoir la taxe d‘apprentissage dans les catégories <b>B</b> et <b>C</b>
                                par la
                                règle du cumul.</p>
                            <ol type="1">
                                <li class="t">Choisissez votre Organisme Collecteur répartiteur de la Taxe
                                    d’Apprentissage (OCTA), qui vous fournira un bordereau de versement.
                                </li>
                                <li class="t">Remplissez celui-ci en précisant la somme que vous désirez verser
                                    en spécifiant le code <span class="turquoise">UIA 0371084 C.</span></li>
                                <li class="t">Pensez à nous renvoyer <a
                                            href="https://www.univ-tours.fr/medias/fichier/avis-de-versement-2016_1455101856302-pdf?ID_FICHE=192613&INLINE=FALSE">le
                                        formulaire</a> déclaratif de versement
                                    préalablement rempli, pour un meilleur suivi de votre dossier. N'oublier pas de
                                    renseigner dans le champ "Votre versement" le Master CCI.
                                </li>
                            </ol>
                            <br/>
                            <p class="text-center" style="font-size:10pt">Pour toute information complémentaire,
                                veuillez <a href="nous-trouver#nous-contacter">nous contacter</a>.</p>
                        </div>
                    </div>
                </div>
            </div>

            <!--Boutons responsives-->
            <div class="row text-center" id="responsiveButt">
                <div class="col-lg-6">
                    <!--boutton qui derige sur le page des projets-->
                    <a href="projets-des-etudiants" class="btn btn-info btn-md" role="button" id="proj_btn">Voir les
                        projets des étudiants</a>
                </div>

                <div class="col-lg-6" id="depotoffre">
                    <!--boutton qui ouvre formulaire de depot de stage-->
                    <button id="depot" type="button" class="btn btn-info btn-md" data-toggle="modal"
                            data-target="#myModal">Déposer
                        une offre de stage
                    </button>
                </div>
            </div>

            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4><b>Déposer une offre de stage</b></h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body panel-default">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#envoiFichier">Formulaire
                                        simplifié</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#rempliFormulaire">Formulaire
                                        complet</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <!-- Formulaire d'envoi de fichier -->
                                <div id="envoiFichier" class="container tab-pane active"><br>
                                    <form id="envoiFichierForm" method="POST" action="envoiOffreFichier" role="form"
                                          enctype=multipart/form-data> {{csrf_field()}}
                                        <div class="row">
                                            <div class="col-lg">
                                                <div>
                                                    <label>Titre du stage *</label>
                                                    <input type="text" class="form-control" id="titre" name="titre"
                                                           required pattern=".*\S+.*" maxlength="49">
                                                </div>
                                                <div>
                                                    <label>Nom de l'entreprise *</label>
                                                    <input type="text" class="form-control" id="entreprise"
                                                           name="entreprise"
                                                           required pattern=".*\S+.*" maxlength="19">
                                                </div>

                                                <div>
                                                    <label>Localisation *</label>
                                                    <input type="text" class="form-control" id="localisation"
                                                           name="localisation"
                                                           required pattern=".*\S+.*" maxlength="49">
                                                </div>

                                            </div>
                                            <div class="col-lg">
                                                <br>
                                                <p><b>Importer une offre de stage au format PDF, ou remplisser le
                                                        formulaire complet.</b></p>
                                                <div>
                                                    <label>Importer votre fichier.</label>
                                                    <input type="file" class="form-control-file" name="fichier"
                                                           id="fichier"
                                                           accept="application/pdf" required pattern=".*\S+.*">
                                                    <p id="error_message" style="color:red; font-size:10pt;"></p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="modal-footer text-center">
                                            <input id="valid_btn" class="btn btn-success" type="submit"
                                                   value="ENVOYER L'OFFRE DE STAGE"
                                                   name="envoyerFichier"/>
                                        </div>

                                    </form>
                                </div>

                                <!-- Formulaire d'envoi de texte -->
                                <div id="rempliFormulaire" class="container tab-pane fade"><br>
                                    <form method="POST" action="envoiOffreTexte" role="form">
                                        {{csrf_field()}}
                                        <div class="row">
                                            <div class="col-lg">
                                                <div>
                                                    <label for="usr">Titre du stage *</label>
                                                    <input type="text" class="form-control" id="usr" name="titre"
                                                           required
                                                           pattern=".*\S+.*" maxlength="49">
                                                </div>

                                                <div class="row">
                                                    <div class="col-lg">
                                                        <label for="cie">Nom de l'entreprise *</label>
                                                        <input type="text" class="form-control" id="entreprise"
                                                               name="entreprise"
                                                               required pattern=".*\S+.*" maxlength="19">
                                                    </div>
                                                    <div class="col-lg">
                                                        <label for="where">Localisation *</label>
                                                        <input type="text" class="form-control" id="localisation"
                                                               name="localisation"
                                                               required pattern=".*\S+.*" maxlength="49">
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-lg">
                                                        <label for="email"> E-mail</label>
                                                        <input type="email" class="form-control" id="mail" name="mail"
                                                               maxlength="49">

                                                    </div>
                                                    <div class="col-lg">
                                                        <label for="teleph"> N° téléphone</label>
                                                        <input type="number" class="form-control" id="telephone"
                                                               name="telephone" min="100000000" max="10000000000000">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg">
                                                        <label>Personne à contacter</label>
                                                        <input type="text" class="form-control" id="contact"
                                                               name="contact" maxlength="49">
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-lg">
                                                <div class="row">
                                                    <div class="col-lg">
                                                        <label for="dateb">Date du début de stage *</label>
                                                        <input type="date" class="form-control" id="debut" name="debut"
                                                               required pattern=".*\S+.*">

                                                    </div>
                                                    <div class="col-lg">
                                                        <label for="datee">Date de fin du stage *</label>
                                                        <input type="date" class="form-control" id="fin" name="fin"
                                                               required pattern=".*\S+.*">
                                                    </div>
                                                </div>
                                                <label for="desc">Description *</label>
                                                <textarea class="form-control" id="description" name="description"
                                                          rows="9" required pattern=".*\S+.*"
                                                          maxlength="1000"></textarea>

                                            </div>
                                        </div>

                                        <div class="modal-footer text-center">
                                            <input id="valid_btn" class="btn btn-primary" type="submit"
                                                   value="ENVOYER L'OFFRE DE STAGE"
                                                   name="envoyerOffre"/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection