@extends('layouts.front')

<?php $page_en_cours = 'enregistrement';?>

@section('title', 'Créer son compte')

@section('auteurs')
    <meta name="author" content="Andy_W_KAMGA">
@show

@section('content')
    <style>
        header, footer{
            display: none;
        }
    </style>
    <!-- Contenu de la page -->
    <div class="container">
        <div class="logo">
            <p>
                Vous êtes un ancien du master CCI et vous n'avez pas pu reinitialiser votre mot de passe ?
                Remplisser le formulaire et cliquer sur le bouton "S'enregistrer".
            </p>
        </div>
        
        <div id="connexion_modal" class="card">
            <div class="card-body">
                <div class="container">
                    @if (session('erreur'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('erreur') }}
                        </div>
                    @endif
                    @if (session('correct'))
                        <div class="alert alert-success" role="alert">
                            {{ session('correct') }}
                        </div>
                    @endif
                    <form method="POST" action="enregistrement">
                        @csrf
                        <div class="form-group">
                            <label>Nom</label>
                            <input maxlength="49" type="text" class="form-control{{ $errors->has('nom') ? ' is-invalid' : '' }}" name="nom" value="{{ old('nom') }}">
                            @if ($errors->has('nom'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('nom') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Prénom</label>
                            <input maxlength="49" type="text" class="form-control{{ $errors->has('prenom') ? ' is-invalid' : '' }}" name="prenom" value="{{ old('prenom') }}">
                            @if ($errors->has('prenom'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('prenom') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Adresse email</label>
                            <input maxlength="49" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Année de promotion</label>
                            <select class="form-control{{ $errors->has('annee') ? ' is-invalid' : '' }}" id="annee" name="annee" value="{{ old('annee') }}">
                                <?php $i = 0;  $annee_actuelle = date('Y'); ?>
                                @for($i=$annee_actuelle-1; $i>1995; $i--)
                                    <option>{{$i}}</option>
                                @endfor
                            </select>
                            @if ($errors->has('annee'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('annee') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Mot de passe</label>
                            <input maxlength="199" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                                <label>Confirmation du mot de passe</label>
                                <input maxlength="199" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password_confirmation" >
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        </div>
                        <div class="form-group">
                            <label for="comment">Rappelez-nous qui vous êtes</label>
                            <textarea maxlength="399" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" rows="5" id="description" name="description"></textarea>
                            @if ($errors->has('description'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                {{ __("S'enregistrer") }}
                            </button>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>

        <div class="retour">
            <a href="/">Retourner à la page d'accueil</a>
        </div>
    </div>

@endsection