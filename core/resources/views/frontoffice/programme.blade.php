@extends('layouts.front')

<?php $page_en_cours = 'programme';?>

@section('title', 'Programme de la formation')

@section('auteurs')
    <meta name="author" content="Remy_Gomet">
    <meta name="author" content="Sandy_Chéry">
@show

@section('content')
    <!-- Contenu de la page -->
    <div id="prem_section_blanche">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-9">
                    <h2>Programme de la formation</h2>
                    <p>Le Master CCI propose un programme s'articulant autour de deux semestres denses suivis
                        d'un stage de 4 à 6 mois. Le stage est évalué par un jury en fonction du
                        rapport écrit du stage, de la note de synthèse, d'une soutenance et des appréciations du tuteur
                        de
                        l'organisme d'accueil. La validation du stage est nécessaire l'obtention du diplôme.
                    </p>
                </div>
                <div class="col-lg-3">
                    <img src="{{ asset('images/frontoffice/programme_illust.svg') }}" alt=photo width=200px/>
                </div>
            </div>
        </div>
    </div>

    <div id="section_grise">
        <div class="container">
            <p class="titre_card">Cliquer pour en savoir plus.</p>
            <!-- PROGRAMME DU SEMESTRE 1 -->
            <div class="card">
                <div class="card-header collapsed card-link" data-toggle="collapse" href="#collapseComm1">
                    <div class="detail">SEMESTRE 1<span class="badge badge-light badge-pill float-right"><img
                                    src="{{ asset('images/frontoffice/arrow off.svg') }}"
                                    style="width:15px;" alt="menu"></span></div>
                </div>
                <div id="collapseComm1" class="collapse">
                    <div class="card-body">
                        <div class="detail table-responsive">

                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">Matières</th>
                                    <th scope="col">Ects</th>
                                    <th scope="col">CM</th>
                                    <th scope="col">TD</th>
                                    <th scope="col">TP</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row"><a href="#" data-toggle="modal"
                                                       data-target="#AlgorithmiqueProgrammation">Algorithmique et
                                            programmation</a></th>
                                    <td>3</td>
                                    <td>20H</td>
                                    <td>20H</td>
                                    <td>20H</td>
                                </tr>
                                <tr>
                                    <th scope="row"><a href="#" data-toggle="modal"
                                                       data-target="#ArchitectureSystèmeInformatiques">Architecture
                                            des systèmes informatiques</a></th>
                                    <td>5</td>
                                    <td>26H</td>
                                    <td>10H</td>
                                    <td>12H</td>
                                </tr>
                                <tr>
                                    <th scope="row"><a href="#" data-toggle="modal" data-target="#POO">Programmation
                                            Orientée Objet (POO)</a></th>
                                    <td>4</td>
                                    <td>20H</td>
                                    <td>20H</td>
                                    <td>-</td>
                                </tr>
                                <tr>
                                    <th scope="row"><a href="#" data-toggle="modal" data-target="#InitiationRéseaux">Initiation
                                            aux réseaux</a></th>
                                    <td>5</td>
                                    <td>26H</td>
                                    <td>12H</td>
                                    <td>10H</td>
                                </tr>
                                <tr>
                                    <th scope="row"><a href="#" data-toggle="modal" data-target="#GénieLogicielUML">Génie
                                            logiciel - Conception UML</a></th>
                                    <td>5</td>
                                    <td>10H</td>
                                    <td>10H</td>
                                    <td>-</td>
                                </tr>
                                <tr>
                                    <th scope="row"><a href="#" data-toggle="modal" data-target="#BasesDonnées">Bases
                                            de données</a></th>
                                    <td>5</td>
                                    <td>30H</td>
                                    <td>10H</td>
                                    <td>24H</td>
                                </tr>
                                <tr>
                                    <th scope="row"><a href="#" data-toggle="modal"
                                                       data-target="#StructuresDonnéesComplexité">Structures
                                            de données et complexité</a></th>
                                    <td>3</td>
                                    <td>12H</td>
                                    <td>12H</td>
                                    <td>12H</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Les popups semestre 1-->

            <div id="AlgorithmiqueProgrammation" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Algorithmique et programmation</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <p>Introduction aux concepts fondamentaux de l’algorithmique et de la
                                programmation.
                                Initiation à la programmation Java.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div id="ArchitectureSystèmeInformatiques" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Architecture des systèmes informatiques</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <p>Découverte de l’architecture des ordinateurs et les fonctions des
                                différents composants d'un système.
                                Acquisition d’une vision d'ensemble des moyens disponibles pour
                                augmenter les performances d'un système.
                            </p>
                            <p>
                                Connaissance des concepts généraux, des systèmes d’exploitation
                                (Windows, Linux), des structures en couches, à noyau (kernel)…
                                Etude de Linux.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div id="POO" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Programmation Orientée Objet (POO)</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <p>Introduction aux concepts de base de la programmation objet.</p>
                            <p>
                                Disposer d'un premier point de vue sur les approches Java et .Net.
                                Identifier les apports de la modélisation UML.
                                Connaissance des éléments fondamentaux nécessaires à
                                l'apprentissage du développement Objet.
                            </p>
                            <p>
                                Notions de classes, attributs, objets/instances, méthode,
                                classe abstraite, interface, visibilité.
                                encapsulation, héritage, polymorphisme.
                                Les notions d’exceptions, thread, swing, generics.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div id="InitiationRéseaux" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Initiation aux réseaux</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <p>Étude des concepts de base des réseaux, des applications internet.
                                Étude de la création et de la programmation d'un site web internet
                                (HTML, CSS, PHP, JavaScript et XML…).
                            </p>
                            <p>
                                Modèle Client/Serveur, protocoles réseaux (TCP/IP) et applicatifs
                                (HTTP, DNS, FTP, SMTP,…).
                                Savoir reconnaître les protocoles de communication utilisés et les
                                services rendus.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div id="GénieLogicielUML" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Génie logiciel - Conception UML</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <p>Introduction au génie logiciel et au cycle de vie d'un logiciel, de
                                l'expression des besoins à la recette.
                            </p>
                            <p>
                                Acquisition des compétences pour pouvoir intervenir dans les phases
                                de la production du logiciel au niveau de la conception et la
                                spécification.
                            </p>
                            <p>
                                Notion des diagrammes UML (diagramme de classe, de cas et
                                de séquences…).
                                Méthode d’analyse et de génération de code.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div id="BasesDonnées" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Bases de données</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <p>Introduction aux bases de données et SGBD (Système de Gestion des
                                bases de Données).
                                Modélisation UML, méthode Merise et application des concepts sur
                                le SGBD MS SQL Serveur.
                            </p>
                            <p>
                                Savoir utiliser des requêtes pour manipuler les données issues de
                                plusieurs tables.
                                Être en mesure d’installer et de configurer SQL Server.
                                Acquérir les compétences nécessaires à la gestion des fichiers de
                                bases de données.
                                Maîtriser les langages SQL et Transact-SQL.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div id="StructuresDonnéesComplexité" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Structures de données et complexité</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <p>Structures de données : liste, pile, file, tableau, arbre, table de
                                hachage.
                                Algorithmes de tri classiques.
                                Complexité des algorithmes.</p>
                        </div>
                    </div>
                </div>
            </div>

            <!-- PROGRAMME DU SEMESTRE 2 -->
            <div class="card">
                <div class="card-header collapsed card-link" data-toggle="collapse" href="#collapseComm2">
                    <div class="detail">SEMESTRE 2<span class="badge badge-light badge-pill float-right"><img
                                    src="images/frontoffice/arrow off.svg"
                                    style="width:15px;" alt="menu"></span></div>
                </div>
                <div id="collapseComm2" class="collapse">
                    <div class="card-body">
                        <div class="detail table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">Matières</th>
                                    <th scope="col">Ects</th>
                                    <th scope="col">CM</th>
                                    <th scope="col">TD</th>
                                    <th scope="col">TP</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <!-- <th scope="row">  Veille Technologique et ProjetsRepository</th> -->

                                    <th scope="row"><a href="#" data-toggle="modal" data-target="#VeilleTechnologique">
                                            Veille technologique et projets</a></th>
                                    <td>8</td>
                                    <td>34H</td>
                                    <td>30H</td>
                                    <td>12H</td>
                                </tr>
                                <tr>
                                    <th scope="row"><a href="#" data-toggle="modal" data-target="#OrganisationGestion">Organisation
                                            et gestion</a></th>
                                    <td>1</td>
                                    <td>10H</td>
                                    <td>10H</td>
                                    <td>-</td>
                                </tr>
                                <tr>
                                    <th scope="row"><a href="#" data-toggle="modal" data-target="#ProgrammationJAVA">Programmation
                                            JAVA - JEE</a></th>
                                    <td>2</td>
                                    <td>20H</td>
                                    <td>20H</td>
                                    <td>-</td>
                                </tr>
                                <tr>
                                    <th scope="row"><a href="#" data-toggle="modal" data-target="#TechniquesExpression">Techniques
                                            d'expression</a></th>
                                    <td>1</td>
                                    <td>10H</td>
                                    <td>10H</td>
                                    <td>-</td>
                                </tr>
                                <tr>
                                    <th scope="row"><a href="#" data-toggle="modal" data-target="#Architecture">Architecture
                                            .NET avec C#</a></th>
                                    <td>2</td>
                                    <td>20H</td>
                                    <td>20H</td>
                                    <td>-</td>
                                </tr>
                                <tr>
                                    <th scope="row"><a href="#" data-toggle="modal" data-target="#InternetIntranet">Internet
                                            - Intranet</a></th>
                                    <td>2</td>
                                    <td>20H</td>
                                    <td>20H</td>
                                    <td>-</td>
                                </tr>
                                <tr>
                                    <th scope="row"><a href="#" data-toggle="modal" data-target="#ApplicationMobiles">Applications
                                            mobiles</a></th>
                                    <td>2</td>
                                    <td>20H</td>
                                    <td>20H</td>
                                    <td>-</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

            <!-- Les popups semestre 2 -->
            <div id="VeilleTechnologique" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Veille technologique et projets</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <p>Conférences et veille technologique dans le but d’être appliqué dans un projet concret.
                                Projet à pratiquer en équipe.
                            </p>
                            <p>
                                Identification des conditions optimales de mise en place de la veille technologique et
                                stratégique.
                            </p>
                            <p>
                                Appréhender les méthodes et les outils.
                            </p>
                            <p>
                                Dialoguer efficacement autour d'un projet de déploiement de veille.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div id="OrganisationGestion" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Organisation et gestion</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <p>Exploiter les informations permettant de faire un budget de service.
                                Découverte des notions de gestion comptable (comptabilité générale,
                                analytique, budgétaire…) financière et commerciale,
                                à travers entre autres des jeux d’entreprises.
                            </p>
                            <p>
                                Anticiper les évolutions et les attentes de son secteur d'activité.
                                Identifier et sélectionner les opportunités.
                                Proposer une analyse complète en matière d'opportunités/risques
                                afin d'orienter la une décision.
                            </p>
                            <p>
                                Maîtriser les démarches d'amélioration continue.
                                Connaissance de mise en place des indicateurs de suivi de gestion,
                                innovation et stratégies entrepreneuriales.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div id="ProgrammationJAVA" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Programmation JAVA - JEE</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <p>Comprendre les concepts et enjeux des architectures Web.
                                Comprendre et savoir évaluer l'architecture et la conception des
                                principaux Framework Web du marché.
                            </p>
                            <p>
                                Acquérir des savoir-faire pour concevoir, développer et utiliser
                                des applications web basées sur les
                                technologies JSP et Servlet, JSTL, EJB (stateful, stateless), JDBC.
                                Problématiques et caractéristiques des applications Web (HTTP, HTML
                                et URL).
                                Exemple d'outillage (IDE/container Web) avec Tomcat.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div id="TechniquesExpression" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Techniques d'expression</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <p>Anglais technique appliqué dans le domaine informatique.
                                Développer son aisance relationnelle en toutes circonstances.
                                Vie associative.
                            </p>
                            <p>
                                Gagner en efficacité dans sa fonction par une meilleure
                                communication.
                                L'ensemble des moyens et techniques permettant la diffusion d'un
                                message auprès d'une certaine audience.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div id="Architecture" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Architecture .NET avec C#</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <p>Présentation de la plateforme .NET et étude des principales notions
                                du langage C# avec la réalisation d'applications (WinForm, WPF,
                                ASP.NET).
                            </p>
                            <p>
                                Maîtriser les design patterns.
                                Approfondir les principes de développement des logiciels.
                                Développer les applications .NET en utilisant l'IDE Visual Studio.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div id="InternetIntranet" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Internet - Intranet</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <p>Etude des services Internet, création et hébergement de sites web
                                avec PHP/MySQL, Frameworks (Bootstrap, JQuery, Laravel 5.7,
                                Angular Js …).
                            </p>
                            <p>
                                Utilisation de Git, GitHub, Composer, ... .
                                Gestion et sécurité de sites internet/intranet (accès anonymes,
                                mot de passe, optimisation des performances…).
                                Création des sites dynamiques, interactions avec les bases de
                                données.
                            </p>
                            <p>
                                Acquisition de la maîtrise globale de la sécurisation d'un réseau
                                privé en utilisant les technologies internet/intranet, et de
                                l'interconnexion de ce réseau avec des réseaux extérieurs.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div id="ApplicationMobiles" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Applications mobiles</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <p>Initiation à la programmation des applications mobiles Android
                                (smartphones, tablettes tactiles…).
                            </p>
                            <p>
                                Concevoir et prototyper des applications mobiles.
                                Savoir définir l'architecture d'une application pour mobile.
                            </p>
                            <p>
                                Comprendre les spécificités d'HTML, JavaScript et CSS propres au
                                développement mobile.
                                Savoir faire interagir l'application avec les fonctions de base du
                                téléphone.</p>
                        </div>
                    </div>
                </div>
            </div>


            <!-- DEBOUCHES DE LA FORMATION -->
            <div class="card">
                <div class="card-header collapsed card-link" data-toggle="collapse" href="#collapseComm3">
                    <!-- Contenu de la 3e card -->
                    <div class="detail" id="deb">VOIR LES DÉBOUCHÉS<span
                                class="badge badge-light badge-pill float-right"><img
                                    src="images/frontoffice/arrow off.svg" style="width:15px;" alt="menu"></span></div>
                </div>

                <div id="collapseComm3" class="collapse">
                    <div class="card-body" id="pic">
                        <div class="detail">
                            <div class="container">
                                <div class="row" id="debouches">
                                    <!-- Colonne gauche dans le dépliant -->
                                    <div class="col-lg-6">
                                        <h5>SECTEURS D'ACTIVITE, TYPE D'EMPLOIS</h5>
                                        <p>Le Master Professionnel Compétence Complémentaire en
                                            Informatique répond au besoin actuel d’une meilleure intégration des
                                            techniques informatiques dans les différents métiers de l’entreprise.</p>
                                        <p>Les emplois visés concernent :</p>
                                        <p><b>L’étude, le développement, l’intégration et l’administration
                                                des systèmes d’informations :</b></p>
                                        <ul>
                                            <li>Administrateur des bases de données</li>
                                            <li>Administrateur outils et technologies Internet</li>
                                            <li>Gestionnaire d’applications</li>
                                            <li>Paramétreur ERP</li>
                                            <li>Ingénieur des besoins</li>
                                            <li>Intégrateur d’applications</li>
                                            <li>Ingénieur de développement</li>
                                            <li>Business Intelligence</li>
                                        </ul>
                                        <p><b>La conduite de projet et la maîtrise d’ouvrage :</b></p>
                                        <ul>
                                            <li>Consultant en conduite du changement</li>
                                            <li>Facilitateur d’un projet d’ingénierie des besoins</li>
                                            <li>Responsable de projet Métier</li>
                                            <li>Chef de projet</li>
                                            <li>Maîtrise d’ouvrage</li>
                                            <li>Analyste fonctionnel</li>
                                            <li>Recette (TMA)</li>
                                        </ul>

                                    </div>
                                    <!-- Colonne droite dans le dépliant -->
                                    <div class="col-lg-6">
                                        <h5 class="domainCompt">DOMAINES DE COMPETENCES</h5>
                                        <p><b>Capacités transversales et connaissances « générales » :</b></p>
                                        <p>Les capacités acquises à l’issue de ce master sont :</p>
                                        <ul>
                                            <li>Des aptitudes comportementales pour <strong>communiquer</strong>,
                                                écouter et s’exprimer dans le cadre de projets informatiques. En
                                                ayant une compétence métier et une compétence informatique, les
                                                diplômés du master CCI peuvent améliorer la relation entre la
                                                maîtrise d’œuvre et la maîtrise d’ouvrage au sein des projets
                                                informatiques.
                                            </li>
                                            <br>
                                            <li>La capacité d’une part, à s’insérer rapidement dans le milieu
                                                professionnel, par l’adéquation de la formation aux besoins
                                                immédiats de l’industrie et d’autre part, à évoluer dans les
                                                différents métiers de l’informatique, en s’adaptant aux nouvelles
                                                demandes du marché, par l’enseignement des bases fondamentales de
                                                l’informatique.
                                            </li>
                                        </ul>
                                        <p><b>Capacités et connaissances liées au domaine du diplôme :</b></p>
                                        <p>Les compétences en informatique acquises à l’issue de ce
                                            master sont :</p>
                                        <ul>
                                            <li>La connaissance et la maîtrise des techniques et méthodes de
                                                l’informatique.
                                            </li>
                                            <li>La capacité à analyser, à concevoir et à développer des systèmes
                                                d’information.
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
