@extends('layouts.front')

<?php $page_en_cours = 'temoignages';?>

@section('title', 'Programme de la formation')

@section('auteurs')
    <meta name="author" content="Remy_Gomet">
@show

@section('content')

    <!-- Contenu de la page -->
    <div id="prem_section_blanche" class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-lg-4" id="illustration">
                    <img src="{{asset('images/frontoffice/temoignage_illust.svg')}}" width="270px"; alt="illustration1">
                </div>
                <div class="col-lg-8" id="texte">
                    <h2>Témoignages</h2>
                    <p>Certains anciens ont choisi ce master dans le but de mettre à jour leurs connaissances en
                        informatique, d'autres dans le cadre d'une reconversion professionnelle. Parfois méconnus,
                        les métiers de l’informatique ne se limitent pas au développement, mais couvrent toutes les
                        activités qui vont de la technique au fonctionnel.</p>
                    <p>Les plus grandes entreprises en France ont déjà choisi de faire confiance aux diplômés du
                        Master CCI, notamment des ESN reconnues. <a href="entreprises">Consulter la situation professionnelle des anciens pour en savoir plus</a>.</p>
                        
                </div>
            </div>
        </div>
    </div>

    <div id="section_grise" class="container-fluid">
        <div class="container">
            <?php $i = 1; ?>
            @foreach($results as $result)
                <div class="card">
                    <div class="card-header collapsed card-link" data-toggle="collapse" href="#<?php echo($i) ?>">
                        <div class="detail">{{$result->prenom}} {{$result->nom}} - {{$result->metier}}<span class="badge badge-light badge-pill float-right">
                            <img src="{{asset('images/frontoffice/arrow off.svg')}}" style="width:15px;" alt="menu"></span>
                        </div>
                    </div>
                    <div id="<?php echo($i) ?>" class="collapse">
                        <div class="card-body">
                            <div class="detail">
                                {!! nl2br(e($result->description)) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <?php $i = $i + 1; ?>
            @endforeach
        </div>
    </div>
@endsection