@extends('layouts.front')

<?php $page_en_cours = 'accueil';?>

@section('title', 'Présentation du Master CCI')

@section('auteurs')
    <meta name="author" content="Andy_W_Kamga">
@show

@section('content')

    <div id="prem_section_blanche" class="container-fluid">
        <div class="container">
            <div class="row">
                <div id="illustration" class="col-lg-5 text-center">
                    <img class="img-fluid" src="images/frontoffice/accueil_illust.svg" alt="Master CCI" width="350">
                </div>

                <div class="col-lg-7">
                    <h2>Le monde de l'informatique <br />vous passionne ?</h2>
                    <p>
                        Le Master CCI permet aux étudiants titulaires d'un bac +4 de faire valoir une double
                        compétence dans leur domaine de formation initial, mais leur donne également
                        la possibilité de se rediriger vers des carrières en informatique, en tant
                        qu'ingénieur d’études, développeur, analyste programmeur, chef de projet
                        informatique,
                        administrateur de bases de données.
                    </p>
                    <p>
                        La formation se déroule sur une seule année et vous offre
                        l'opportunité de maîtriser
                        les compétences les plus recherchées aujourd'hui.
                    </p>
                    <br />
                    <a href="programme"><button id="enSavoirPlus" class="btn btn-primary"><b>En savoir plus</b></button></a>
                </div>
            </div>
        </div>

    </div>
    
    <div id="section_bleuClaire" class="container-fluid text-center" style="padding-top:70px;padding-bottom:70px">
        <h1 class="text_fond text-center">ILS EN PARLENT</h1>
        <div class="container">
            <h4 class="titre_souligne" id="temoignage">Témoignages</h4>
            <div id="carouselContent" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
        
                    <?php $valeur1 = true; ?>
                    @foreach($results as $result)
                        <?php if ($valeur1 == true) { ?>
                            <div class="carousel-item active text-center p-4">
                                <p>{{$result->resume}}</p>
                                <p class="nom_temoin"><b>{{$result->prenom}} {{$result->nom}}</b></p>
                                <p class="profession_temoin">{{$result->metier}}</p>
                            </div>
                            <?php $valeur1 = false; } else {?>
                            <div class="carousel-item text-center p-4">
                                <p>{{$result->resume}}</p>
                                <p class="nom_temoin"><b>{{$result->prenom}} {{$result->nom}}</b></p>
                                <p class="profession_temoin">{{$result->metier}}</p>
                            </div>
                        <?php } ?>
                    @endforeach

                </div>
                <ol class="carousel-indicators">
                    <li data-target="#carouselContent" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselContent" data-slide-to="1"></li>
                    <li data-target="#carouselContent" data-slide-to="2"></li>
                    <li data-target="#carouselContent" data-slide-to="3"></li>
                </ol>
            </div>
            <a href="temoignages"><button class="btn btn-outline-success" type="submit"><b>Voir tous les
                        témoignages</b></button></a>
                        
        </div>
    </div>
    
    <div id="section_blanche" class="container-fluid text-center" style="padding-top:70px;padding-bottom:70px">
        <div class="container">
            <h4 class="titre_souligne">Nos partenaires</h4>
            <div class="row">
                <div class="col-lg-12" id="logo_partenaires">
                    <a href="https://www.soprasteria.com/fr/a-propos/sopra-steria-en-bref" target="_blank"><img src="images/frontoffice/partenaire_Sopra.png"
                        alt="Soprasteria" width="200"></a>
                    <a href="https://www.apside.com/" target="_blank"><img src="images/frontoffice/apside.png"
                            alt="Apside" width="150"></a>
                    <a href="https://www.umanis.com/en-bref/" target="_blank"><img src="images/frontoffice/partenaire_umanis.png"
                        alt="Umanis" width="130"></a>
                    <a href="https://www.acensi.fr/ACENSI - Présentation.html" target="_blank"><img src="images/frontoffice/partenaire_acensi.png"
                        alt="Acensi" width="150"></a>
                </div>
            </div>
        </div>
    </div>

@endsection
