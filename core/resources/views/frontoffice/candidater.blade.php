@extends('layouts.front')

<?php $page_en_cours = 'candidater';?>

@section('title', 'Candidater')

@section('auteurs')
    <meta name="author" content="Rudy_Kajdan">
@show

@section('content')
    <!-- Contenu de la page -->
    <div id="prem_section_blanche" class="container-fluid">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-9">
                    <h2>Conditions d’admission</h2>
                    <p>Les admissions s’effectuent sur dossier, en formation initiale ou continue. Pour candidater, vous
                        devez justifier d’un diplôme de niveau
                        <strong class="texte_colore">minimum Bac+4 ou équivalent scientifique, economie, gestion</strong>
                        ou être un ancien
                        informaticien désirant actualiser ses connaissances.</p>
                    <p>Des bases en algorithmique, un fort intérêt pour la discipline et une
                        importante motivation sont exigés pour pouvoir suivre cette formation
                        dense et intensive. En outre, <strong class="texte_colore"> un certificat attestant de
                            compétences
                            en HTML et CSS est exigé. En attendant l'ouverture des candidatures, vous pouvez vous
                            inscrire sur
                            le site MOOC de votre choix (OpenClassrooms, Udemy, ...) pour obtenir votre certificat, et
                            le mettre dans
                            votre dossier de candidature</strong>.</p>
                </div>
                <div class="col-lg-3">
                    <img src="{{ asset('images/frontoffice/candidature_illust.svg') }}" alt=photo width=200px/>
                </div>
            </div>
        </div>
    </div>

    <div id="section_grise" class="container-fluid">
        <div class="container">
            <p class="candidater_titre_processus">Veuillez sélectionner votre profil pour en savoir plus.</p>
            <div class="card">
                <div class="card-header collapsed card-link" data-toggle="collapse" href="#etudiantFR">
                    <div class="detail">ÉTUDIANT FRANÇAIS OU RESSORTISSANT DE L'UE
                        <span class="badge badge-light badge-pill float-right">
                            <img src="{{ asset('images/frontoffice/arrow off.svg') }}" style="width:15px;" alt="menu"/>
                        </span>
                    </div>
                </div>
                <div id="etudiantFR" class="collapse">
                    <div class="card-body">
                        <div class="detail">
                            <p>Si vous avez arrêté les études depuis moins de 2 ans, vous relevez de la formation
                                initiale. Constituez directement votre dossier de candidature en cliquant sur le bouton
                                <b>candidater maintenant</b> ci-dessous.
                            <p>Vous devez passer par la rubrique <b>Validation d'acquis</b>, puis <b>VA_M2 Compétence
                                    complémentaire en informatique</b>.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header collapsed card-link" data-toggle="collapse" href="#etudiantEtranger">
                    <div class="detail">
                        ÉTUDIANT HORS UNION EUROPÉENNE
                        <span class="badge badge-light badge-pill float-right">
                            <img src="{{ asset('images/frontoffice/arrow off.svg') }}" style="width:15px;" alt="menu"/>
                        </span>
                    </div>
                </div>
                <div id="etudiantEtranger" class="collapse">
                    <div class="card-body">
                        <div class="detail">
                            <p>Si vous êtes un étudiant vivant en dehors de l'Union Européenne, passez par le
                                CampusFrance de votre pays. Retrouvez toutes les
                                informations sur <a href="http://www.campusfrance.org/fr/" target="_blank">campusfrance.org</a>.
                            </p>
                            <p>Si vous habitez en France, cliquez sur le bouton <b>candidater maintenant</b> ci-dessous
                                et rendez-vous à la rubrique <b>Master (Dossier étudiants étrangers)</b>, puis <b>CI_M2
                                    Compétence Complémentaire en Informatique</b>.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header collapsed card-link" data-toggle="collapse" href="#formationContinue">
                    <div class="detail">SALARIÉ, DEMANDEUR D'EMPLOI OU EN REPRISE D'ÉTUDES
                        <span class="badge badge-light badge-pill float-right">
                            <img src="{{ asset('images/frontoffice/arrow off.svg') }}" style="width:15px;" alt="menu">
                        </span>
                    </div>
                </div>
                <div id="formationContinue" class="collapse">
                    <div class="card-body">
                        <div class="detail">
                            <p class>Si vous êtes salarié, demandeur d’emploi ou sans activité et souhaitez
                                reprendre des études, ce master est également accessible dans le cadre de la
                                formation continue, avec éventuellement la Validation des Acquis Professionnels
                                (VAP).</p>
                            <p>Les personnes relevant de la formation
                                continue peuvent payer personnellement leur formation ou bénéficier de l’aide
                                financière du Conseil Régional (nombre de places limité) ou de Pôle Emploi.</p>
                            <p>Tarif : 5260 €* (TVA non applicable).<br/>
                                <em>* Il existe un tarif pour les particuliers auto-finançant leur formation, contactez
                                    le Service de Formation Continue.</em>
                            <p>Le Master CCI est éligible au CPF (Compte Personnel de Formation).</p>
                            </p>
                            <p>Pour toute information concernant le statut, le financement ou toute autre
                                démarche administrative :<br/>
                            <ul>
                                <li>Nadia Joubert – <a href="mailto:nadia.joubert@univ-tours.fr">nadia.joubert@univ-tours.fr</a>
                                </li>
                                <li>Vous pouvez également consulter le <a
                                            href="https://formation-continue.univ-tours.fr">portail
                                        de la formation continue</a>.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div id="section_blanche" class="container-fluid">
        <div class="container">
            <div class="candidater_conteneur_tableau">
                <h2 class="candidater_texte_date">DATES IMPORTANTES</h2>
                <div class="candidater_cadre_tableau">
                    <br/>

                    <table class="candidater_tableau">


                        <?php  // tableau relationnel dont la cle est celle de le la table 'dates_importantes' et la valeur, la date au format jour mois(lettres) année
                        $dates = [];
                        $mois = '';

                        // la variable $mois contient le mois écrit en tout lettre
                        ?>

                        @foreach($results as $result)

                            @switch($result->mois)
                                @case (1)
                                <?php $mois = 'janvier';?>
                                @break
                                @case (2)
                                <?php $mois = 'février';?>
                                @break
                                @case (3)
                                <?php $mois = 'mars';?>
                                @break
                                @case (4)
                                <?php $mois = 'avril';?>
                                @break
                                @case (5)
                                <?php $mois = 'mai';?>
                                @break
                                @case (6)
                                <?php $mois = 'juin';?>
                                @break
                                @case (7)
                                <?php $mois = 'juillet';?>
                                @break
                                @case (8)
                                <?php $mois = 'août';?>
                                @break
                                @case (9)
                                <?php $mois = 'septembre';?>
                                @break
                                @case (10)
                                <?php $mois = 'octobre';?>
                                @break
                                @case (11)
                                <?php $mois = 'novembre';?>
                                @break
                                @case (12)
                                <?php $mois = 'décembre';?>
                                @break
                            @endswitch

                            <?php //remplissage du tableau
                            $dates[$result->cle] = $result->jour.' '.$mois.' '.$result->annee;
                            ?>


                        @endforeach


                        <tr>
                            <td>Ouverture des candidatures :</td>
                            <td class="candidater_tableau_colonne_droite"><?php echo $dates['debut_candidature']?></td>
                        </tr>
                        <tr>
                            <td>Limite de réception des dossiers :</td>
                            <td class="candidater_tableau_colonne_droite"><?php echo $dates['fin_candidature']?></td>
                        </tr>
                        <tr>
                            <td>Réunion de pré-rentrée :</td>
                            <td class="candidater_tableau_colonne_droite"><?php echo $dates['prerentree']?></td>
                        </tr>
                        <tr>
                            <td>Rentrée et début des cours :</td>
                            <td class="candidater_tableau_colonne_droite"><?php echo $dates['debut_cours']?></td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="candidater_button text-center">
                <a href="https://ecandidat.univ-tours.fr/ecandidat/#!accueilView" target="_blank">
                    <button type="button"
                            class="btn btn-lg"><strong>Candidater
                            maintenant</strong></button>
                </a>
            </div>
        </div>
    </div>
@endsection
