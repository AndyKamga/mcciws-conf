@extends('layouts.front')

<?php $page_en_cours = 'mentions-legales';?>

@section('title', 'Mentions légales')

@section('auteurs')
    <meta name="author" content="Andy_W_KAMGA">
@show

@section('content')
    <main>
        <div class="container">
            <br><br>
            <div class="mention-content">

                <h2>Mentions légales</h2><br>

                <!--Présentation du site-->
                <div class="grey-zone">
                    <div class="grey-zone-content">
                        <h5 class="titre-zone">Présentation du site</h5>
                        <p>En vertu de l'article 6 de la loi n° 2004-575 du 21 juin 2004 pour la confiance dans
                            l'économie numérique, il est précisé aux utilisateurs du site www.cci.univ-tours.fr
                            l'identité des différents intervenants dans le cadre de sa réalisation et de son suivi :
                        </p><br>
                        <h6>Développeurs (Promotion 2018/2019) : </h6>
                        <p>Andy WAFO KAMGA, Louis FORESTIER, Rudy KAJDAN, Rémy GOMET, Danuta DOBROWOLSKA, Rajae
                            BOUTOUALA, Aïcha GAMAL LAMBERT, Sandy CHERY, Thomas CLIQUE.</p><br>
                        <h6>Date de mise en ligne : </h6>
                        <p>Mercredi le 06 février 2019.</p><br>
                        <h6>Responsable publication : </h6>
                        <p>Michel TEGUIA</p>
                        <p>teguia@univ-tours.fr</p>
                        <p>Le responsable publication est une personne physique ou une personne morale.</p><br>
                    </div>
                </div>

                <!--Legislation-->
                <div class="grey-zone">
                    <div class="grey-zone-content">
                        <h5 class="titre-zone">Législation</h5>
                        <p>
                            En application de la loi du 11 mars 1957 (art. 41) et du code de la propriété
                            intellectuelle du 1er juillet 1992, toute reproduction partielle ou totale à usage
                            collectif est strictement interdite sans autorisation de l'université de Tours.
                            Les logos, visuels et marques présents sur ce site sont la propriété de leur détenteur
                            respectif.<br><br>

                            Ce site peut, à son insu, avoir été relié à d'autres sites par le biais de liens
                            hypermédia. L'Université François-Rabelais décline toute responsabilité pour les
                            informations présentées sur ces autres sites.<br><br>

                            L'internaute reconnaît que l'utilisation du site de l'université de Tours est régie par le
                            droit français.<br><br>
                        </p>
                    </div>
                </div>

                <!--Hébergement-->
                <div class="grey-zone">
                    <div class="grey-zone-content">
                        <h5 class="titre-zone">Hébergement</h5>
                        <h6>Université de Tours<br>Direction des Systèmes d'Information et de la communication (DSI)</h6>
                        <p>60 rue du Plat d'Etain</p>
                        <p>BP 12050</p>
                        <p>37020 Tours Cedex 1</p>
                        <p>France</p>
                        <p>service.informatique@univ-tours.fr</p>
                    </div>
                </div>

                <!--Conditions générales d’utilisation du site et des services proposés-->
                <div class="grey-zone">
                    <div class="grey-zone-content">
                        <h5 class="titre-zone">Gestion des données personnelles</h5>
                        <p>
                            En France, les données personnelles sont notamment protégées par la loi n° 78-87 du 6
                            janvier 1978, la loi n° 2004-801 du 6 août 2004, l'article L. 226-13 du Code pénal et la
                            Directive Européenne du 24 octobre 1995.<br><br>

                            A l'occasion de l'utilisation du site www.cci.univ-tours.fr, peuvent êtres recueillies :
                            l'URL des liens par l'intermédiaire desquels l'utilisateur a accédé au site
                            www.cci.univ-tours.fr, le fournisseur d'accès de l'utilisateur, l'adresse de protocole
                            Internet (IP) de l'utilisateur.<br><br>

                            En tout état de cause l'Université de Tours ne collecte des informations personnelles
                            relatives à l'utilisateur que pour le besoin de certains services proposés par le site
                            www.cci.univ-tours.fr. L'utilisateur fournit ces informations en toute connaissance de
                            cause, notamment lorsqu'il procède par lui-même à leur saisie. Il est alors précisé à
                            l'utilisateur du site www.cci.univ-tours.fr l’obligation ou non de fournir ces
                            informations.<br><br>

                            Conformément aux dispositions des articles 38 et suivants de la loi 78-17 du 6 janvier 1978
                            relative à l’informatique, aux fichiers et aux libertés, tout utilisateur dispose d’un
                            droit d’accès, de rectification et d’opposition aux données personnelles le concernant, en
                            effectuant sa demande écrite et signée, accompagnée d’une copie du titre d’identité avec
                            signature du titulaire de la pièce, en précisant l’adresse à laquelle la réponse doit être
                            envoyée.<br><br>

                            Aucune information personnelle de l'utilisateur du site www.cci.univ-tours.fr n'est publiée
                            à l'insu de l'utilisateur, échangée, transférée, cédée ou vendue sur un support quelconque
                            à des tiers.<br><br>

                            Les bases de données sont protégées par les dispositions de la loi du 1er juillet 1998
                            transposant la directive 96/9 du 11 mars 1996 relative à la protection juridique des bases
                            de données.<br><br>
                        </p>
                    </div>
                </div>
            </div>
            <br><br><br>
        </div>
    </main>
@endsection