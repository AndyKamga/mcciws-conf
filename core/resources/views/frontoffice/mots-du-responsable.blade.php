@extends('layouts.front')

<?php $page_en_cours = 'mots-du-responsable';?>

@section('title', 'Mots du responsable')

@section('auteurs')
  <meta name="author" content="Rajae_Boutouala">
@show

@section('content')

  <!-- Contenu de la page -->
  <main class="container-fluid">
    <div class="container">
      <div class="row">
        <div class="col-lg-2" id="nom">
          <hr color="white">
          <h3 class="titre1">MICHEL TEGUIA</h3>
          <p>Responsable de la formation</p>
          <hr color="white">
        </div>
        <div class="col-lg">
          <article>
            <h3 class="titre2">UN MASTER EN PROGRESSION CONTINUE</h3>
            <p>
              Créé en 1992, l’ancêtre du Master CCI est le premier (ordre chronologique) diplôme professionnel de
              niveau bac+5 de l’UFR Sciences et Techniques.
              <br /><br />
              Au cours de ses 26 années d’existence, il a eu plusieurs dénominations : DESS double compétence en
              informatique, puis DESS Compétence Complémentaire en Informatique (CCI). Depuis 2004, avec la mise
              en
              place du LMD, il est désigné sous le nom de Master CCI, spécialité de toutes les mentions de masters de
              l’Université de Tours.
              <br /><br />
              Le chemin parcouru depuis 26 ans est important. A noter la mise en place en 1999 d’une version
              délocalisée
              au
              Maroc qui a accueilli en moyenne 15 étudiants par an pendant 10 ans et en 2000 d’un diplôme d’université (DU
              CCI)
              en formation continue.
            </p>

            <h3 class="titre2">UNE CLE DE REUSSITE INFINIE</h3>

            <p>
              Le Master CCI et ses ancêtres c’est plus de 700 diplômés qui sont en majorité
              recrutés en début de carrière comme ingénieurs d’études dans les SSII ou ESN et certains d’entre eux sont
              devenus chefs de projet informatique ou responsables dans de grandes entreprises en France et à
              l’étranger (Maroc, Canada, …).
            </p>

            <h3 class="titre2">DES COLLABORATEURS PASSIONNES</h3>

            <p>Cette réussite repose sur l’implication de tous les acteurs :<br/><br/>
              
              En premier lieu des étudiants à travers les groupes de travail (communication, gestion conférences,
              gestion salles, gestion anciens, gestion rapports) et qui seront demain les ambassadeurs du Master CCI
              partout dans le monde.
              <br /><br />
              Ensuite du personnel administratif et technique notamment Madame Valérie Jurado secrétaire du Master CCI
              qui
              a été à mes côtés pendant toutes ces années pour la gestion administrative.
              <br /><br />
              Enfin et surtout de tous les enseignants et en particulier de mon complice Robert-Michel di Scala avec
              lequel
              j’ai tenu la barque avec beaucoup de turbulences pendant plus de 15 ans, mes collègues et amis : Philippe
              Adair et Fabrice Mourlin de l’Université de Paris 12 que je remercie pour leur aide aux moments
              difficiles
              et
              critiques du Master CCI.
              <br /><br />
              En conclusion le Master CCI de Tours est une formation connue et reconnue qui reçoit en moyenne 600
              dossiers de candidature par an.
            </p>

          </article>
        </div>
      </div>
  </main>
@endsection