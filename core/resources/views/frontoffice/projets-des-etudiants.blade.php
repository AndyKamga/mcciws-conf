@extends('layouts.front')

<?php $page_en_cours = 'projets-des-etudiants';?>

@section('title', 'Projet des étudiants')

@section('auteurs')
    <meta name="author" content="Sandy_Chéry">
@show

@section('content')
    <div id="section_bleuFonce" class="container-fluid">
        <div class="container">
            <h2>PROJETS DES ÉTUDIANTS</h2>
            <p>Retrouver l'ensemble des projets effectués par nos étudiants au cours de leur année.</p>

            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <form method="GET" action="/projets-des-etudiants">
                        <div id="form-group" class="form-group">
                            <select class="form-control" name="annee" required="true" id="annee">
                                <option value="0">Tous les projets</option>
                                @foreach($results1 as $result)
                                    <option>{{$result->date}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div id="form-group" class="form-group">
                            <button type="submit" class="btn btn-default" id="trier">FILTRER</button>
                        </div>  
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="section_grise" class="container-fluid">
        <div class="container">
            @if($annee==0)
                <h2 id="titreAnnee" class="text-center">Tous les projets</h2>
            @elseif(count($results2)==0)
                <h2 id="titreAnnee" class="text-center">Aucun résultat</h2>
            @else
                <h2 id="titreAnnee" class="text-center">{{$annee}}</h2>
            @endif

            <div class="wrapper">
                @foreach($results2 as $result)
                    <div class="card">
                        <div class="card-header">
                            {{$result->titre}}
                        </div>
                        <div class="card-body">
                            <h4>Description du projet</h4>
                            <p>{{$result->description}}</p>
                            <h4>Language(s) et compétence(s) utilisé(s)</h4>
                            <p>{{$result->langage_competence}}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection