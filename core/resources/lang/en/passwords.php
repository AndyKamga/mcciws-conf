<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Le mot de passe doit être composé de 6 caractères minimum.',
    'reset' => 'Votre mots de passe a été reinitialisé !',
    'sent' => 'Nous vous avons envoyé un mail pour reinitialiser votre compte !',
    'token' => 'Votre mot de passe est incorrect.',
    'user' => "L'adresse e-mail n'existe pas.",
];
