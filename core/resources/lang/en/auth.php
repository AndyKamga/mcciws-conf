<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Les informations transmises n\'ont pas permis de vous authentifier.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
];
